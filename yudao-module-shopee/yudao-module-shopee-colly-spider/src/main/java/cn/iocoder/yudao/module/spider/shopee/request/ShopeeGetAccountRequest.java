package cn.iocoder.yudao.module.spider.shopee.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashMap;
import java.util.Map;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ShopeeGetAccountRequest {
    private Long sysUserId;
    private Long tenantId;
    private Long accountId;
    private String cookie;

    public Map<String, Object> getParameters() {
        Map<String, Object> parameterMap = new HashMap<>();
        parameterMap.put("COOKIE", this.cookie);
        return parameterMap;
    }
}
