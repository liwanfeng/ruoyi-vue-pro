package cn.iocoder.yudao.module.spider.shopee.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ShopeeMpskuGetMpskuListV2Request {
    private Long tenantId;
    private Long sysUserId;
    private Long shopId;
}
