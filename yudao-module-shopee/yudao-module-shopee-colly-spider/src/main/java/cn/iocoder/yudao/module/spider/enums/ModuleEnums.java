package cn.iocoder.yudao.module.spider.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ModuleEnums {
    GET_ACCOUNT(SiteEnums.SHOPEE, "GET_ACCOUNT"),
    GET_MERCHANT_SHOP_LIST(SiteEnums.SHOPEE, "GET_MERCHANT_SHOP_LIST"),
    GET_MPSKU_LIST(SiteEnums.SHOPEE, "GET_MPSKU_LIST");

    private SiteEnums site;
    private String module;
}
