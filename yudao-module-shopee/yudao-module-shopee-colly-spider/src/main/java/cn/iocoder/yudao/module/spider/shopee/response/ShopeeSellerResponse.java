package cn.iocoder.yudao.module.spider.shopee.response;

import lombok.Data;

@Data
public class ShopeeSellerResponse {
    private Integer code;
    /**
     * 返回数据
     */
    private String data;
    /**
     * 错误提示，用户可阅读
     *
     */
    private String msg;

}
