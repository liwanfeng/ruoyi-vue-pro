package cn.iocoder.yudao.module.spider.shopee.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ShopeeSellerRequest {
    protected Long sysUserId;
    protected Integer tenantId;
    protected Integer accountId;
    protected String site;
    protected String module;
    protected String type;
    protected String dataId;
    protected Integer sync;
    protected Map<String, Object> parameters;
}
