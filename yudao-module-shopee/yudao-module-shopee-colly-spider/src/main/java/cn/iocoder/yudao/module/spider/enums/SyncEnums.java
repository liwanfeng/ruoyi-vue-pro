package cn.iocoder.yudao.module.spider.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum SyncEnums {
    SYNC(0, "同步"),
    ASYNC(1, "异步"),
    ;

    private Integer value;
    private String desc;
}
