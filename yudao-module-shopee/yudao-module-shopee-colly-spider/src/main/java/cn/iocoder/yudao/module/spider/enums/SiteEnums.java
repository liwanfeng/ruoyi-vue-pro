package cn.iocoder.yudao.module.spider.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum SiteEnums {
    SHOPEE("SHOPEE")
    ;
    private String name;
}
