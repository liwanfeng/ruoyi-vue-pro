package cn.iocoder.yudao.module.shopee.enums;

import cn.iocoder.yudao.framework.common.exception.ErrorCode;

/**
 * Infra 错误码枚举类
 * <p>
 * infra 系统，使用 1-001-000-000 段
 */
public interface ErrorCodeConstants {
    ErrorCode ACCOUNT_NOT_EXISTS = new ErrorCode(1009001000, "虾皮账户不存在");
    ErrorCode SHOP_NOT_EXISTS = new ErrorCode(1009002000, "虾皮店铺不存在");
    ErrorCode PRODUCT_NOT_EXISTS = new ErrorCode(1009003000, "虾皮商品不存在");

    ErrorCode ACCOUNT_HISTORY_NOT_EXISTS = new ErrorCode(1009001001, "虾皮账户历史不存在");
    ErrorCode SHOP_HISTORY_NOT_EXISTS = new ErrorCode(1009002001, "虾皮店铺历史不存在");
    ErrorCode PRODUCT_HISTORY_NOT_EXISTS = new ErrorCode(1009003001, "虾皮商品历史不存在");

}
