package cn.iocoder.boot.yudao.module.spider.shopee.seller.response.ad;

import lombok.Data;

import java.util.List;

@Data
public class SellerAdDataResponse {
    private List<SellerAdMetricsMapResponse> reportByTime;
    private SellerAdMetricsResponse reportAggregate;

}
