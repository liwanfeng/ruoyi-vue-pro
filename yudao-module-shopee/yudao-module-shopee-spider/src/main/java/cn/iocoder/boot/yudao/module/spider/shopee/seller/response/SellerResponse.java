package cn.iocoder.boot.yudao.module.spider.shopee.seller.response;

import lombok.Data;

@Data
public class SellerResponse {
    private Integer code;
    private Integer errcode;
    private String debugMessage;
    private String message;
    private String msg;
    private String debugDetail;
}
