package cn.iocoder.boot.yudao.module.spider.shopee.seller.response.shop;

import lombok.Data;

import java.util.List;

@Data
public class SellerShopDataResponse {
    private List<SellerShopResponse> shops;
    private Long total;
}
