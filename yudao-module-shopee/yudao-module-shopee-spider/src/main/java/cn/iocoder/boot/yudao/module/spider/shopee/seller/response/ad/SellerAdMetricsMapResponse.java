package cn.iocoder.boot.yudao.module.spider.shopee.seller.response.ad;

import lombok.Data;

@Data
public class SellerAdMetricsMapResponse {
    private Long key;
    private SellerAdMetricsResponse metrics;
}
