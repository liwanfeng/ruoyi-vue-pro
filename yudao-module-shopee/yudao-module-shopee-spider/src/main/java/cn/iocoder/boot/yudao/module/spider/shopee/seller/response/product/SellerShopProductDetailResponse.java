package cn.iocoder.boot.yudao.module.spider.shopee.seller.response.product;

import lombok.Data;

import java.util.List;

@Data
public class SellerShopProductDetailResponse {
    private Boolean addOnDeal;
    private Integer boostCoolDownSeconds;
    private String bundleDeal;
    private List<Long> categoryPath;
    private Boolean deboosted;
    private Integer flag;
    private Long id;
    private List<String> images;
    private Boolean inPromotion;
    private Integer likeCount;
    private List modelList;
    private String name;
    private SellerOngoingUncomingCampaignsResponse ongoingUpcomingCampaigns;
    private String parentSku;
    private Integer sold;
    private Integer status;
    private List tierVariation;
    private Integer viewCount;
    private List virtualSkuInfoList;
    private Boolean wholesale;
    private Integer categoryStatus;
    private Integer daysToShip;
    private Boolean preOrder;
    private List videoList;
    private Integer brandId;
    private String brand;
    private List attributes;
    private Integer createTime;
    private Integer modifyTime;
    private Boolean enableModelLevelDts;
    private Boolean isB2cSkuInFbsShop;
    private Integer mtskuItemId;
}
