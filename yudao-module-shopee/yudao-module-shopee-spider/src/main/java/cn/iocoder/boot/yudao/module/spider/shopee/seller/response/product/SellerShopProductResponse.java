package cn.iocoder.boot.yudao.module.spider.shopee.seller.response.product;

import cn.iocoder.boot.yudao.module.spider.shopee.seller.response.SellerResponse;
import lombok.Data;

@Data
public class SellerShopProductResponse extends SellerResponse {
    private SellerShopProductDataResponse data;
}
