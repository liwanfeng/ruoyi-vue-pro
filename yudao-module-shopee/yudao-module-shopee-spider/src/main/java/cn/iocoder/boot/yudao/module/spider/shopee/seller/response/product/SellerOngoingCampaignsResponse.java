package cn.iocoder.boot.yudao.module.spider.shopee.seller.response.product;

import lombok.Data;

@Data
public class SellerOngoingCampaignsResponse {
    private Long productId;
    private Long modelId;
    private Integer campaignType;
    private Integer promoSource;
    private Long startTime;
    private Long endTime;
    private String price;
    private Long stock;
    private String inputPrice;
}
