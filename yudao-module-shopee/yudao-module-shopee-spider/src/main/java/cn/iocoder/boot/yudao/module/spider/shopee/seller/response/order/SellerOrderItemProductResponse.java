package cn.iocoder.boot.yudao.module.spider.shopee.seller.response.order;

import lombok.Data;

import java.util.List;

@Data
public class SellerOrderItemProductResponse {
    private String name;
    private List<String> images;
    private String brand;
    private Integer catId;
    private Integer cmtCount;
    private Integer condition;
    private Long ctime;
    private String currency;
    private String description;
    private Integer estimatedDays;
    private boolean isPreOrder;
    private Long itemId;
    private Long modelId;
    private Integer likedCount;
    private String price;
    private String priceBeforeDiscount;
    private Long shopId;
    private Integer sold;
    private Integer status;
    private String sku;
    private Long snapshotId;
}
