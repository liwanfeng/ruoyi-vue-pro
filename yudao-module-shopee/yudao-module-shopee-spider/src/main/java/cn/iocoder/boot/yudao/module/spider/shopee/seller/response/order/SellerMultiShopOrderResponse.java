package cn.iocoder.boot.yudao.module.spider.shopee.seller.response.order;

import cn.iocoder.boot.yudao.module.spider.shopee.seller.response.SellerResponse;
import lombok.Data;

import java.util.List;

@Data
public class SellerMultiShopOrderResponse extends SellerResponse {
    private List<SellerOrderResponse> orders;
}
