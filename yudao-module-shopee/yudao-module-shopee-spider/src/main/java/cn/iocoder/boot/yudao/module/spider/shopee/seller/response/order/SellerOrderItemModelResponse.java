package cn.iocoder.boot.yudao.module.spider.shopee.seller.response.order;

import lombok.Data;

@Data
public class SellerOrderItemModelResponse {
    private Long modelId;
    private Long itemId;
    private String name;
    private String price;
    private String currency;
    private Integer status;
    private String priceBeforeDiscount;
    private Long promotionId;
    private String rebatePrice;
    private Integer sold;
    private Long ctime;
    private Long mtime;
    private String sku;
}
