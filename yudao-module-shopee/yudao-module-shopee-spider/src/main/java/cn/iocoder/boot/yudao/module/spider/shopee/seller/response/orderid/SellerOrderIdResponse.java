package cn.iocoder.boot.yudao.module.spider.shopee.seller.response.orderid;

import lombok.Data;

@Data
public class SellerOrderIdResponse {
    private Long orderId;
    private Long shopId;
    private String regionId;
}
