package cn.iocoder.boot.yudao.module.spider.shopee.seller.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 爬虫请求对象
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SellerRequest {
    private Long accountId;
    private Long shopId;
    private String regionId;
    private String spcCds;
    private String cookie;
}
