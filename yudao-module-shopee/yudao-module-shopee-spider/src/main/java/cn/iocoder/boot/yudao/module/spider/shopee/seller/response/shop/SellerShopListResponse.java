package cn.iocoder.boot.yudao.module.spider.shopee.seller.response.shop;

import cn.iocoder.boot.yudao.module.spider.shopee.seller.response.SellerResponse;
import lombok.Data;

@Data
public class SellerShopListResponse extends SellerResponse {
    private SellerShopDataResponse data;
}
