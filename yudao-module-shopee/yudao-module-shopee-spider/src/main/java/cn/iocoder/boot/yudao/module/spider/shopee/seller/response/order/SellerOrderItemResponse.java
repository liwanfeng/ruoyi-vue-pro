package cn.iocoder.boot.yudao.module.spider.shopee.seller.response.order;

import lombok.Data;

@Data
public class SellerOrderItemResponse {
    private Integer addOnDealId;
    private Integer amount;
    private Integer bundleDealId;
    private String groupId;
    private Boolean isAddOnSubItem;
    private Boolean isWholesale;
    private String itemPrice;
    private Long itemId;
    private Long modelId;
    private String orderPrice;
    private String priceBeforeBundle;
    private Long snapshotId;
    private Integer status;
    private Long shopId;
    private Boolean isVirtualSku;
    private String bundleDeal;
    private String bundleDealModel;
    private String itemList;
    private String bundleDealProduct;
    private Integer subType;
    private Boolean isPrescriptionItem;
    private SellerOrderItemModelResponse itemModel;
    private SellerOrderItemProductResponse product;
}
