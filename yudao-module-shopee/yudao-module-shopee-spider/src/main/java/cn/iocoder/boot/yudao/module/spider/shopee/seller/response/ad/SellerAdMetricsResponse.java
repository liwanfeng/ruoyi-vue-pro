package cn.iocoder.boot.yudao.module.spider.shopee.seller.response.ad;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class SellerAdMetricsResponse {
    private BigDecimal broadCir;
    private BigDecimal broadGmv;
    private BigDecimal broadOrder;
    private BigDecimal broadOrderAmount;
    private BigDecimal broadRoi;
    private BigDecimal checkout;
    private BigDecimal click;
    private BigDecimal cost;
    private BigDecimal cpc;
    private BigDecimal cpdc;
    private BigDecimal cr;
    private BigDecimal ctr;
    private BigDecimal directCr;
    private BigDecimal directCir;
    private BigDecimal directGmv;
    private BigDecimal directOrder;
    private BigDecimal directOrderAmount;
    private BigDecimal directRoi;
    private BigDecimal impression;
    private BigDecimal avgRank;
    private BigDecimal productClick;
    private BigDecimal productImpression;
    private BigDecimal productCtr;
    private BigDecimal locationInAds;
}
