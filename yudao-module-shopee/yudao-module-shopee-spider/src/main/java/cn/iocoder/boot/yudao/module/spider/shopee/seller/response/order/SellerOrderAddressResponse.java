package cn.iocoder.boot.yudao.module.spider.shopee.seller.response.order;

import lombok.Data;

@Data
public class SellerOrderAddressResponse {
    private Integer addressId;
    private Integer userId;
    private String name;
    private String phone;
    private String country;
    private String state;
    private String city;
    private String address;
    private Integer status;
    private Long ctime;
    private Long mtime;
    private String zipCode;
    private Integer defTime;
    private String fullAddress;
    private String district;
    private String town;
    private Integer logisticsStatus;
    private String icno;
}
