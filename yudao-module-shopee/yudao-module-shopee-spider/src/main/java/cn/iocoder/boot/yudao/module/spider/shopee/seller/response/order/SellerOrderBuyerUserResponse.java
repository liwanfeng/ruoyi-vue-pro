package cn.iocoder.boot.yudao.module.spider.shopee.seller.response.order;

import lombok.Data;

@Data
public class SellerOrderBuyerUserResponse {
    private Integer deliverySuccCount;
    private Integer hideLikes;
    private Long userId;
    private String userName;
    private String portrait;
    private Boolean phonePublic;
    private String language;
    private Integer cbOption;
    private Integer deliveryOrderCount;
    private Integer buyerRating;
    private Integer ratingStar;
    private Long shopId;
    private Boolean followed;
}
