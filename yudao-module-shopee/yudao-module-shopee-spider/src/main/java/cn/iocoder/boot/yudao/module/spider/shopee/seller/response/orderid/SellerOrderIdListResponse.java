package cn.iocoder.boot.yudao.module.spider.shopee.seller.response.orderid;

import cn.iocoder.boot.yudao.module.spider.shopee.seller.response.SellerResponse;
import lombok.Data;

@Data
public class SellerOrderIdListResponse extends SellerResponse {
    private SellerOrderIdListDataResponse data;
}
