package cn.iocoder.boot.yudao.module.spider.shopee.seller.response.product;

import lombok.Data;

import java.util.List;

@Data
public class SellerOngoingUncomingCampaignsResponse {
    private List<SellerOngoingCampaignsResponse> ongoingCampaigns;
}
