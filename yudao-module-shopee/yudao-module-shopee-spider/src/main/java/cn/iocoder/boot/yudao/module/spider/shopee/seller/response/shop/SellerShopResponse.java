package cn.iocoder.boot.yudao.module.spider.shopee.seller.response.shop;

import lombok.Data;

@Data
public class SellerShopResponse {
    private String username;
    private Long userId;
    private String region;
    private Boolean enabled;
    private Boolean isSipAffiliated;
    private String shopName;
    private Long shopId;
    private Integer lastLogin;
    private Boolean isSipPrimary;
    private String portrait;
    private Integer cbOption;
    private Boolean isInactive;
    private Boolean is3pfShop;
}
