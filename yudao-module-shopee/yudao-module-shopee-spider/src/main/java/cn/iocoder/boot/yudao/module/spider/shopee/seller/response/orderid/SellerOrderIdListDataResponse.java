package cn.iocoder.boot.yudao.module.spider.shopee.seller.response.orderid;

import cn.iocoder.boot.yudao.module.spider.shopee.seller.response.SellerPageResponse;
import lombok.Data;

import java.util.List;

@Data
public class SellerOrderIdListDataResponse {
    private SellerPageResponse pageInfo;
    private List<SellerOrderIdResponse> orders;
}
