package cn.iocoder.boot.yudao.module.spider.shopee.seller.response.account;

import cn.iocoder.boot.yudao.module.spider.shopee.seller.response.SellerResponse;
import lombok.Data;

@Data
public class SellerSessionResponse extends SellerResponse {
    private SellerSessionSubAccountInfoResponse subAccountInfo;
}
