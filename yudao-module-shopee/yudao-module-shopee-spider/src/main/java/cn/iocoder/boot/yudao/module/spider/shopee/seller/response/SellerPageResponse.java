package cn.iocoder.boot.yudao.module.spider.shopee.seller.response;

import lombok.Data;

@Data
public class SellerPageResponse {
    private Long total;
    private Long pageNumber;
    private Long pageNum;
    private Long pageSize;
    private Boolean hasNextPage;
}
