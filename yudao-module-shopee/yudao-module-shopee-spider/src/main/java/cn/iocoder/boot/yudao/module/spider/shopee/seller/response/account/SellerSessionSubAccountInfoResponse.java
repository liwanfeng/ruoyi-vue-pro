package cn.iocoder.boot.yudao.module.spider.shopee.seller.response.account;

import lombok.Data;

@Data
public class SellerSessionSubAccountInfoResponse {
    private String accountType;
    private Long accountId;
    private Long tobAccountId;
    private String phone;
    private String portrait;
    private String accountName;
    private Boolean isOverdue;
    private String language;
    private String nickName;
    private Integer mainAccountId;
    private Integer subAccountId;
    private String email;
    private String merchantName;
    private Long currentShopId;
    private Long currentMerchantId;
}
