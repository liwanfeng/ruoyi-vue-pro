package cn.iocoder.boot.yudao.module.spider.shopee.seller.response.product;

import cn.iocoder.boot.yudao.module.spider.shopee.seller.response.SellerPageResponse;
import lombok.Data;

import java.util.List;

@Data
public class SellerShopProductDataResponse {
    private SellerPageResponse pageInfo;
    private List<SellerShopProductDetailResponse> list;
}
