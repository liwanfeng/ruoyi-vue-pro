package cn.iocoder.yudao.module.shopee.controller.admin.shop.vo;

import lombok.*;
import java.util.*;
import io.swagger.v3.oas.annotations.media.Schema;
import javax.validation.constraints.*;

@Schema(description = "管理后台 - 虾皮店铺创建 Request VO")
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class ShopCreateReqVO extends ShopBaseVO {

}
