package cn.iocoder.yudao.module.shopee.convert.accounthistory;

import java.util.*;

import cn.iocoder.yudao.framework.common.pojo.PageResult;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import cn.iocoder.yudao.module.shopee.controller.admin.accounthistory.vo.*;
import cn.iocoder.yudao.module.shopee.dal.dataobject.accounthistory.AccountHistoryDO;

/**
 * 虾皮账户历史 Convert
 *
 * @author 芋道源码
 */
@Mapper
public interface AccountHistoryConvert {

    AccountHistoryConvert INSTANCE = Mappers.getMapper(AccountHistoryConvert.class);

    AccountHistoryDO convert(AccountHistoryCreateReqVO bean);

    AccountHistoryDO convert(AccountHistoryUpdateReqVO bean);

    AccountHistoryRespVO convert(AccountHistoryDO bean);

    List<AccountHistoryRespVO> convertList(List<AccountHistoryDO> list);

    PageResult<AccountHistoryRespVO> convertPage(PageResult<AccountHistoryDO> page);

    List<AccountHistoryExcelVO> convertList02(List<AccountHistoryDO> list);

}
