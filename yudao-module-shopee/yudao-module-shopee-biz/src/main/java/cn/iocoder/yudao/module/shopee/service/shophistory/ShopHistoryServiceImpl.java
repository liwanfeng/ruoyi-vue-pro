package cn.iocoder.yudao.module.shopee.service.shophistory;

import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import org.springframework.validation.annotation.Validated;

import java.util.*;
import cn.iocoder.yudao.module.shopee.controller.admin.shophistory.vo.*;
import cn.iocoder.yudao.module.shopee.dal.dataobject.shophistory.ShopHistoryDO;
import cn.iocoder.yudao.framework.common.pojo.PageResult;

import cn.iocoder.yudao.module.shopee.convert.shophistory.ShopHistoryConvert;
import cn.iocoder.yudao.module.shopee.dal.mysql.shophistory.ShopHistoryMapper;

import static cn.iocoder.yudao.framework.common.exception.util.ServiceExceptionUtil.exception;
import static cn.iocoder.yudao.module.shopee.enums.ErrorCodeConstants.*;

/**
 * 虾皮店铺历史 Service 实现类
 *
 * @author 芋道源码
 */
@Service
@Validated
public class ShopHistoryServiceImpl implements ShopHistoryService {

    @Resource
    private ShopHistoryMapper shopHistoryMapper;

    @Override
    public Long createShopHistory(ShopHistoryCreateReqVO createReqVO) {
        // 插入
        ShopHistoryDO shopHistory = ShopHistoryConvert.INSTANCE.convert(createReqVO);
        shopHistoryMapper.insert(shopHistory);
        // 返回
        return shopHistory.getId();
    }

    @Override
    public void updateShopHistory(ShopHistoryUpdateReqVO updateReqVO) {
        // 校验存在
        validateShopHistoryExists(updateReqVO.getId());
        // 更新
        ShopHistoryDO updateObj = ShopHistoryConvert.INSTANCE.convert(updateReqVO);
        shopHistoryMapper.updateById(updateObj);
    }

    @Override
    public void deleteShopHistory(Long id) {
        // 校验存在
        validateShopHistoryExists(id);
        // 删除
        shopHistoryMapper.deleteById(id);
    }

    private void validateShopHistoryExists(Long id) {
        if (shopHistoryMapper.selectById(id) == null) {
            throw exception(SHOP_HISTORY_NOT_EXISTS);
        }
    }

    @Override
    public ShopHistoryDO getShopHistory(Long id) {
        return shopHistoryMapper.selectById(id);
    }

    @Override
    public List<ShopHistoryDO> getShopHistoryList(Collection<Long> ids) {
        return shopHistoryMapper.selectBatchIds(ids);
    }

    @Override
    public PageResult<ShopHistoryDO> getShopHistoryPage(ShopHistoryPageReqVO pageReqVO) {
        return shopHistoryMapper.selectPage(pageReqVO);
    }

    @Override
    public List<ShopHistoryDO> getShopHistoryList(ShopHistoryExportReqVO exportReqVO) {
        return shopHistoryMapper.selectList(exportReqVO);
    }

}
