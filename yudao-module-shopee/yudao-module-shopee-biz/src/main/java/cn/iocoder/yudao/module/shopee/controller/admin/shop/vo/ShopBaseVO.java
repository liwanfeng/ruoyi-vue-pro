package cn.iocoder.yudao.module.shopee.controller.admin.shop.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;
import java.util.*;
import java.time.LocalDateTime;
import java.time.LocalDateTime;
import javax.validation.constraints.*;

/**
 * 虾皮店铺 Base VO，提供给添加、修改、详细的子 VO 使用
 * 如果子 VO 存在差异的字段，请不要添加到这里，影响 Swagger 文档生成
 */
@Data
public class ShopBaseVO {

    @Schema(description = "所属用户ID", example = "24767")
    private Long sysUserId;

    @Schema(description = "账户ID", example = "29544")
    private Long accountId;

    @Schema(description = "用户名", example = "赵六")
    private String username;

    @Schema(description = "用户ID", example = "24637")
    private Long userId;

    @Schema(description = "所属区域")
    private String region;

    @Schema(description = "是否启用")
    private Boolean enabled;

    @Schema(description = "是否SIP")
    private Boolean isSipAffiliated;

    @Schema(description = "店铺名称", example = "王五")
    private String shopName;

    @Schema(description = "店铺ID", example = "2547")
    private Long shopId;

    @Schema(description = "最后登录时间")
    private Long lastLogin;

    @Schema(description = "是否SIP主店铺")
    private Boolean isSipPrimary;

    @Schema(description = "描述")
    private String portrait;

    @Schema(description = "CB选项")
    private Long cbOption;

    @Schema(description = "是否不活跃")
    private Boolean isInactive;

    @Schema(description = "是否三方店铺")
    private Boolean is3PfShop;

}
