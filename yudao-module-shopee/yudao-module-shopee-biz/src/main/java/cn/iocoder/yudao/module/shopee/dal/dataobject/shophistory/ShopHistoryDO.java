package cn.iocoder.yudao.module.shopee.dal.dataobject.shophistory;

import lombok.*;
import java.util.*;
import java.time.LocalDateTime;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.*;
import cn.iocoder.yudao.framework.mybatis.core.dataobject.BaseDO;

/**
 * 虾皮店铺历史 DO
 *
 * @author 芋道源码
 */
@TableName("shopee_shop_history")
@KeySequence("shopee_shop_history_seq") // 用于 Oracle、PostgreSQL、Kingbase、DB2、H2 数据库的主键自增。如果是 MySQL 等数据库，可不写。
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ShopHistoryDO extends BaseDO {

    /**
     * 主键
     */
    @TableId
    private Long id;
    /**
     * 所属用户ID
     */
    private Long sysUserId;
    /**
     * 账户ID
     */
    private Long accountId;
    /**
     * 用户名
     */
    private String username;
    /**
     * 用户ID
     */
    private Long userId;
    /**
     * 所属区域
     */
    private String region;
    /**
     * 是否启用
     */
    private Boolean enabled;
    /**
     * 是否SIP
     */
    private Boolean isSipAffiliated;
    /**
     * 店铺名称
     */
    private String shopName;
    /**
     * 店铺ID
     */
    private Long shopId;
    /**
     * 最后登录时间
     */
    private Long lastLogin;
    /**
     * 是否SIP主店铺
     */
    private Boolean isSipPrimary;
    /**
     * 描述
     */
    private String portrait;
    /**
     * CB选项
     */
    private Long cbOption;
    /**
     * 是否不活跃
     */
    private Boolean isInactive;
    /**
     * 是否三方店铺
     */
    private Boolean is3PfShop;

}
