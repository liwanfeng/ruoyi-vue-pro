package cn.iocoder.yudao.module.shopee.controller.admin.account.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;
import java.util.*;
import java.time.LocalDateTime;
import java.time.LocalDateTime;

import com.alibaba.excel.annotation.ExcelProperty;

/**
 * 虾皮账户 Excel VO
 *
 * @author 芋道源码
 */
@Data
public class AccountExcelVO {

    @ExcelProperty("主键")
    private Long id;

    @ExcelProperty("所属用户ID")
    private Long sysUserId;

    @ExcelProperty("账户ID")
    private Long accountId;

    @ExcelProperty("账户类型")
    private String accountType;

    @ExcelProperty("TOB账户ID")
    private Long tobAccountId;

    @ExcelProperty("联系电话")
    private String phone;

    @ExcelProperty("描述")
    private String portrait;

    @ExcelProperty("账号")
    private String accountName;

    @ExcelProperty("是否逾期")
    private Boolean isOverdue;

    @ExcelProperty("语言")
    private String language;

    @ExcelProperty("用户昵称")
    private String nickName;

    @ExcelProperty("主账户ID")
    private Long mainAccountId;

    @ExcelProperty("子账户ID")
    private Long subAccountId;

    @ExcelProperty("邮件地址")
    private String email;

    @ExcelProperty("商家名称")
    private String merchantName;

    @ExcelProperty("创建时间")
    private LocalDateTime createTime;

}
