package cn.iocoder.yudao.module.shopee.controller.admin.shop;

import cn.iocoder.yudao.framework.common.pojo.CommonResult;
import cn.iocoder.yudao.framework.common.pojo.PageResult;
import cn.iocoder.yudao.framework.excel.core.util.ExcelUtils;
import cn.iocoder.yudao.framework.operatelog.core.annotations.OperateLog;
import cn.iocoder.yudao.module.shopee.controller.admin.account.vo.AccountExportReqVO;
import cn.iocoder.yudao.module.shopee.controller.admin.shop.vo.*;
import cn.iocoder.yudao.module.shopee.convert.shop.ShopConvert;
import cn.iocoder.yudao.module.shopee.dal.dataobject.account.AccountDO;
import cn.iocoder.yudao.module.shopee.dal.dataobject.shop.ShopDO;
import cn.iocoder.yudao.module.shopee.service.account.AccountService;
import cn.iocoder.yudao.module.shopee.service.shop.ShopService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import static cn.iocoder.yudao.framework.common.pojo.CommonResult.success;
import static cn.iocoder.yudao.framework.operatelog.core.enums.OperateTypeEnum.EXPORT;

@Tag(name = "管理后台 - 虾皮店铺")
@RestController
@RequestMapping("/shopee/shop")
@Validated
public class ShopController {

    @Resource
    private ShopService shopService;
    @Resource
    private AccountService accountService;

    @PostMapping("/create")
    @Operation(summary = "创建虾皮店铺")
    @PreAuthorize("@ss.hasPermission('shopee:shop:create')")
    public CommonResult<Long> createShop(@Valid @RequestBody ShopCreateReqVO createReqVO) {
        return success(shopService.createShop(createReqVO));
    }

    @PutMapping("/update")
    @Operation(summary = "更新虾皮店铺")
    @PreAuthorize("@ss.hasPermission('shopee:shop:update')")
    public CommonResult<Boolean> updateShop(@Valid @RequestBody ShopUpdateReqVO updateReqVO) {
        shopService.updateShop(updateReqVO);
        return success(true);
    }

    @DeleteMapping("/delete")
    @Operation(summary = "删除虾皮店铺")
    @Parameter(name = "id", description = "编号", required = true)
    @PreAuthorize("@ss.hasPermission('shopee:shop:delete')")
    public CommonResult<Boolean> deleteShop(@RequestParam("id") Long id) {
        shopService.deleteShop(id);
        return success(true);
    }

    @GetMapping("/get")
    @Operation(summary = "获得虾皮店铺")
    @Parameter(name = "id", description = "编号", required = true, example = "1024")
    @PreAuthorize("@ss.hasPermission('shopee:shop:query')")
    public CommonResult<ShopRespVO> getShop(@RequestParam("id") Long id) {
        ShopDO shop = shopService.getShop(id);
        return success(ShopConvert.INSTANCE.convert(shop));
    }

    @GetMapping("/list")
    @Operation(summary = "获得虾皮店铺列表")
    @Parameter(name = "ids", description = "编号列表", required = true, example = "1024,2048")
    @PreAuthorize("@ss.hasPermission('shopee:shop:query')")
    public CommonResult<List<ShopRespVO>> getShopList(@RequestParam("ids") Collection<Long> ids) {
        List<ShopDO> list = shopService.getShopList(ids);
        return success(ShopConvert.INSTANCE.convertList(list));
    }

    @GetMapping("/page")
    @Operation(summary = "获得虾皮店铺分页")
    @PreAuthorize("@ss.hasPermission('shopee:shop:query')")
    public CommonResult<PageResult<ShopRespVO>> getShopPage(@Valid ShopPageReqVO pageVO) {
        PageResult<ShopDO> pageResult = shopService.getShopPage(pageVO);
        return success(ShopConvert.INSTANCE.convertPage(pageResult));
    }

    @GetMapping("/export-excel")
    @Operation(summary = "导出虾皮店铺 Excel")
    @PreAuthorize("@ss.hasPermission('shopee:shop:export')")
    @OperateLog(type = EXPORT)
    public void exportShopExcel(@Valid ShopExportReqVO exportReqVO,
              HttpServletResponse response) throws IOException {
        List<ShopDO> list = shopService.getShopList(exportReqVO);
        // 导出 Excel
        List<ShopExcelVO> datas = ShopConvert.INSTANCE.convertList02(list);
        ExcelUtils.write(response, "虾皮店铺.xls", "数据", ShopExcelVO.class, datas);
    }

    @GetMapping("/refreshAllShop")
    @Operation(summary = "更新店铺下全部商品")
    @PreAuthorize("@ss.hasPermission('shopee:shop:update')")
    public CommonResult<String> refreshAllShop() {
        List<AccountDO> accountList = accountService.getAccountList(new AccountExportReqVO());
        shopService.upsertFromRemote(accountList.stream().map(AccountDO::getAccountId).collect(Collectors.toList()));
        return success("更新成功");
    }

}
