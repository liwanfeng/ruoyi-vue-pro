package cn.iocoder.yudao.module.shopee.controller.admin.account.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;
import java.util.*;
import javax.validation.constraints.*;

@Schema(description = "管理后台 - 虾皮账户更新 Request VO")
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class AccountUpdateReqVO extends AccountBaseVO {

    @Schema(description = "主键", requiredMode = Schema.RequiredMode.REQUIRED, example = "5779")
    @NotNull(message = "主键不能为空")
    private Long id;
    /**
     * Cookie信息
     */
    private String cookie;

}
