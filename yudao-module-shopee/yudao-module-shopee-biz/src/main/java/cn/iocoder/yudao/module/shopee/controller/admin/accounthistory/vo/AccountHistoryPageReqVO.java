package cn.iocoder.yudao.module.shopee.controller.admin.accounthistory.vo;

import lombok.*;
import java.util.*;
import io.swagger.v3.oas.annotations.media.Schema;
import cn.iocoder.yudao.framework.common.pojo.PageParam;
import org.springframework.format.annotation.DateTimeFormat;
import java.time.LocalDateTime;

import static cn.iocoder.yudao.framework.common.util.date.DateUtils.FORMAT_YEAR_MONTH_DAY_HOUR_MINUTE_SECOND;

@Schema(description = "管理后台 - 虾皮账户历史分页 Request VO")
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class AccountHistoryPageReqVO extends PageParam {

    @Schema(description = "所属用户ID", example = "12522")
    private Long sysUserId;

    @Schema(description = "账户ID", example = "27806")
    private Long accountId;

    @Schema(description = "账户类型", example = "2")
    private String accountType;

    @Schema(description = "TOB账户ID", example = "30984")
    private Long tobAccountId;

    @Schema(description = "联系电话")
    private String phone;

    @Schema(description = "描述")
    private String portrait;

    @Schema(description = "账号", example = "赵六")
    private String accountName;

    @Schema(description = "是否逾期")
    private Boolean isOverdue;

    @Schema(description = "语言")
    private String language;

    @Schema(description = "用户昵称", example = "王五")
    private String nickName;

    @Schema(description = "主账户ID", example = "364")
    private Long mainAccountId;

    @Schema(description = "子账户ID", example = "16637")
    private Long subAccountId;

    @Schema(description = "邮件地址")
    private String email;

    @Schema(description = "商家名称", example = "王五")
    private String merchantName;

    @Schema(description = "创建时间")
    @DateTimeFormat(pattern = FORMAT_YEAR_MONTH_DAY_HOUR_MINUTE_SECOND)
    private LocalDateTime[] createTime;

}
