package cn.iocoder.yudao.module.shopee.dal.dataobject.accounthistory;

import lombok.*;
import java.util.*;
import java.time.LocalDateTime;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.*;
import cn.iocoder.yudao.framework.mybatis.core.dataobject.BaseDO;

/**
 * 虾皮账户历史 DO
 *
 * @author 芋道源码
 */
@TableName("shopee_account_history")
@KeySequence("shopee_account_history_seq") // 用于 Oracle、PostgreSQL、Kingbase、DB2、H2 数据库的主键自增。如果是 MySQL 等数据库，可不写。
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AccountHistoryDO extends BaseDO {

    /**
     * 主键
     */
    @TableId
    private Long id;
    /**
     * 所属用户ID
     */
    private Long sysUserId;
    /**
     * 账户ID
     */
    private Long accountId;
    /**
     * 账户类型
     */
    private String accountType;
    /**
     * TOB账户ID
     */
    private Long tobAccountId;
    /**
     * 联系电话
     */
    private String phone;
    /**
     * 描述
     */
    private String portrait;
    /**
     * 账号
     */
    private String accountName;
    /**
     * 是否逾期
     */
    private Boolean isOverdue;
    /**
     * 语言
     */
    private String language;
    /**
     * 用户昵称
     */
    private String nickName;
    /**
     * 主账户ID
     */
    private Long mainAccountId;
    /**
     * 子账户ID
     */
    private Long subAccountId;
    /**
     * 邮件地址
     */
    private String email;
    /**
     * 商家名称
     */
    private String merchantName;

}
