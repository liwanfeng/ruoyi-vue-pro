package cn.iocoder.yudao.module.shopee.dal.mysql.product;

import java.util.*;

import cn.iocoder.yudao.framework.common.pojo.PageResult;
import cn.iocoder.yudao.framework.mybatis.core.query.LambdaQueryWrapperX;
import cn.iocoder.yudao.framework.mybatis.core.mapper.BaseMapperX;
import cn.iocoder.yudao.module.shopee.dal.dataobject.product.ProductDO;
import org.apache.ibatis.annotations.Mapper;
import cn.iocoder.yudao.module.shopee.controller.admin.product.vo.*;
import org.apache.ibatis.annotations.Param;

/**
 * 虾皮商品 Mapper
 *
 * @author 芋道源码
 */
@Mapper
public interface ProductMapper extends BaseMapperX<ProductDO> {

    default PageResult<ProductDO> selectPage(ProductPageReqVO reqVO) {
        return selectPage(reqVO, new LambdaQueryWrapperX<ProductDO>()
                .eqIfPresent(ProductDO::getSysUserId, reqVO.getSysUserId())
                .eqIfPresent(ProductDO::getAccountId, reqVO.getAccountId())
                .eqIfPresent(ProductDO::getShopId, reqVO.getShopId())
                .eqIfPresent(ProductDO::getSkuId, reqVO.getSkuId())
                .eqIfPresent(ProductDO::getAddOnDeal, reqVO.getAddOnDeal())
                .eqIfPresent(ProductDO::getBoostCoolDownSeconds, reqVO.getBoostCoolDownSeconds())
                .eqIfPresent(ProductDO::getDeboosted, reqVO.getDeboosted())
                .eqIfPresent(ProductDO::getFlag, reqVO.getFlag())
                .eqIfPresent(ProductDO::getInPromotion, reqVO.getInPromotion())
                .eqIfPresent(ProductDO::getLikeCount, reqVO.getLikeCount())
                .likeIfPresent(ProductDO::getName, reqVO.getName())
                .eqIfPresent(ProductDO::getParentSku, reqVO.getParentSku())
                .eqIfPresent(ProductDO::getSold, reqVO.getSold())
                .eqIfPresent(ProductDO::getStatus, reqVO.getStatus())
                .eqIfPresent(ProductDO::getSellerCurrentStock, reqVO.getSellerCurrentStock())
                .eqIfPresent(ProductDO::getViewCount, reqVO.getViewCount())
                .eqIfPresent(ProductDO::getWholesale, reqVO.getWholesale())
                .eqIfPresent(ProductDO::getCategoryStatus, reqVO.getCategoryStatus())
                .eqIfPresent(ProductDO::getDaysToShip, reqVO.getDaysToShip())
                .eqIfPresent(ProductDO::getPreOrder, reqVO.getPreOrder())
                .eqIfPresent(ProductDO::getBrandId, reqVO.getBrandId())
                .eqIfPresent(ProductDO::getBrand, reqVO.getBrand())
                .betweenIfPresent(ProductDO::getModifyTime, reqVO.getModifyTime())
                .eqIfPresent(ProductDO::getIsB2CSkuInFbsShop, reqVO.getIsB2CSkuInFbsShop())
                .eqIfPresent(ProductDO::getMtskuItemId, reqVO.getMtskuItemId())
                .eqIfPresent(ProductDO::getCategory1, reqVO.getCategory1())
                .eqIfPresent(ProductDO::getCategory2, reqVO.getCategory2())
                .eqIfPresent(ProductDO::getCategory3, reqVO.getCategory3())
                .eqIfPresent(ProductDO::getOngoingUpcomingCampaigns, reqVO.getOngoingUpcomingCampaigns())
                .betweenIfPresent(ProductDO::getCreateTime, reqVO.getCreateTime())
                .orderByDesc(ProductDO::getId));
    }

    default List<ProductDO> selectList(ProductExportReqVO reqVO) {
        return selectList(new LambdaQueryWrapperX<ProductDO>()
                .eqIfPresent(ProductDO::getSysUserId, reqVO.getSysUserId())
                .eqIfPresent(ProductDO::getAccountId, reqVO.getAccountId())
                .eqIfPresent(ProductDO::getShopId, reqVO.getShopId())
                .eqIfPresent(ProductDO::getSkuId, reqVO.getSkuId())
                .eqIfPresent(ProductDO::getAddOnDeal, reqVO.getAddOnDeal())
                .eqIfPresent(ProductDO::getBoostCoolDownSeconds, reqVO.getBoostCoolDownSeconds())
                .eqIfPresent(ProductDO::getDeboosted, reqVO.getDeboosted())
                .eqIfPresent(ProductDO::getFlag, reqVO.getFlag())
                .eqIfPresent(ProductDO::getInPromotion, reqVO.getInPromotion())
                .eqIfPresent(ProductDO::getLikeCount, reqVO.getLikeCount())
                .likeIfPresent(ProductDO::getName, reqVO.getName())
                .eqIfPresent(ProductDO::getParentSku, reqVO.getParentSku())
                .eqIfPresent(ProductDO::getSold, reqVO.getSold())
                .eqIfPresent(ProductDO::getStatus, reqVO.getStatus())
                .eqIfPresent(ProductDO::getSellerCurrentStock, reqVO.getSellerCurrentStock())
                .eqIfPresent(ProductDO::getViewCount, reqVO.getViewCount())
                .eqIfPresent(ProductDO::getWholesale, reqVO.getWholesale())
                .eqIfPresent(ProductDO::getCategoryStatus, reqVO.getCategoryStatus())
                .eqIfPresent(ProductDO::getDaysToShip, reqVO.getDaysToShip())
                .eqIfPresent(ProductDO::getPreOrder, reqVO.getPreOrder())
                .eqIfPresent(ProductDO::getBrandId, reqVO.getBrandId())
                .eqIfPresent(ProductDO::getBrand, reqVO.getBrand())
                .betweenIfPresent(ProductDO::getModifyTime, reqVO.getModifyTime())
                .eqIfPresent(ProductDO::getIsB2CSkuInFbsShop, reqVO.getIsB2CSkuInFbsShop())
                .eqIfPresent(ProductDO::getMtskuItemId, reqVO.getMtskuItemId())
                .eqIfPresent(ProductDO::getCategory1, reqVO.getCategory1())
                .eqIfPresent(ProductDO::getCategory2, reqVO.getCategory2())
                .eqIfPresent(ProductDO::getCategory3, reqVO.getCategory3())
                .eqIfPresent(ProductDO::getOngoingUpcomingCampaigns, reqVO.getOngoingUpcomingCampaigns())
                .betweenIfPresent(ProductDO::getCreateTime, reqVO.getCreateTime())
                .orderByDesc(ProductDO::getId));
    }

    void upsert(@Param("ew") ProductDO productDO);

}
