package cn.iocoder.yudao.module.shopee.controller.admin.shophistory.vo;

import lombok.*;
import java.util.*;
import io.swagger.v3.oas.annotations.media.Schema;
import cn.iocoder.yudao.framework.common.pojo.PageParam;
import java.time.LocalDateTime;
import org.springframework.format.annotation.DateTimeFormat;

import static cn.iocoder.yudao.framework.common.util.date.DateUtils.FORMAT_YEAR_MONTH_DAY_HOUR_MINUTE_SECOND;

@Schema(description = "管理后台 - 虾皮店铺历史 Excel 导出 Request VO，参数和 ShopHistoryPageReqVO 是一致的")
@Data
public class ShopHistoryExportReqVO {

    @Schema(description = "所属用户ID", example = "31996")
    private Long sysUserId;

    @Schema(description = "账户ID", example = "7947")
    private Long accountId;

    @Schema(description = "用户名", example = "王五")
    private String username;

    @Schema(description = "用户ID", example = "5569")
    private Long userId;

    @Schema(description = "所属区域")
    private String region;

    @Schema(description = "是否启用")
    private Boolean enabled;

    @Schema(description = "是否SIP")
    private Boolean isSipAffiliated;

    @Schema(description = "店铺名称", example = "张三")
    private String shopName;

    @Schema(description = "店铺ID", example = "6476")
    private Long shopId;

    @Schema(description = "最后登录时间")
    private Long lastLogin;

    @Schema(description = "是否SIP主店铺")
    private Boolean isSipPrimary;

    @Schema(description = "描述")
    private String portrait;

    @Schema(description = "CB选项")
    private Long cbOption;

    @Schema(description = "是否不活跃")
    private Boolean isInactive;

    @Schema(description = "是否三方店铺")
    private Boolean is3PfShop;

    @Schema(description = "创建时间")
    @DateTimeFormat(pattern = FORMAT_YEAR_MONTH_DAY_HOUR_MINUTE_SECOND)
    private LocalDateTime[] createTime;

}
