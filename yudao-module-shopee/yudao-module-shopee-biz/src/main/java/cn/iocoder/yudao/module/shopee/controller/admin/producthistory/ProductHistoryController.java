package cn.iocoder.yudao.module.shopee.controller.admin.producthistory;

import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import org.springframework.validation.annotation.Validated;
import org.springframework.security.access.prepost.PreAuthorize;
import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Operation;

import javax.validation.constraints.*;
import javax.validation.*;
import javax.servlet.http.*;
import java.util.*;
import java.io.IOException;

import cn.iocoder.yudao.framework.common.pojo.PageResult;
import cn.iocoder.yudao.framework.common.pojo.CommonResult;
import static cn.iocoder.yudao.framework.common.pojo.CommonResult.success;

import cn.iocoder.yudao.framework.excel.core.util.ExcelUtils;

import cn.iocoder.yudao.framework.operatelog.core.annotations.OperateLog;
import static cn.iocoder.yudao.framework.operatelog.core.enums.OperateTypeEnum.*;

import cn.iocoder.yudao.module.shopee.controller.admin.producthistory.vo.*;
import cn.iocoder.yudao.module.shopee.dal.dataobject.producthistory.ProductHistoryDO;
import cn.iocoder.yudao.module.shopee.convert.producthistory.ProductHistoryConvert;
import cn.iocoder.yudao.module.shopee.service.producthistory.ProductHistoryService;

@Tag(name = "管理后台 - 虾皮商品历史")
@RestController
@RequestMapping("/shopee/product-history")
@Validated
public class ProductHistoryController {

    @Resource
    private ProductHistoryService productHistoryService;

    @PostMapping("/create")
    @Operation(summary = "创建虾皮商品历史")
    @PreAuthorize("@ss.hasPermission('shopee:product-history:create')")
    public CommonResult<Long> createProductHistory(@Valid @RequestBody ProductHistoryCreateReqVO createReqVO) {
        return success(productHistoryService.createProductHistory(createReqVO));
    }

    @PutMapping("/update")
    @Operation(summary = "更新虾皮商品历史")
    @PreAuthorize("@ss.hasPermission('shopee:product-history:update')")
    public CommonResult<Boolean> updateProductHistory(@Valid @RequestBody ProductHistoryUpdateReqVO updateReqVO) {
        productHistoryService.updateProductHistory(updateReqVO);
        return success(true);
    }

    @DeleteMapping("/delete")
    @Operation(summary = "删除虾皮商品历史")
    @Parameter(name = "id", description = "编号", required = true)
    @PreAuthorize("@ss.hasPermission('shopee:product-history:delete')")
    public CommonResult<Boolean> deleteProductHistory(@RequestParam("id") Long id) {
        productHistoryService.deleteProductHistory(id);
        return success(true);
    }

    @GetMapping("/get")
    @Operation(summary = "获得虾皮商品历史")
    @Parameter(name = "id", description = "编号", required = true, example = "1024")
    @PreAuthorize("@ss.hasPermission('shopee:product-history:query')")
    public CommonResult<ProductHistoryRespVO> getProductHistory(@RequestParam("id") Long id) {
        ProductHistoryDO productHistory = productHistoryService.getProductHistory(id);
        return success(ProductHistoryConvert.INSTANCE.convert(productHistory));
    }

    @GetMapping("/list")
    @Operation(summary = "获得虾皮商品历史列表")
    @Parameter(name = "ids", description = "编号列表", required = true, example = "1024,2048")
    @PreAuthorize("@ss.hasPermission('shopee:product-history:query')")
    public CommonResult<List<ProductHistoryRespVO>> getProductHistoryList(@RequestParam("ids") Collection<Long> ids) {
        List<ProductHistoryDO> list = productHistoryService.getProductHistoryList(ids);
        return success(ProductHistoryConvert.INSTANCE.convertList(list));
    }

    @GetMapping("/page")
    @Operation(summary = "获得虾皮商品历史分页")
    @PreAuthorize("@ss.hasPermission('shopee:product-history:query')")
    public CommonResult<PageResult<ProductHistoryRespVO>> getProductHistoryPage(@Valid ProductHistoryPageReqVO pageVO) {
        PageResult<ProductHistoryDO> pageResult = productHistoryService.getProductHistoryPage(pageVO);
        return success(ProductHistoryConvert.INSTANCE.convertPage(pageResult));
    }

    @GetMapping("/export-excel")
    @Operation(summary = "导出虾皮商品历史 Excel")
    @PreAuthorize("@ss.hasPermission('shopee:product-history:export')")
    @OperateLog(type = EXPORT)
    public void exportProductHistoryExcel(@Valid ProductHistoryExportReqVO exportReqVO,
              HttpServletResponse response) throws IOException {
        List<ProductHistoryDO> list = productHistoryService.getProductHistoryList(exportReqVO);
        // 导出 Excel
        List<ProductHistoryExcelVO> datas = ProductHistoryConvert.INSTANCE.convertList02(list);
        ExcelUtils.write(response, "虾皮商品历史.xls", "数据", ProductHistoryExcelVO.class, datas);
    }

}
