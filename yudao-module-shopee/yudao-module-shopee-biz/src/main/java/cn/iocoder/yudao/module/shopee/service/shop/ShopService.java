package cn.iocoder.yudao.module.shopee.service.shop;

import java.util.*;
import javax.validation.*;
import cn.iocoder.yudao.module.shopee.controller.admin.shop.vo.*;
import cn.iocoder.yudao.module.shopee.dal.dataobject.shop.ShopDO;
import cn.iocoder.yudao.framework.common.pojo.PageResult;

/**
 * 虾皮店铺 Service 接口
 *
 * @author 芋道源码
 */
public interface ShopService {

    /**
     * 创建虾皮店铺
     *
     * @param createReqVO 创建信息
     * @return 编号
     */
    Long createShop(@Valid ShopCreateReqVO createReqVO);

    /**
     * 更新虾皮店铺
     *
     * @param updateReqVO 更新信息
     */
    void updateShop(@Valid ShopUpdateReqVO updateReqVO);

    /**
     * 删除虾皮店铺
     *
     * @param id 编号
     */
    void deleteShop(Long id);

    /**
     * 获得虾皮店铺
     *
     * @param id 编号
     * @return 虾皮店铺
     */
    ShopDO getShop(Long id);

    /**
     * 获得虾皮店铺列表
     *
     * @param ids 编号
     * @return 虾皮店铺列表
     */
    List<ShopDO> getShopList(Collection<Long> ids);

    /**
     * 获得虾皮店铺分页
     *
     * @param pageReqVO 分页查询
     * @return 虾皮店铺分页
     */
    PageResult<ShopDO> getShopPage(ShopPageReqVO pageReqVO);

    /**
     * 获得虾皮店铺列表, 用于 Excel 导出
     *
     * @param exportReqVO 查询条件
     * @return 虾皮店铺列表
     */
    List<ShopDO> getShopList(ShopExportReqVO exportReqVO);

    /**
     * 按照账户ID更新全部店铺信息
     * @param accountIdList shopee账户id列表
     */
    void upsertFromRemote(List<Long> accountIdList);
}
