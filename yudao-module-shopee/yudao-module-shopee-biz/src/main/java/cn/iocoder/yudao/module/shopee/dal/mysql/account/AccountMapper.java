package cn.iocoder.yudao.module.shopee.dal.mysql.account;

import java.util.*;

import cn.iocoder.yudao.framework.common.pojo.PageResult;
import cn.iocoder.yudao.framework.mybatis.core.query.LambdaQueryWrapperX;
import cn.iocoder.yudao.framework.mybatis.core.mapper.BaseMapperX;
import cn.iocoder.yudao.module.shopee.dal.dataobject.account.AccountDO;
import org.apache.ibatis.annotations.Mapper;
import cn.iocoder.yudao.module.shopee.controller.admin.account.vo.*;
import org.apache.ibatis.annotations.Param;

/**
 * 虾皮账户 Mapper
 *
 * @author 芋道源码
 */
@Mapper
public interface AccountMapper extends BaseMapperX<AccountDO> {

    default PageResult<AccountDO> selectPage(AccountPageReqVO reqVO) {
        return selectPage(reqVO, new LambdaQueryWrapperX<AccountDO>()
                .eqIfPresent(AccountDO::getSysUserId, reqVO.getSysUserId())
                .eqIfPresent(AccountDO::getAccountId, reqVO.getAccountId())
                .eqIfPresent(AccountDO::getAccountType, reqVO.getAccountType())
                .eqIfPresent(AccountDO::getTobAccountId, reqVO.getTobAccountId())
                .eqIfPresent(AccountDO::getPhone, reqVO.getPhone())
                .eqIfPresent(AccountDO::getPortrait, reqVO.getPortrait())
                .likeIfPresent(AccountDO::getAccountName, reqVO.getAccountName())
                .eqIfPresent(AccountDO::getIsOverdue, reqVO.getIsOverdue())
                .eqIfPresent(AccountDO::getLanguage, reqVO.getLanguage())
                .likeIfPresent(AccountDO::getNickName, reqVO.getNickName())
                .eqIfPresent(AccountDO::getMainAccountId, reqVO.getMainAccountId())
                .eqIfPresent(AccountDO::getSubAccountId, reqVO.getSubAccountId())
                .eqIfPresent(AccountDO::getEmail, reqVO.getEmail())
                .likeIfPresent(AccountDO::getMerchantName, reqVO.getMerchantName())
                .betweenIfPresent(AccountDO::getCreateTime, reqVO.getCreateTime())
                .orderByDesc(AccountDO::getId));
    }

    default List<AccountDO> selectList(AccountExportReqVO reqVO) {
        return selectList(new LambdaQueryWrapperX<AccountDO>()
                .eqIfPresent(AccountDO::getSysUserId, reqVO.getSysUserId())
                .eqIfPresent(AccountDO::getAccountId, reqVO.getAccountId())
                .eqIfPresent(AccountDO::getAccountType, reqVO.getAccountType())
                .eqIfPresent(AccountDO::getTobAccountId, reqVO.getTobAccountId())
                .eqIfPresent(AccountDO::getPhone, reqVO.getPhone())
                .eqIfPresent(AccountDO::getPortrait, reqVO.getPortrait())
                .likeIfPresent(AccountDO::getAccountName, reqVO.getAccountName())
                .eqIfPresent(AccountDO::getIsOverdue, reqVO.getIsOverdue())
                .eqIfPresent(AccountDO::getLanguage, reqVO.getLanguage())
                .likeIfPresent(AccountDO::getNickName, reqVO.getNickName())
                .eqIfPresent(AccountDO::getMainAccountId, reqVO.getMainAccountId())
                .eqIfPresent(AccountDO::getSubAccountId, reqVO.getSubAccountId())
                .eqIfPresent(AccountDO::getEmail, reqVO.getEmail())
                .likeIfPresent(AccountDO::getMerchantName, reqVO.getMerchantName())
                .betweenIfPresent(AccountDO::getCreateTime, reqVO.getCreateTime())
                .orderByDesc(AccountDO::getId));
    }

    Long upsertAccount(@Param("ew") AccountDO createReqVO);

}
