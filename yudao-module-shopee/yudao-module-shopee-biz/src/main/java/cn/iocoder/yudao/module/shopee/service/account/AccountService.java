package cn.iocoder.yudao.module.shopee.service.account;

import java.util.*;
import javax.validation.*;
import cn.iocoder.yudao.module.shopee.controller.admin.account.vo.*;
import cn.iocoder.yudao.module.shopee.dal.dataobject.account.AccountDO;
import cn.iocoder.yudao.framework.common.pojo.PageResult;

/**
 * 虾皮账户 Service 接口
 *
 * @author 芋道源码
 */
public interface AccountService {

    /**
     * 创建虾皮账户
     *
     * @param createReqVO 创建信息
     * @return 编号
     */
    Long createAccount(@Valid AccountCreateReqVO createReqVO);

    /**
     * 更新虾皮账户
     *
     * @param updateReqVO 更新信息
     */
    void updateAccount(@Valid AccountUpdateReqVO updateReqVO);

    /**
     * 删除虾皮账户
     *
     * @param id 编号
     */
    void deleteAccount(Long id);

    /**
     * 获得虾皮账户
     *
     * @param id 编号
     * @return 虾皮账户
     */
    AccountDO getAccount(Long id);

    /**
     * 获得虾皮账户列表
     *
     * @param ids 编号
     * @return 虾皮账户列表
     */
    List<AccountDO> getAccountList(Collection<Long> ids);

    /**
     * 获得虾皮账户分页
     *
     * @param pageReqVO 分页查询
     * @return 虾皮账户分页
     */
    PageResult<AccountDO> getAccountPage(AccountPageReqVO pageReqVO);

    /**
     * 获得虾皮账户列表, 用于 Excel 导出
     *
     * @param exportReqVO 查询条件
     * @return 虾皮账户列表
     */
    List<AccountDO> getAccountList(AccountExportReqVO exportReqVO);

    /**
     * 创建虾皮账户
     *
     * @param createReqVO 创建信息
     * @return 编号
     */
    Long upsertAccount(AccountCreateReqVO createReqVO);

    /**
     * 通过爬虫同步本地信息
     * @param cookie cookie信息
     * @return 账户ID
     */
    Long updateRemoteAccount(String cookie);

}
