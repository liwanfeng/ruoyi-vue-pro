package cn.iocoder.yudao.module.shopee.controller.admin.product;

import cn.iocoder.yudao.framework.common.pojo.CommonResult;
import cn.iocoder.yudao.framework.common.pojo.PageResult;
import cn.iocoder.yudao.framework.excel.core.util.ExcelUtils;
import cn.iocoder.yudao.framework.operatelog.core.annotations.OperateLog;
import cn.iocoder.yudao.module.shopee.controller.admin.product.vo.*;
import cn.iocoder.yudao.module.shopee.controller.admin.shop.vo.ShopExportReqVO;
import cn.iocoder.yudao.module.shopee.convert.product.ProductConvert;
import cn.iocoder.yudao.module.shopee.dal.dataobject.product.ProductDO;
import cn.iocoder.yudao.module.shopee.dal.dataobject.shop.ShopDO;
import cn.iocoder.yudao.module.shopee.service.product.ProductService;
import cn.iocoder.yudao.module.shopee.service.shop.ShopService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.util.Collection;
import java.util.List;

import static cn.iocoder.yudao.framework.common.pojo.CommonResult.success;
import static cn.iocoder.yudao.framework.operatelog.core.enums.OperateTypeEnum.EXPORT;

@Tag(name = "管理后台 - 虾皮商品")
@RestController
@RequestMapping("/shopee/product")
@Validated
public class ProductController {

    @Resource
    private ProductService productService;
    @Resource
    private ShopService shopService;

    @PostMapping("/create")
    @Operation(summary = "创建虾皮商品")
    @PreAuthorize("@ss.hasPermission('shopee:product:create')")
    public CommonResult<Long> createProduct(@Valid @RequestBody ProductCreateReqVO createReqVO) {
        return success(productService.createProduct(createReqVO));
    }

    @PutMapping("/update")
    @Operation(summary = "更新虾皮商品")
    @PreAuthorize("@ss.hasPermission('shopee:product:update')")
    public CommonResult<Boolean> updateProduct(@Valid @RequestBody ProductUpdateReqVO updateReqVO) {
        productService.updateProduct(updateReqVO);
        return success(true);
    }

    @DeleteMapping("/delete")
    @Operation(summary = "删除虾皮商品")
    @Parameter(name = "id", description = "编号", required = true)
    @PreAuthorize("@ss.hasPermission('shopee:product:delete')")
    public CommonResult<Boolean> deleteProduct(@RequestParam("id") Long id) {
        productService.deleteProduct(id);
        return success(true);
    }

    @GetMapping("/get")
    @Operation(summary = "获得虾皮商品")
    @Parameter(name = "id", description = "编号", required = true, example = "1024")
    @PreAuthorize("@ss.hasPermission('shopee:product:query')")
    public CommonResult<ProductRespVO> getProduct(@RequestParam("id") Long id) {
        ProductDO product = productService.getProduct(id);
        return success(ProductConvert.INSTANCE.convert(product));
    }

    @GetMapping("/refreshAll")
    @Operation(summary = "更新店铺下全部商品")
    @PreAuthorize("@ss.hasPermission('shopee:product:update')")
    public CommonResult<String> refreshAllProduct() {
        List<ShopDO> shopList = shopService.getShopList(new ShopExportReqVO());
        productService.updateFromRemote(shopList);
        return success("刷新成功");
    }

    @GetMapping("/list")
    @Operation(summary = "获得虾皮商品列表")
    @Parameter(name = "ids", description = "编号列表", required = true, example = "1024,2048")
    @PreAuthorize("@ss.hasPermission('shopee:product:query')")
    public CommonResult<List<ProductRespVO>> getProductList(@RequestParam("ids") Collection<Long> ids) {
        List<ProductDO> list = productService.getProductList(ids);
        return success(ProductConvert.INSTANCE.convertList(list));
    }

    @GetMapping("/page")
    @Operation(summary = "获得虾皮商品分页")
    @PreAuthorize("@ss.hasPermission('shopee:product:query')")
    public CommonResult<PageResult<ProductRespVO>> getProductPage(@Valid ProductPageReqVO pageVO) {
        PageResult<ProductDO> pageResult = productService.getProductPage(pageVO);
        return success(ProductConvert.INSTANCE.convertPage(pageResult));
    }

    @GetMapping("/export-excel")
    @Operation(summary = "导出虾皮商品 Excel")
    @PreAuthorize("@ss.hasPermission('shopee:product:export')")
    @OperateLog(type = EXPORT)
    public void exportProductExcel(@Valid ProductExportReqVO exportReqVO,
                                   HttpServletResponse response) throws IOException {
        List<ProductDO> list = productService.getProductList(exportReqVO);
        // 导出 Excel
        List<ProductExcelVO> datas = ProductConvert.INSTANCE.convertList02(list);
        ExcelUtils.write(response, "虾皮商品.xls", "数据", ProductExcelVO.class, datas);
    }

}
