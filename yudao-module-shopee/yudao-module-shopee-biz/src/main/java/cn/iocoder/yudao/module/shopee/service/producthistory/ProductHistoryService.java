package cn.iocoder.yudao.module.shopee.service.producthistory;

import java.util.*;
import javax.validation.*;
import cn.iocoder.yudao.module.shopee.controller.admin.producthistory.vo.*;
import cn.iocoder.yudao.module.shopee.dal.dataobject.producthistory.ProductHistoryDO;
import cn.iocoder.yudao.framework.common.pojo.PageResult;

/**
 * 虾皮商品历史 Service 接口
 *
 * @author 芋道源码
 */
public interface ProductHistoryService {

    /**
     * 创建虾皮商品历史
     *
     * @param createReqVO 创建信息
     * @return 编号
     */
    Long createProductHistory(@Valid ProductHistoryCreateReqVO createReqVO);

    /**
     * 更新虾皮商品历史
     *
     * @param updateReqVO 更新信息
     */
    void updateProductHistory(@Valid ProductHistoryUpdateReqVO updateReqVO);

    /**
     * 删除虾皮商品历史
     *
     * @param id 编号
     */
    void deleteProductHistory(Long id);

    /**
     * 获得虾皮商品历史
     *
     * @param id 编号
     * @return 虾皮商品历史
     */
    ProductHistoryDO getProductHistory(Long id);

    /**
     * 获得虾皮商品历史列表
     *
     * @param ids 编号
     * @return 虾皮商品历史列表
     */
    List<ProductHistoryDO> getProductHistoryList(Collection<Long> ids);

    /**
     * 获得虾皮商品历史分页
     *
     * @param pageReqVO 分页查询
     * @return 虾皮商品历史分页
     */
    PageResult<ProductHistoryDO> getProductHistoryPage(ProductHistoryPageReqVO pageReqVO);

    /**
     * 获得虾皮商品历史列表, 用于 Excel 导出
     *
     * @param exportReqVO 查询条件
     * @return 虾皮商品历史列表
     */
    List<ProductHistoryDO> getProductHistoryList(ProductHistoryExportReqVO exportReqVO);

}
