package cn.iocoder.yudao.module.shopee.dal.mysql.producthistory;

import java.util.*;

import cn.iocoder.yudao.framework.common.pojo.PageResult;
import cn.iocoder.yudao.framework.mybatis.core.query.LambdaQueryWrapperX;
import cn.iocoder.yudao.framework.mybatis.core.mapper.BaseMapperX;
import cn.iocoder.yudao.module.shopee.dal.dataobject.producthistory.ProductHistoryDO;
import org.apache.ibatis.annotations.Mapper;
import cn.iocoder.yudao.module.shopee.controller.admin.producthistory.vo.*;

/**
 * 虾皮商品历史 Mapper
 *
 * @author 芋道源码
 */
@Mapper
public interface ProductHistoryMapper extends BaseMapperX<ProductHistoryDO> {

    default PageResult<ProductHistoryDO> selectPage(ProductHistoryPageReqVO reqVO) {
        return selectPage(reqVO, new LambdaQueryWrapperX<ProductHistoryDO>()
                .eqIfPresent(ProductHistoryDO::getSysUserId, reqVO.getSysUserId())
                .eqIfPresent(ProductHistoryDO::getAccountId, reqVO.getAccountId())
                .eqIfPresent(ProductHistoryDO::getShopId, reqVO.getShopId())
                .eqIfPresent(ProductHistoryDO::getSkuId, reqVO.getSkuId())
                .eqIfPresent(ProductHistoryDO::getAddOnDeal, reqVO.getAddOnDeal())
                .eqIfPresent(ProductHistoryDO::getBoostCoolDownSeconds, reqVO.getBoostCoolDownSeconds())
                .eqIfPresent(ProductHistoryDO::getDeboosted, reqVO.getDeboosted())
                .eqIfPresent(ProductHistoryDO::getFlag, reqVO.getFlag())
                .eqIfPresent(ProductHistoryDO::getInPromotion, reqVO.getInPromotion())
                .eqIfPresent(ProductHistoryDO::getLikeCount, reqVO.getLikeCount())
                .likeIfPresent(ProductHistoryDO::getName, reqVO.getName())
                .eqIfPresent(ProductHistoryDO::getParentSku, reqVO.getParentSku())
                .eqIfPresent(ProductHistoryDO::getSold, reqVO.getSold())
                .eqIfPresent(ProductHistoryDO::getStatus, reqVO.getStatus())
                .eqIfPresent(ProductHistoryDO::getSellerCurrentStock, reqVO.getSellerCurrentStock())
                .eqIfPresent(ProductHistoryDO::getViewCount, reqVO.getViewCount())
                .eqIfPresent(ProductHistoryDO::getWholesale, reqVO.getWholesale())
                .eqIfPresent(ProductHistoryDO::getCategoryStatus, reqVO.getCategoryStatus())
                .eqIfPresent(ProductHistoryDO::getDaysToShip, reqVO.getDaysToShip())
                .eqIfPresent(ProductHistoryDO::getPreOrder, reqVO.getPreOrder())
                .eqIfPresent(ProductHistoryDO::getBrandId, reqVO.getBrandId())
                .eqIfPresent(ProductHistoryDO::getBrand, reqVO.getBrand())
                .betweenIfPresent(ProductHistoryDO::getModifyTime, reqVO.getModifyTime())
                .eqIfPresent(ProductHistoryDO::getIsB2CSkuInFbsShop, reqVO.getIsB2CSkuInFbsShop())
                .eqIfPresent(ProductHistoryDO::getMtskuItemId, reqVO.getMtskuItemId())
                .eqIfPresent(ProductHistoryDO::getCategory1, reqVO.getCategory1())
                .eqIfPresent(ProductHistoryDO::getCategory2, reqVO.getCategory2())
                .eqIfPresent(ProductHistoryDO::getCategory3, reqVO.getCategory3())
                .eqIfPresent(ProductHistoryDO::getOngoingUpcomingCampaigns, reqVO.getOngoingUpcomingCampaigns())
                .betweenIfPresent(ProductHistoryDO::getCreateTime, reqVO.getCreateTime())
                .orderByDesc(ProductHistoryDO::getId));
    }

    default List<ProductHistoryDO> selectList(ProductHistoryExportReqVO reqVO) {
        return selectList(new LambdaQueryWrapperX<ProductHistoryDO>()
                .eqIfPresent(ProductHistoryDO::getSysUserId, reqVO.getSysUserId())
                .eqIfPresent(ProductHistoryDO::getAccountId, reqVO.getAccountId())
                .eqIfPresent(ProductHistoryDO::getShopId, reqVO.getShopId())
                .eqIfPresent(ProductHistoryDO::getSkuId, reqVO.getSkuId())
                .eqIfPresent(ProductHistoryDO::getAddOnDeal, reqVO.getAddOnDeal())
                .eqIfPresent(ProductHistoryDO::getBoostCoolDownSeconds, reqVO.getBoostCoolDownSeconds())
                .eqIfPresent(ProductHistoryDO::getDeboosted, reqVO.getDeboosted())
                .eqIfPresent(ProductHistoryDO::getFlag, reqVO.getFlag())
                .eqIfPresent(ProductHistoryDO::getInPromotion, reqVO.getInPromotion())
                .eqIfPresent(ProductHistoryDO::getLikeCount, reqVO.getLikeCount())
                .likeIfPresent(ProductHistoryDO::getName, reqVO.getName())
                .eqIfPresent(ProductHistoryDO::getParentSku, reqVO.getParentSku())
                .eqIfPresent(ProductHistoryDO::getSold, reqVO.getSold())
                .eqIfPresent(ProductHistoryDO::getStatus, reqVO.getStatus())
                .eqIfPresent(ProductHistoryDO::getSellerCurrentStock, reqVO.getSellerCurrentStock())
                .eqIfPresent(ProductHistoryDO::getViewCount, reqVO.getViewCount())
                .eqIfPresent(ProductHistoryDO::getWholesale, reqVO.getWholesale())
                .eqIfPresent(ProductHistoryDO::getCategoryStatus, reqVO.getCategoryStatus())
                .eqIfPresent(ProductHistoryDO::getDaysToShip, reqVO.getDaysToShip())
                .eqIfPresent(ProductHistoryDO::getPreOrder, reqVO.getPreOrder())
                .eqIfPresent(ProductHistoryDO::getBrandId, reqVO.getBrandId())
                .eqIfPresent(ProductHistoryDO::getBrand, reqVO.getBrand())
                .betweenIfPresent(ProductHistoryDO::getModifyTime, reqVO.getModifyTime())
                .eqIfPresent(ProductHistoryDO::getIsB2CSkuInFbsShop, reqVO.getIsB2CSkuInFbsShop())
                .eqIfPresent(ProductHistoryDO::getMtskuItemId, reqVO.getMtskuItemId())
                .eqIfPresent(ProductHistoryDO::getCategory1, reqVO.getCategory1())
                .eqIfPresent(ProductHistoryDO::getCategory2, reqVO.getCategory2())
                .eqIfPresent(ProductHistoryDO::getCategory3, reqVO.getCategory3())
                .eqIfPresent(ProductHistoryDO::getOngoingUpcomingCampaigns, reqVO.getOngoingUpcomingCampaigns())
                .betweenIfPresent(ProductHistoryDO::getCreateTime, reqVO.getCreateTime())
                .orderByDesc(ProductHistoryDO::getId));
    }

}
