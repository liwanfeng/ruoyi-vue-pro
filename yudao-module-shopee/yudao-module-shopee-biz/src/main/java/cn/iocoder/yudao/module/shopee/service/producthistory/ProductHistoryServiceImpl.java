package cn.iocoder.yudao.module.shopee.service.producthistory;

import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import org.springframework.validation.annotation.Validated;

import java.util.*;
import cn.iocoder.yudao.module.shopee.controller.admin.producthistory.vo.*;
import cn.iocoder.yudao.module.shopee.dal.dataobject.producthistory.ProductHistoryDO;
import cn.iocoder.yudao.framework.common.pojo.PageResult;

import cn.iocoder.yudao.module.shopee.convert.producthistory.ProductHistoryConvert;
import cn.iocoder.yudao.module.shopee.dal.mysql.producthistory.ProductHistoryMapper;

import static cn.iocoder.yudao.framework.common.exception.util.ServiceExceptionUtil.exception;
import static cn.iocoder.yudao.module.shopee.enums.ErrorCodeConstants.*;

/**
 * 虾皮商品历史 Service 实现类
 *
 * @author 芋道源码
 */
@Service
@Validated
public class ProductHistoryServiceImpl implements ProductHistoryService {

    @Resource
    private ProductHistoryMapper productHistoryMapper;

    @Override
    public Long createProductHistory(ProductHistoryCreateReqVO createReqVO) {
        // 插入
        ProductHistoryDO productHistory = ProductHistoryConvert.INSTANCE.convert(createReqVO);
        productHistoryMapper.insert(productHistory);
        // 返回
        return productHistory.getId();
    }

    @Override
    public void updateProductHistory(ProductHistoryUpdateReqVO updateReqVO) {
        // 校验存在
        validateProductHistoryExists(updateReqVO.getId());
        // 更新
        ProductHistoryDO updateObj = ProductHistoryConvert.INSTANCE.convert(updateReqVO);
        productHistoryMapper.updateById(updateObj);
    }

    @Override
    public void deleteProductHistory(Long id) {
        // 校验存在
        validateProductHistoryExists(id);
        // 删除
        productHistoryMapper.deleteById(id);
    }

    private void validateProductHistoryExists(Long id) {
        if (productHistoryMapper.selectById(id) == null) {
            throw exception(PRODUCT_HISTORY_NOT_EXISTS);
        }
    }

    @Override
    public ProductHistoryDO getProductHistory(Long id) {
        return productHistoryMapper.selectById(id);
    }

    @Override
    public List<ProductHistoryDO> getProductHistoryList(Collection<Long> ids) {
        return productHistoryMapper.selectBatchIds(ids);
    }

    @Override
    public PageResult<ProductHistoryDO> getProductHistoryPage(ProductHistoryPageReqVO pageReqVO) {
        return productHistoryMapper.selectPage(pageReqVO);
    }

    @Override
    public List<ProductHistoryDO> getProductHistoryList(ProductHistoryExportReqVO exportReqVO) {
        return productHistoryMapper.selectList(exportReqVO);
    }

}
