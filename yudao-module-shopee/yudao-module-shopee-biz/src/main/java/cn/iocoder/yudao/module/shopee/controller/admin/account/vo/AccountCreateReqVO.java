package cn.iocoder.yudao.module.shopee.controller.admin.account.vo;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.*;
import java.util.*;
import io.swagger.v3.oas.annotations.media.Schema;
import javax.validation.constraints.*;

@Schema(description = "管理后台 - 虾皮账户创建 Request VO")
@Data
@ToString(callSuper = true)
public class AccountCreateReqVO extends AccountBaseVO {

    /**
     * Cookie信息
     */
    private String cookie;
}
