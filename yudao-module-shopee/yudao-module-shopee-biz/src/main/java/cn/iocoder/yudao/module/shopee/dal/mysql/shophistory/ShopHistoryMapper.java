package cn.iocoder.yudao.module.shopee.dal.mysql.shophistory;

import java.util.*;

import cn.iocoder.yudao.framework.common.pojo.PageResult;
import cn.iocoder.yudao.framework.mybatis.core.query.LambdaQueryWrapperX;
import cn.iocoder.yudao.framework.mybatis.core.mapper.BaseMapperX;
import cn.iocoder.yudao.module.shopee.dal.dataobject.shophistory.ShopHistoryDO;
import org.apache.ibatis.annotations.Mapper;
import cn.iocoder.yudao.module.shopee.controller.admin.shophistory.vo.*;

/**
 * 虾皮店铺历史 Mapper
 *
 * @author 芋道源码
 */
@Mapper
public interface ShopHistoryMapper extends BaseMapperX<ShopHistoryDO> {

    default PageResult<ShopHistoryDO> selectPage(ShopHistoryPageReqVO reqVO) {
        return selectPage(reqVO, new LambdaQueryWrapperX<ShopHistoryDO>()
                .eqIfPresent(ShopHistoryDO::getSysUserId, reqVO.getSysUserId())
                .eqIfPresent(ShopHistoryDO::getAccountId, reqVO.getAccountId())
                .likeIfPresent(ShopHistoryDO::getUsername, reqVO.getUsername())
                .eqIfPresent(ShopHistoryDO::getUserId, reqVO.getUserId())
                .eqIfPresent(ShopHistoryDO::getRegion, reqVO.getRegion())
                .eqIfPresent(ShopHistoryDO::getEnabled, reqVO.getEnabled())
                .eqIfPresent(ShopHistoryDO::getIsSipAffiliated, reqVO.getIsSipAffiliated())
                .likeIfPresent(ShopHistoryDO::getShopName, reqVO.getShopName())
                .eqIfPresent(ShopHistoryDO::getShopId, reqVO.getShopId())
                .eqIfPresent(ShopHistoryDO::getLastLogin, reqVO.getLastLogin())
                .eqIfPresent(ShopHistoryDO::getIsSipPrimary, reqVO.getIsSipPrimary())
                .eqIfPresent(ShopHistoryDO::getPortrait, reqVO.getPortrait())
                .eqIfPresent(ShopHistoryDO::getCbOption, reqVO.getCbOption())
                .eqIfPresent(ShopHistoryDO::getIsInactive, reqVO.getIsInactive())
                .eqIfPresent(ShopHistoryDO::getIs3PfShop, reqVO.getIs3PfShop())
                .betweenIfPresent(ShopHistoryDO::getCreateTime, reqVO.getCreateTime())
                .orderByDesc(ShopHistoryDO::getId));
    }

    default List<ShopHistoryDO> selectList(ShopHistoryExportReqVO reqVO) {
        return selectList(new LambdaQueryWrapperX<ShopHistoryDO>()
                .eqIfPresent(ShopHistoryDO::getSysUserId, reqVO.getSysUserId())
                .eqIfPresent(ShopHistoryDO::getAccountId, reqVO.getAccountId())
                .likeIfPresent(ShopHistoryDO::getUsername, reqVO.getUsername())
                .eqIfPresent(ShopHistoryDO::getUserId, reqVO.getUserId())
                .eqIfPresent(ShopHistoryDO::getRegion, reqVO.getRegion())
                .eqIfPresent(ShopHistoryDO::getEnabled, reqVO.getEnabled())
                .eqIfPresent(ShopHistoryDO::getIsSipAffiliated, reqVO.getIsSipAffiliated())
                .likeIfPresent(ShopHistoryDO::getShopName, reqVO.getShopName())
                .eqIfPresent(ShopHistoryDO::getShopId, reqVO.getShopId())
                .eqIfPresent(ShopHistoryDO::getLastLogin, reqVO.getLastLogin())
                .eqIfPresent(ShopHistoryDO::getIsSipPrimary, reqVO.getIsSipPrimary())
                .eqIfPresent(ShopHistoryDO::getPortrait, reqVO.getPortrait())
                .eqIfPresent(ShopHistoryDO::getCbOption, reqVO.getCbOption())
                .eqIfPresent(ShopHistoryDO::getIsInactive, reqVO.getIsInactive())
                .eqIfPresent(ShopHistoryDO::getIs3PfShop, reqVO.getIs3PfShop())
                .betweenIfPresent(ShopHistoryDO::getCreateTime, reqVO.getCreateTime())
                .orderByDesc(ShopHistoryDO::getId));
    }

}
