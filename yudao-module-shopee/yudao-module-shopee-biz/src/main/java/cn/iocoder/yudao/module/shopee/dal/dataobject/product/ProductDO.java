package cn.iocoder.yudao.module.shopee.dal.dataobject.product;

import lombok.*;
import java.util.*;
import java.time.LocalDateTime;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.*;
import cn.iocoder.yudao.framework.mybatis.core.dataobject.BaseDO;

/**
 * 虾皮商品 DO
 *
 * @author 芋道源码
 */
@TableName("shopee_product")
@KeySequence("shopee_product_seq") // 用于 Oracle、PostgreSQL、Kingbase、DB2、H2 数据库的主键自增。如果是 MySQL 等数据库，可不写。
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ProductDO extends BaseDO {

    /**
     * 主键
     */
    @TableId
    private Long id;
    /**
     * 所属用户ID
     */
    private Long sysUserId;
    /**
     * 账户ID
     */
    private Long accountId;
    /**
     * 店铺ID
     */
    private Long shopId;
    /**
     * 货号
     */
    private Long skuId;
    /**
     * 是否在售
     */
    private Boolean addOnDeal;
    /**
     * 冷却秒
     */
    private Long boostCoolDownSeconds;
    /**
     * 是否脱销
     */
    private Boolean deboosted;
    /**
     * 标识
     */
    private Long flag;
    /**
     * 是否活动
     */
    private Boolean inPromotion;
    /**
     * 喜欢数量
     */
    private Long likeCount;
    /**
     * 商品名称
     */
    private String name;
    /**
     * 父SKU
     */
    private String parentSku;
    /**
     * 销售数量
     */
    private Long sold;
    /**
     * 状态
     */
    private Long status;
    /**
     * 已有库存
     */
    private Long sellerCurrentStock;
    /**
     * 浏览量
     */
    private Long viewCount;
    /**
     * 是否在售
     */
    private Boolean wholesale;
    /**
     * 分类状态
     */
    private Long categoryStatus;
    /**
     * 邮寄天数
     */
    private Long daysToShip;
    /**
     * 是否预售
     */
    private Boolean preOrder;
    /**
     * 品牌ID
     */
    private Long brandId;
    /**
     * 品牌名称
     */
    private String brand;
    /**
     * 虾皮修改时间
     */
    private Long modifyTime;
    /**
     * 是否B2C店铺
     */
    private Boolean isB2CSkuInFbsShop;
    /**
     * MT商品ID
     */
    private Long mtskuItemId;
    /**
     * 一级分类
     */
    private Long category1;
    /**
     * 二级分类
     */
    private Long category2;
    /**
     * 三级分类
     */
    private Long category3;
    /**
     * 参与活动列表
     */
    private String ongoingUpcomingCampaigns;

}
