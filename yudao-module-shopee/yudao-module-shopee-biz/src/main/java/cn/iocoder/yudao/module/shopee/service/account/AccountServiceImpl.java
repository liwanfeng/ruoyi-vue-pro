package cn.iocoder.yudao.module.shopee.service.account;

import cn.hutool.core.bean.BeanUtil;
import cn.iocoder.boot.yudao.module.spider.shopee.seller.SellerSpider;
import cn.iocoder.boot.yudao.module.spider.shopee.seller.request.SellerRequest;
import cn.iocoder.boot.yudao.module.spider.shopee.seller.response.account.SellerSessionResponse;
import cn.iocoder.boot.yudao.module.spider.shopee.seller.response.account.SellerSessionSubAccountInfoResponse;
import cn.iocoder.yudao.framework.common.pojo.PageResult;
import cn.iocoder.yudao.module.shopee.controller.admin.account.vo.AccountCreateReqVO;
import cn.iocoder.yudao.module.shopee.controller.admin.account.vo.AccountExportReqVO;
import cn.iocoder.yudao.module.shopee.controller.admin.account.vo.AccountPageReqVO;
import cn.iocoder.yudao.module.shopee.controller.admin.account.vo.AccountUpdateReqVO;
import cn.iocoder.yudao.module.shopee.convert.account.AccountConvert;
import cn.iocoder.yudao.module.shopee.dal.dataobject.account.AccountDO;
import cn.iocoder.yudao.module.shopee.dal.dataobject.accounthistory.AccountHistoryDO;
import cn.iocoder.yudao.module.shopee.dal.mysql.account.AccountMapper;
import cn.iocoder.yudao.module.shopee.dal.mysql.accounthistory.AccountHistoryMapper;
import cn.iocoder.yudao.module.shopee.utils.AccountRedisUtils;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import javax.annotation.Resource;
import java.util.Collection;
import java.util.List;

import static cn.iocoder.yudao.framework.common.exception.util.ServiceExceptionUtil.exception;
import static cn.iocoder.yudao.module.shopee.enums.ErrorCodeConstants.ACCOUNT_NOT_EXISTS;

/**
 * 虾皮账户 Service 实现类
 *
 * @author 芋道源码
 */
@Service
@Validated
public class AccountServiceImpl implements AccountService {

    @Resource
    private AccountRedisUtils accountRedisUtils;
    @Resource
    private AccountHistoryMapper accountHistoryMapper;

    @Resource
    private AccountMapper accountMapper;

    @Override
    public Long createAccount(AccountCreateReqVO createReqVO) {
        // 插入
        AccountDO account = AccountConvert.INSTANCE.convert(createReqVO);
        accountMapper.insert(account);
        // 返回
        return account.getId();
    }

    @Override
    public void updateAccount(AccountUpdateReqVO updateReqVO) {
        // 校验存在
        validateAccountExists(updateReqVO.getId());
        // 更新
        AccountDO updateObj = AccountConvert.INSTANCE.convert(updateReqVO);
        accountMapper.updateById(updateObj);
    }

    @Override
    public void deleteAccount(Long id) {
        // 校验存在
        validateAccountExists(id);
        // 删除
        accountMapper.deleteById(id);
    }

    private void validateAccountExists(Long id) {
        if (accountMapper.selectById(id) == null) {
            throw exception(ACCOUNT_NOT_EXISTS);
        }
    }

    @Override
    public AccountDO getAccount(Long id) {
        return accountMapper.selectById(id);
    }

    @Override
    public List<AccountDO> getAccountList(Collection<Long> ids) {
        return accountMapper.selectBatchIds(ids);
    }

    @Override
    public PageResult<AccountDO> getAccountPage(AccountPageReqVO pageReqVO) {
        return accountMapper.selectPage(pageReqVO);
    }

    @Override
    public List<AccountDO> getAccountList(AccountExportReqVO exportReqVO) {
        return accountMapper.selectList(exportReqVO);
    }

    @Override
    public Long upsertAccount(AccountCreateReqVO createReqVO) {
        AccountDO account = AccountConvert.INSTANCE.convert(createReqVO);
        return accountMapper.upsertAccount(account);
    }

    @Override
    public Long updateRemoteAccount(String cookie)  {
        SellerSessionResponse session = SellerSpider.getSession(SellerRequest.builder().cookie(cookie).build());
        if (session == null || session.getSubAccountInfo() == null) {
            throw new RuntimeException("同步用户Session信息失败");
        }
        SellerSessionSubAccountInfoResponse subAccountInfo = session.getSubAccountInfo();
        AccountDO accountDO = new AccountDO();
        BeanUtil.copyProperties(subAccountInfo, accountDO);
        AccountExportReqVO reqVO = new AccountExportReqVO();
        reqVO.setAccountId(subAccountInfo.getAccountId());
        accountMapper.upsertAccount(accountDO);
        // 将cookie保存到Redis中
        accountRedisUtils.saveCookie( accountDO.getAccountId(), cookie);

        // 保存历史记录
        AccountHistoryDO accountHistoryDO = new AccountHistoryDO();
        BeanUtil.copyProperties(subAccountInfo, accountHistoryDO);
        accountHistoryMapper.insert(accountHistoryDO);
        return accountDO.getId();
    }
}
