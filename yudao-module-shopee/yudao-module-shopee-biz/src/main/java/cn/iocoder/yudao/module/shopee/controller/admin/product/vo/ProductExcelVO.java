package cn.iocoder.yudao.module.shopee.controller.admin.product.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;
import java.util.*;
import java.time.LocalDateTime;
import java.time.LocalDateTime;

import com.alibaba.excel.annotation.ExcelProperty;

/**
 * 虾皮商品 Excel VO
 *
 * @author 芋道源码
 */
@Data
public class ProductExcelVO {

    @ExcelProperty("主键")
    private Long id;

    @ExcelProperty("所属用户ID")
    private Long sysUserId;

    @ExcelProperty("账户ID")
    private Long accountId;

    @ExcelProperty("店铺ID")
    private Long shopId;

    @ExcelProperty("货号")
    private Long skuId;

    @ExcelProperty("是否在售")
    private Boolean addOnDeal;

    @ExcelProperty("冷却秒")
    private Long boostCoolDownSeconds;

    @ExcelProperty("是否脱销")
    private Boolean deboosted;

    @ExcelProperty("标识")
    private Long flag;

    @ExcelProperty("是否活动")
    private Boolean inPromotion;

    @ExcelProperty("喜欢数量")
    private Long likeCount;

    @ExcelProperty("商品名称")
    private String name;

    @ExcelProperty("父SKU")
    private String parentSku;

    @ExcelProperty("销售数量")
    private Long sold;

    @ExcelProperty("状态")
    private Long status;

    @ExcelProperty("已有库存")
    private Long sellerCurrentStock;

    @ExcelProperty("浏览量")
    private Long viewCount;

    @ExcelProperty("是否在售")
    private Boolean wholesale;

    @ExcelProperty("分类状态")
    private Long categoryStatus;

    @ExcelProperty("邮寄天数")
    private Long daysToShip;

    @ExcelProperty("是否预售")
    private Boolean preOrder;

    @ExcelProperty("品牌ID")
    private Long brandId;

    @ExcelProperty("品牌名称")
    private String brand;

    @ExcelProperty("虾皮修改时间")
    private Long modifyTime;

    @ExcelProperty("是否B2C店铺")
    private Boolean isB2CSkuInFbsShop;

    @ExcelProperty("MT商品ID")
    private Long mtskuItemId;

    @ExcelProperty("一级分类")
    private Long category1;

    @ExcelProperty("二级分类")
    private Long category2;

    @ExcelProperty("三级分类")
    private Long category3;

    @ExcelProperty("参与活动列表")
    private String ongoingUpcomingCampaigns;

    @ExcelProperty("创建时间")
    private LocalDateTime createTime;

}
