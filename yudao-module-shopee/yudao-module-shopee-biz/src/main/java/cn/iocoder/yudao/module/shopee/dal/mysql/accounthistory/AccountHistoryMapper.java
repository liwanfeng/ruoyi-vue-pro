package cn.iocoder.yudao.module.shopee.dal.mysql.accounthistory;

import java.util.*;

import cn.iocoder.yudao.framework.common.pojo.PageResult;
import cn.iocoder.yudao.framework.mybatis.core.query.LambdaQueryWrapperX;
import cn.iocoder.yudao.framework.mybatis.core.mapper.BaseMapperX;
import cn.iocoder.yudao.module.shopee.dal.dataobject.accounthistory.AccountHistoryDO;
import org.apache.ibatis.annotations.Mapper;
import cn.iocoder.yudao.module.shopee.controller.admin.accounthistory.vo.*;

/**
 * 虾皮账户历史 Mapper
 *
 * @author 芋道源码
 */
@Mapper
public interface AccountHistoryMapper extends BaseMapperX<AccountHistoryDO> {

    default PageResult<AccountHistoryDO> selectPage(AccountHistoryPageReqVO reqVO) {
        return selectPage(reqVO, new LambdaQueryWrapperX<AccountHistoryDO>()
                .eqIfPresent(AccountHistoryDO::getSysUserId, reqVO.getSysUserId())
                .eqIfPresent(AccountHistoryDO::getAccountId, reqVO.getAccountId())
                .eqIfPresent(AccountHistoryDO::getAccountType, reqVO.getAccountType())
                .eqIfPresent(AccountHistoryDO::getTobAccountId, reqVO.getTobAccountId())
                .eqIfPresent(AccountHistoryDO::getPhone, reqVO.getPhone())
                .eqIfPresent(AccountHistoryDO::getPortrait, reqVO.getPortrait())
                .likeIfPresent(AccountHistoryDO::getAccountName, reqVO.getAccountName())
                .eqIfPresent(AccountHistoryDO::getIsOverdue, reqVO.getIsOverdue())
                .eqIfPresent(AccountHistoryDO::getLanguage, reqVO.getLanguage())
                .likeIfPresent(AccountHistoryDO::getNickName, reqVO.getNickName())
                .eqIfPresent(AccountHistoryDO::getMainAccountId, reqVO.getMainAccountId())
                .eqIfPresent(AccountHistoryDO::getSubAccountId, reqVO.getSubAccountId())
                .eqIfPresent(AccountHistoryDO::getEmail, reqVO.getEmail())
                .likeIfPresent(AccountHistoryDO::getMerchantName, reqVO.getMerchantName())
                .betweenIfPresent(AccountHistoryDO::getCreateTime, reqVO.getCreateTime())
                .orderByDesc(AccountHistoryDO::getId));
    }

    default List<AccountHistoryDO> selectList(AccountHistoryExportReqVO reqVO) {
        return selectList(new LambdaQueryWrapperX<AccountHistoryDO>()
                .eqIfPresent(AccountHistoryDO::getSysUserId, reqVO.getSysUserId())
                .eqIfPresent(AccountHistoryDO::getAccountId, reqVO.getAccountId())
                .eqIfPresent(AccountHistoryDO::getAccountType, reqVO.getAccountType())
                .eqIfPresent(AccountHistoryDO::getTobAccountId, reqVO.getTobAccountId())
                .eqIfPresent(AccountHistoryDO::getPhone, reqVO.getPhone())
                .eqIfPresent(AccountHistoryDO::getPortrait, reqVO.getPortrait())
                .likeIfPresent(AccountHistoryDO::getAccountName, reqVO.getAccountName())
                .eqIfPresent(AccountHistoryDO::getIsOverdue, reqVO.getIsOverdue())
                .eqIfPresent(AccountHistoryDO::getLanguage, reqVO.getLanguage())
                .likeIfPresent(AccountHistoryDO::getNickName, reqVO.getNickName())
                .eqIfPresent(AccountHistoryDO::getMainAccountId, reqVO.getMainAccountId())
                .eqIfPresent(AccountHistoryDO::getSubAccountId, reqVO.getSubAccountId())
                .eqIfPresent(AccountHistoryDO::getEmail, reqVO.getEmail())
                .likeIfPresent(AccountHistoryDO::getMerchantName, reqVO.getMerchantName())
                .betweenIfPresent(AccountHistoryDO::getCreateTime, reqVO.getCreateTime())
                .orderByDesc(AccountHistoryDO::getId));
    }

}
