package cn.iocoder.yudao.module.shopee.controller.admin.account.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;
import java.util.*;
import java.time.LocalDateTime;
import java.time.LocalDateTime;
import javax.validation.constraints.*;

/**
 * 虾皮账户 Base VO，提供给添加、修改、详细的子 VO 使用
 * 如果子 VO 存在差异的字段，请不要添加到这里，影响 Swagger 文档生成
 */
@Data
public class AccountBaseVO {

    @Schema(description = "所属用户ID", example = "18029")
    private Long sysUserId;

    @Schema(description = "账户ID", example = "7006")
    private Long accountId;

    @Schema(description = "账户类型", example = "2")
    private String accountType;

    @Schema(description = "TOB账户ID", example = "14366")
    private Long tobAccountId;

    @Schema(description = "联系电话")
    private String phone;

    @Schema(description = "描述")
    private String portrait;

    @Schema(description = "账号", example = "李四")
    private String accountName;

    @Schema(description = "是否逾期")
    private Boolean isOverdue;

    @Schema(description = "语言")
    private String language;

    @Schema(description = "用户昵称", example = "张三")
    private String nickName;

    @Schema(description = "主账户ID", example = "24651")
    private Long mainAccountId;

    @Schema(description = "子账户ID", example = "21677")
    private Long subAccountId;

    @Schema(description = "邮件地址")
    private String email;

    @Schema(description = "商家名称", example = "张三")
    private String merchantName;

}
