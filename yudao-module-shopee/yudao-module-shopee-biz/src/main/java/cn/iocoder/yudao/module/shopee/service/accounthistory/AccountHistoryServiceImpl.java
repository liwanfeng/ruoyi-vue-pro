package cn.iocoder.yudao.module.shopee.service.accounthistory;

import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import org.springframework.validation.annotation.Validated;

import java.util.*;
import cn.iocoder.yudao.module.shopee.controller.admin.accounthistory.vo.*;
import cn.iocoder.yudao.module.shopee.dal.dataobject.accounthistory.AccountHistoryDO;
import cn.iocoder.yudao.framework.common.pojo.PageResult;

import cn.iocoder.yudao.module.shopee.convert.accounthistory.AccountHistoryConvert;
import cn.iocoder.yudao.module.shopee.dal.mysql.accounthistory.AccountHistoryMapper;

import static cn.iocoder.yudao.framework.common.exception.util.ServiceExceptionUtil.exception;
import static cn.iocoder.yudao.module.shopee.enums.ErrorCodeConstants.*;

/**
 * 虾皮账户历史 Service 实现类
 *
 * @author 芋道源码
 */
@Service
@Validated
public class AccountHistoryServiceImpl implements AccountHistoryService {

    @Resource
    private AccountHistoryMapper accountHistoryMapper;

    @Override
    public Long createAccountHistory(AccountHistoryCreateReqVO createReqVO) {
        // 插入
        AccountHistoryDO accountHistory = AccountHistoryConvert.INSTANCE.convert(createReqVO);
        accountHistoryMapper.insert(accountHistory);
        // 返回
        return accountHistory.getId();
    }

    @Override
    public void updateAccountHistory(AccountHistoryUpdateReqVO updateReqVO) {
        // 校验存在
        validateAccountHistoryExists(updateReqVO.getId());
        // 更新
        AccountHistoryDO updateObj = AccountHistoryConvert.INSTANCE.convert(updateReqVO);
        accountHistoryMapper.updateById(updateObj);
    }

    @Override
    public void deleteAccountHistory(Long id) {
        // 校验存在
        validateAccountHistoryExists(id);
        // 删除
        accountHistoryMapper.deleteById(id);
    }

    private void validateAccountHistoryExists(Long id) {
        if (accountHistoryMapper.selectById(id) == null) {
            throw exception(ACCOUNT_HISTORY_NOT_EXISTS);
        }
    }

    @Override
    public AccountHistoryDO getAccountHistory(Long id) {
        return accountHistoryMapper.selectById(id);
    }

    @Override
    public List<AccountHistoryDO> getAccountHistoryList(Collection<Long> ids) {
        return accountHistoryMapper.selectBatchIds(ids);
    }

    @Override
    public PageResult<AccountHistoryDO> getAccountHistoryPage(AccountHistoryPageReqVO pageReqVO) {
        return accountHistoryMapper.selectPage(pageReqVO);
    }

    @Override
    public List<AccountHistoryDO> getAccountHistoryList(AccountHistoryExportReqVO exportReqVO) {
        return accountHistoryMapper.selectList(exportReqVO);
    }

}
