package cn.iocoder.yudao.module.shopee.utils;

import cn.hutool.core.util.StrUtil;
import org.springframework.data.redis.core.RedisTemplate;

import javax.annotation.Resource;

@Resource
public class AccountRedisUtils {
    @Resource
    private RedisTemplate<String, String> redisTemplate;

    /**
     * 保存账户的Cookie信息
     * @param accountId shopee账户ID
     * @param cookie Cookie信息
     */
    public void saveCookie(Long accountId, String cookie) {
        redisTemplate.opsForValue().set(StrUtil.format("SHOPEE:COOKIE:DATA:{}", accountId), cookie);
    }

    /**
     * 获取账户的Cookie信息
     * @param accountId shopee账户ID
     * @return cookie信息
     */
    public String getCookie(Long accountId) {
        return  redisTemplate.opsForValue().get(StrUtil.format("SHOPEE:COOKIE:DATA:{}", accountId));
    }

    /**
     * 判断Cookie是否有效
     * @param accountId shopee账户ID
     * @return 是否有效
     */
    public Boolean hasCookie(Long accountId) {
        return redisTemplate.hasKey(StrUtil.format("SHOPEE:COOKIE:DATA:{}", accountId));
    }

    /**
     * 记录Cookie请求失败
     * @param accountId shopee账户ID
     */
    public void error(Long accountId) {
        redisTemplate.opsForValue().increment(StrUtil.format("SHOPEE:COOKIE:ERROR:{}", accountId));
    }

    /**
     * 请求成功，删除错误记录
     * @param accountId shopee账户ID
     */
    public void success(Long accountId) {
        redisTemplate.delete(StrUtil.format("SHOPEE:COOKIE:ERROR:{}", accountId));
    }
}
