package cn.iocoder.yudao.module.shopee.convert.account;

import java.util.*;

import cn.iocoder.boot.yudao.module.spider.shopee.seller.response.account.SellerSessionSubAccountInfoResponse;
import cn.iocoder.yudao.framework.common.pojo.PageResult;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import cn.iocoder.yudao.module.shopee.controller.admin.account.vo.*;
import cn.iocoder.yudao.module.shopee.dal.dataobject.account.AccountDO;

/**
 * 虾皮账户 Convert
 *
 * @author 芋道源码
 */
@Mapper
public interface AccountConvert {

    AccountConvert INSTANCE = Mappers.getMapper(AccountConvert.class);

    AccountDO convert(AccountCreateReqVO bean);

    AccountDO convert(AccountUpdateReqVO bean);

    AccountRespVO convert(AccountDO bean);

    List<AccountRespVO> convertList(List<AccountDO> list);

    PageResult<AccountRespVO> convertPage(PageResult<AccountDO> page);

    List<AccountExcelVO> convertList02(List<AccountDO> list);
}
