package cn.iocoder.yudao.module.shopee.controller.admin.accounthistory;

import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import org.springframework.validation.annotation.Validated;
import org.springframework.security.access.prepost.PreAuthorize;
import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Operation;

import javax.validation.constraints.*;
import javax.validation.*;
import javax.servlet.http.*;
import java.util.*;
import java.io.IOException;

import cn.iocoder.yudao.framework.common.pojo.PageResult;
import cn.iocoder.yudao.framework.common.pojo.CommonResult;
import static cn.iocoder.yudao.framework.common.pojo.CommonResult.success;

import cn.iocoder.yudao.framework.excel.core.util.ExcelUtils;

import cn.iocoder.yudao.framework.operatelog.core.annotations.OperateLog;
import static cn.iocoder.yudao.framework.operatelog.core.enums.OperateTypeEnum.*;

import cn.iocoder.yudao.module.shopee.controller.admin.accounthistory.vo.*;
import cn.iocoder.yudao.module.shopee.dal.dataobject.accounthistory.AccountHistoryDO;
import cn.iocoder.yudao.module.shopee.convert.accounthistory.AccountHistoryConvert;
import cn.iocoder.yudao.module.shopee.service.accounthistory.AccountHistoryService;

@Tag(name = "管理后台 - 虾皮账户历史")
@RestController
@RequestMapping("/shopee/account-history")
@Validated
public class AccountHistoryController {

    @Resource
    private AccountHistoryService accountHistoryService;

    @PostMapping("/create")
    @Operation(summary = "创建虾皮账户历史")
    @PreAuthorize("@ss.hasPermission('shopee:account-history:create')")
    public CommonResult<Long> createAccountHistory(@Valid @RequestBody AccountHistoryCreateReqVO createReqVO) {
        return success(accountHistoryService.createAccountHistory(createReqVO));
    }

    @PutMapping("/update")
    @Operation(summary = "更新虾皮账户历史")
    @PreAuthorize("@ss.hasPermission('shopee:account-history:update')")
    public CommonResult<Boolean> updateAccountHistory(@Valid @RequestBody AccountHistoryUpdateReqVO updateReqVO) {
        accountHistoryService.updateAccountHistory(updateReqVO);
        return success(true);
    }

    @DeleteMapping("/delete")
    @Operation(summary = "删除虾皮账户历史")
    @Parameter(name = "id", description = "编号", required = true)
    @PreAuthorize("@ss.hasPermission('shopee:account-history:delete')")
    public CommonResult<Boolean> deleteAccountHistory(@RequestParam("id") Long id) {
        accountHistoryService.deleteAccountHistory(id);
        return success(true);
    }

    @GetMapping("/get")
    @Operation(summary = "获得虾皮账户历史")
    @Parameter(name = "id", description = "编号", required = true, example = "1024")
    @PreAuthorize("@ss.hasPermission('shopee:account-history:query')")
    public CommonResult<AccountHistoryRespVO> getAccountHistory(@RequestParam("id") Long id) {
        AccountHistoryDO accountHistory = accountHistoryService.getAccountHistory(id);
        return success(AccountHistoryConvert.INSTANCE.convert(accountHistory));
    }

    @GetMapping("/list")
    @Operation(summary = "获得虾皮账户历史列表")
    @Parameter(name = "ids", description = "编号列表", required = true, example = "1024,2048")
    @PreAuthorize("@ss.hasPermission('shopee:account-history:query')")
    public CommonResult<List<AccountHistoryRespVO>> getAccountHistoryList(@RequestParam("ids") Collection<Long> ids) {
        List<AccountHistoryDO> list = accountHistoryService.getAccountHistoryList(ids);
        return success(AccountHistoryConvert.INSTANCE.convertList(list));
    }

    @GetMapping("/page")
    @Operation(summary = "获得虾皮账户历史分页")
    @PreAuthorize("@ss.hasPermission('shopee:account-history:query')")
    public CommonResult<PageResult<AccountHistoryRespVO>> getAccountHistoryPage(@Valid AccountHistoryPageReqVO pageVO) {
        PageResult<AccountHistoryDO> pageResult = accountHistoryService.getAccountHistoryPage(pageVO);
        return success(AccountHistoryConvert.INSTANCE.convertPage(pageResult));
    }

    @GetMapping("/export-excel")
    @Operation(summary = "导出虾皮账户历史 Excel")
    @PreAuthorize("@ss.hasPermission('shopee:account-history:export')")
    @OperateLog(type = EXPORT)
    public void exportAccountHistoryExcel(@Valid AccountHistoryExportReqVO exportReqVO,
              HttpServletResponse response) throws IOException {
        List<AccountHistoryDO> list = accountHistoryService.getAccountHistoryList(exportReqVO);
        // 导出 Excel
        List<AccountHistoryExcelVO> datas = AccountHistoryConvert.INSTANCE.convertList02(list);
        ExcelUtils.write(response, "虾皮账户历史.xls", "数据", AccountHistoryExcelVO.class, datas);
    }

}
