package cn.iocoder.yudao.module.shopee.convert.shophistory;

import java.util.*;

import cn.iocoder.yudao.framework.common.pojo.PageResult;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import cn.iocoder.yudao.module.shopee.controller.admin.shophistory.vo.*;
import cn.iocoder.yudao.module.shopee.dal.dataobject.shophistory.ShopHistoryDO;

/**
 * 虾皮店铺历史 Convert
 *
 * @author 芋道源码
 */
@Mapper
public interface ShopHistoryConvert {

    ShopHistoryConvert INSTANCE = Mappers.getMapper(ShopHistoryConvert.class);

    ShopHistoryDO convert(ShopHistoryCreateReqVO bean);

    ShopHistoryDO convert(ShopHistoryUpdateReqVO bean);

    ShopHistoryRespVO convert(ShopHistoryDO bean);

    List<ShopHistoryRespVO> convertList(List<ShopHistoryDO> list);

    PageResult<ShopHistoryRespVO> convertPage(PageResult<ShopHistoryDO> page);

    List<ShopHistoryExcelVO> convertList02(List<ShopHistoryDO> list);

}
