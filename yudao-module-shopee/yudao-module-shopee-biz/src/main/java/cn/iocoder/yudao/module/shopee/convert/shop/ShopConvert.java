package cn.iocoder.yudao.module.shopee.convert.shop;

import java.util.*;

import cn.iocoder.yudao.framework.common.pojo.PageResult;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import cn.iocoder.yudao.module.shopee.controller.admin.shop.vo.*;
import cn.iocoder.yudao.module.shopee.dal.dataobject.shop.ShopDO;

/**
 * 虾皮店铺 Convert
 *
 * @author 芋道源码
 */
@Mapper
public interface ShopConvert {

    ShopConvert INSTANCE = Mappers.getMapper(ShopConvert.class);

    ShopDO convert(ShopCreateReqVO bean);

    ShopDO convert(ShopUpdateReqVO bean);

    ShopRespVO convert(ShopDO bean);

    List<ShopRespVO> convertList(List<ShopDO> list);

    PageResult<ShopRespVO> convertPage(PageResult<ShopDO> page);

    List<ShopExcelVO> convertList02(List<ShopDO> list);

}
