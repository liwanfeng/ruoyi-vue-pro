package cn.iocoder.yudao.module.shopee.controller.admin.producthistory.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;
import java.util.*;
import javax.validation.constraints.*;

@Schema(description = "管理后台 - 虾皮商品历史更新 Request VO")
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class ProductHistoryUpdateReqVO extends ProductHistoryBaseVO {

    @Schema(description = "主键", requiredMode = Schema.RequiredMode.REQUIRED, example = "15438")
    @NotNull(message = "主键不能为空")
    private Long id;

}
