package cn.iocoder.yudao.module.shopee.dal.mysql.shop;

import java.util.*;

import cn.iocoder.yudao.framework.common.pojo.PageResult;
import cn.iocoder.yudao.framework.mybatis.core.query.LambdaQueryWrapperX;
import cn.iocoder.yudao.framework.mybatis.core.mapper.BaseMapperX;
import cn.iocoder.yudao.module.shopee.dal.dataobject.account.AccountDO;
import cn.iocoder.yudao.module.shopee.dal.dataobject.shop.ShopDO;
import org.apache.ibatis.annotations.Mapper;
import cn.iocoder.yudao.module.shopee.controller.admin.shop.vo.*;
import org.apache.ibatis.annotations.Param;

/**
 * 虾皮店铺 Mapper
 *
 * @author 芋道源码
 */
@Mapper
public interface ShopMapper extends BaseMapperX<ShopDO> {

    default PageResult<ShopDO> selectPage(ShopPageReqVO reqVO) {
        return selectPage(reqVO, new LambdaQueryWrapperX<ShopDO>()
                .eqIfPresent(ShopDO::getSysUserId, reqVO.getSysUserId())
                .eqIfPresent(ShopDO::getAccountId, reqVO.getAccountId())
                .likeIfPresent(ShopDO::getUsername, reqVO.getUsername())
                .eqIfPresent(ShopDO::getUserId, reqVO.getUserId())
                .eqIfPresent(ShopDO::getRegion, reqVO.getRegion())
                .eqIfPresent(ShopDO::getEnabled, reqVO.getEnabled())
                .eqIfPresent(ShopDO::getIsSipAffiliated, reqVO.getIsSipAffiliated())
                .likeIfPresent(ShopDO::getShopName, reqVO.getShopName())
                .eqIfPresent(ShopDO::getShopId, reqVO.getShopId())
                .eqIfPresent(ShopDO::getLastLogin, reqVO.getLastLogin())
                .eqIfPresent(ShopDO::getIsSipPrimary, reqVO.getIsSipPrimary())
                .eqIfPresent(ShopDO::getPortrait, reqVO.getPortrait())
                .eqIfPresent(ShopDO::getCbOption, reqVO.getCbOption())
                .eqIfPresent(ShopDO::getIsInactive, reqVO.getIsInactive())
                .eqIfPresent(ShopDO::getIs3PfShop, reqVO.getIs3PfShop())
                .betweenIfPresent(ShopDO::getCreateTime, reqVO.getCreateTime())
                .orderByDesc(ShopDO::getId));
    }

    default List<ShopDO> selectList(ShopExportReqVO reqVO) {
        return selectList(new LambdaQueryWrapperX<ShopDO>()
                .eqIfPresent(ShopDO::getSysUserId, reqVO.getSysUserId())
                .eqIfPresent(ShopDO::getAccountId, reqVO.getAccountId())
                .likeIfPresent(ShopDO::getUsername, reqVO.getUsername())
                .eqIfPresent(ShopDO::getUserId, reqVO.getUserId())
                .eqIfPresent(ShopDO::getRegion, reqVO.getRegion())
                .eqIfPresent(ShopDO::getEnabled, reqVO.getEnabled())
                .eqIfPresent(ShopDO::getIsSipAffiliated, reqVO.getIsSipAffiliated())
                .likeIfPresent(ShopDO::getShopName, reqVO.getShopName())
                .eqIfPresent(ShopDO::getShopId, reqVO.getShopId())
                .eqIfPresent(ShopDO::getLastLogin, reqVO.getLastLogin())
                .eqIfPresent(ShopDO::getIsSipPrimary, reqVO.getIsSipPrimary())
                .eqIfPresent(ShopDO::getPortrait, reqVO.getPortrait())
                .eqIfPresent(ShopDO::getCbOption, reqVO.getCbOption())
                .eqIfPresent(ShopDO::getIsInactive, reqVO.getIsInactive())
                .eqIfPresent(ShopDO::getIs3PfShop, reqVO.getIs3PfShop())
                .betweenIfPresent(ShopDO::getCreateTime, reqVO.getCreateTime())
                .orderByDesc(ShopDO::getId));
    }

    Long upsert(@Param("ew") ShopDO shopDO);

}
