package cn.iocoder.yudao.module.shopee.controller.admin.accounthistory.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;
import java.util.*;
import java.time.LocalDateTime;
import java.time.LocalDateTime;
import javax.validation.constraints.*;

/**
 * 虾皮账户历史 Base VO，提供给添加、修改、详细的子 VO 使用
 * 如果子 VO 存在差异的字段，请不要添加到这里，影响 Swagger 文档生成
 */
@Data
public class AccountHistoryBaseVO {

    @Schema(description = "所属用户ID", example = "12522")
    private Long sysUserId;

    @Schema(description = "账户ID", example = "27806")
    private Long accountId;

    @Schema(description = "账户类型", example = "2")
    private String accountType;

    @Schema(description = "TOB账户ID", example = "30984")
    private Long tobAccountId;

    @Schema(description = "联系电话")
    private String phone;

    @Schema(description = "描述")
    private String portrait;

    @Schema(description = "账号", example = "赵六")
    private String accountName;

    @Schema(description = "是否逾期")
    private Boolean isOverdue;

    @Schema(description = "语言")
    private String language;

    @Schema(description = "用户昵称", example = "王五")
    private String nickName;

    @Schema(description = "主账户ID", example = "364")
    private Long mainAccountId;

    @Schema(description = "子账户ID", example = "16637")
    private Long subAccountId;

    @Schema(description = "邮件地址")
    private String email;

    @Schema(description = "商家名称", example = "王五")
    private String merchantName;

}
