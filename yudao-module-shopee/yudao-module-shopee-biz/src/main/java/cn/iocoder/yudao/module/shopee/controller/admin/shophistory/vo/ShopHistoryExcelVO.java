package cn.iocoder.yudao.module.shopee.controller.admin.shophistory.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;
import java.util.*;
import java.time.LocalDateTime;
import java.time.LocalDateTime;

import com.alibaba.excel.annotation.ExcelProperty;

/**
 * 虾皮店铺历史 Excel VO
 *
 * @author 芋道源码
 */
@Data
public class ShopHistoryExcelVO {

    @ExcelProperty("主键")
    private Long id;

    @ExcelProperty("所属用户ID")
    private Long sysUserId;

    @ExcelProperty("账户ID")
    private Long accountId;

    @ExcelProperty("用户名")
    private String username;

    @ExcelProperty("用户ID")
    private Long userId;

    @ExcelProperty("所属区域")
    private String region;

    @ExcelProperty("是否启用")
    private Boolean enabled;

    @ExcelProperty("是否SIP")
    private Boolean isSipAffiliated;

    @ExcelProperty("店铺名称")
    private String shopName;

    @ExcelProperty("店铺ID")
    private Long shopId;

    @ExcelProperty("最后登录时间")
    private Long lastLogin;

    @ExcelProperty("是否SIP主店铺")
    private Boolean isSipPrimary;

    @ExcelProperty("描述")
    private String portrait;

    @ExcelProperty("CB选项")
    private Long cbOption;

    @ExcelProperty("是否不活跃")
    private Boolean isInactive;

    @ExcelProperty("是否三方店铺")
    private Boolean is3PfShop;

    @ExcelProperty("创建时间")
    private LocalDateTime createTime;

}
