package cn.iocoder.yudao.module.shopee.controller.admin.product.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;
import java.util.*;
import java.time.LocalDateTime;
import java.time.LocalDateTime;
import javax.validation.constraints.*;

/**
 * 虾皮商品 Base VO，提供给添加、修改、详细的子 VO 使用
 * 如果子 VO 存在差异的字段，请不要添加到这里，影响 Swagger 文档生成
 */
@Data
public class ProductBaseVO {

    @Schema(description = "所属用户ID", example = "25995")
    private Long sysUserId;

    @Schema(description = "账户ID", example = "12175")
    private Long accountId;

    @Schema(description = "店铺ID", example = "31779")
    private Long shopId;

    @Schema(description = "货号", example = "22694")
    private Long skuId;

    @Schema(description = "是否在售")
    private Boolean addOnDeal;

    @Schema(description = "冷却秒")
    private Long boostCoolDownSeconds;

    @Schema(description = "是否脱销")
    private Boolean deboosted;

    @Schema(description = "标识")
    private Long flag;

    @Schema(description = "是否活动")
    private Boolean inPromotion;

    @Schema(description = "喜欢数量", example = "30801")
    private Long likeCount;

    @Schema(description = "商品名称", example = "赵六")
    private String name;

    @Schema(description = "父SKU")
    private String parentSku;

    @Schema(description = "销售数量")
    private Long sold;

    @Schema(description = "状态", example = "1")
    private Long status;

    @Schema(description = "已有库存")
    private Long sellerCurrentStock;

    @Schema(description = "浏览量", example = "586")
    private Long viewCount;

    @Schema(description = "是否在售")
    private Boolean wholesale;

    @Schema(description = "分类状态", example = "2")
    private Long categoryStatus;

    @Schema(description = "邮寄天数")
    private Long daysToShip;

    @Schema(description = "是否预售")
    private Boolean preOrder;

    @Schema(description = "品牌ID", example = "1127")
    private Long brandId;

    @Schema(description = "品牌名称")
    private String brand;

    @Schema(description = "虾皮修改时间")
    private Long modifyTime;

    @Schema(description = "是否B2C店铺")
    private Boolean isB2CSkuInFbsShop;

    @Schema(description = "MT商品ID", example = "3789")
    private Long mtskuItemId;

    @Schema(description = "一级分类")
    private Long category1;

    @Schema(description = "二级分类")
    private Long category2;

    @Schema(description = "三级分类")
    private Long category3;

    @Schema(description = "参与活动列表")
    private String ongoingUpcomingCampaigns;

}
