package cn.iocoder.yudao.module.shopee.controller.admin.account.vo;

import lombok.*;
import java.util.*;
import io.swagger.v3.oas.annotations.media.Schema;
import cn.iocoder.yudao.framework.common.pojo.PageParam;
import java.time.LocalDateTime;
import org.springframework.format.annotation.DateTimeFormat;

import static cn.iocoder.yudao.framework.common.util.date.DateUtils.FORMAT_YEAR_MONTH_DAY_HOUR_MINUTE_SECOND;

@Schema(description = "管理后台 - 虾皮账户 Excel 导出 Request VO，参数和 AccountPageReqVO 是一致的")
@Data
public class AccountExportReqVO {

    @Schema(description = "所属用户ID", example = "18029")
    private Long sysUserId;

    @Schema(description = "账户ID", example = "7006")
    private Long accountId;

    @Schema(description = "账户类型", example = "2")
    private String accountType;

    @Schema(description = "TOB账户ID", example = "14366")
    private Long tobAccountId;

    @Schema(description = "联系电话")
    private String phone;

    @Schema(description = "描述")
    private String portrait;

    @Schema(description = "账号", example = "李四")
    private String accountName;

    @Schema(description = "是否逾期")
    private Boolean isOverdue;

    @Schema(description = "语言")
    private String language;

    @Schema(description = "用户昵称", example = "张三")
    private String nickName;

    @Schema(description = "主账户ID", example = "24651")
    private Long mainAccountId;

    @Schema(description = "子账户ID", example = "21677")
    private Long subAccountId;

    @Schema(description = "邮件地址")
    private String email;

    @Schema(description = "商家名称", example = "张三")
    private String merchantName;

    @Schema(description = "创建时间")
    @DateTimeFormat(pattern = FORMAT_YEAR_MONTH_DAY_HOUR_MINUTE_SECOND)
    private LocalDateTime[] createTime;

}
