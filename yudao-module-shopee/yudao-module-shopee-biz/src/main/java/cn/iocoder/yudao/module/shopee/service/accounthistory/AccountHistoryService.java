package cn.iocoder.yudao.module.shopee.service.accounthistory;

import java.util.*;
import javax.validation.*;
import cn.iocoder.yudao.module.shopee.controller.admin.accounthistory.vo.*;
import cn.iocoder.yudao.module.shopee.dal.dataobject.accounthistory.AccountHistoryDO;
import cn.iocoder.yudao.framework.common.pojo.PageResult;

/**
 * 虾皮账户历史 Service 接口
 *
 * @author 芋道源码
 */
public interface AccountHistoryService {

    /**
     * 创建虾皮账户历史
     *
     * @param createReqVO 创建信息
     * @return 编号
     */
    Long createAccountHistory(@Valid AccountHistoryCreateReqVO createReqVO);

    /**
     * 更新虾皮账户历史
     *
     * @param updateReqVO 更新信息
     */
    void updateAccountHistory(@Valid AccountHistoryUpdateReqVO updateReqVO);

    /**
     * 删除虾皮账户历史
     *
     * @param id 编号
     */
    void deleteAccountHistory(Long id);

    /**
     * 获得虾皮账户历史
     *
     * @param id 编号
     * @return 虾皮账户历史
     */
    AccountHistoryDO getAccountHistory(Long id);

    /**
     * 获得虾皮账户历史列表
     *
     * @param ids 编号
     * @return 虾皮账户历史列表
     */
    List<AccountHistoryDO> getAccountHistoryList(Collection<Long> ids);

    /**
     * 获得虾皮账户历史分页
     *
     * @param pageReqVO 分页查询
     * @return 虾皮账户历史分页
     */
    PageResult<AccountHistoryDO> getAccountHistoryPage(AccountHistoryPageReqVO pageReqVO);

    /**
     * 获得虾皮账户历史列表, 用于 Excel 导出
     *
     * @param exportReqVO 查询条件
     * @return 虾皮账户历史列表
     */
    List<AccountHistoryDO> getAccountHistoryList(AccountHistoryExportReqVO exportReqVO);

}
