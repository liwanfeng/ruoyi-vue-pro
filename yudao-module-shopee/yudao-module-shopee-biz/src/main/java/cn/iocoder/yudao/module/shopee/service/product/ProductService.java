package cn.iocoder.yudao.module.shopee.service.product;

import java.util.*;
import javax.validation.*;
import cn.iocoder.yudao.module.shopee.controller.admin.product.vo.*;
import cn.iocoder.yudao.module.shopee.dal.dataobject.product.ProductDO;
import cn.iocoder.yudao.framework.common.pojo.PageResult;
import cn.iocoder.yudao.module.shopee.dal.dataobject.shop.ShopDO;

/**
 * 虾皮商品 Service 接口
 *
 * @author 芋道源码
 */
public interface ProductService {

    /**
     * 创建虾皮商品
     *
     * @param createReqVO 创建信息
     * @return 编号
     */
    Long createProduct(@Valid ProductCreateReqVO createReqVO);

    /**
     * 更新虾皮商品
     *
     * @param updateReqVO 更新信息
     */
    void updateProduct(@Valid ProductUpdateReqVO updateReqVO);

    /**
     * 删除虾皮商品
     *
     * @param id 编号
     */
    void deleteProduct(Long id);

    /**
     * 获得虾皮商品
     *
     * @param id 编号
     * @return 虾皮商品
     */
    ProductDO getProduct(Long id);

    /**
     * 获得虾皮商品列表
     *
     * @param ids 编号
     * @return 虾皮商品列表
     */
    List<ProductDO> getProductList(Collection<Long> ids);

    /**
     * 获得虾皮商品分页
     *
     * @param pageReqVO 分页查询
     * @return 虾皮商品分页
     */
    PageResult<ProductDO> getProductPage(ProductPageReqVO pageReqVO);

    /**
     * 获得虾皮商品列表, 用于 Excel 导出
     *
     * @param exportReqVO 查询条件
     * @return 虾皮商品列表
     */
    List<ProductDO> getProductList(ProductExportReqVO exportReqVO);

    void updateFromRemote(List<ShopDO> shopDOList);
}
