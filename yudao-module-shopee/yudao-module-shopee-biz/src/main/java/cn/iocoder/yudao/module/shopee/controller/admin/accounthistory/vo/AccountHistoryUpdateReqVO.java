package cn.iocoder.yudao.module.shopee.controller.admin.accounthistory.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;
import java.util.*;
import javax.validation.constraints.*;

@Schema(description = "管理后台 - 虾皮账户历史更新 Request VO")
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class AccountHistoryUpdateReqVO extends AccountHistoryBaseVO {

    @Schema(description = "主键", requiredMode = Schema.RequiredMode.REQUIRED, example = "31714")
    @NotNull(message = "主键不能为空")
    private Long id;

}
