package cn.iocoder.yudao.module.shopee.controller.admin.shophistory;

import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import org.springframework.validation.annotation.Validated;
import org.springframework.security.access.prepost.PreAuthorize;
import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Operation;

import javax.validation.constraints.*;
import javax.validation.*;
import javax.servlet.http.*;
import java.util.*;
import java.io.IOException;

import cn.iocoder.yudao.framework.common.pojo.PageResult;
import cn.iocoder.yudao.framework.common.pojo.CommonResult;
import static cn.iocoder.yudao.framework.common.pojo.CommonResult.success;

import cn.iocoder.yudao.framework.excel.core.util.ExcelUtils;

import cn.iocoder.yudao.framework.operatelog.core.annotations.OperateLog;
import static cn.iocoder.yudao.framework.operatelog.core.enums.OperateTypeEnum.*;

import cn.iocoder.yudao.module.shopee.controller.admin.shophistory.vo.*;
import cn.iocoder.yudao.module.shopee.dal.dataobject.shophistory.ShopHistoryDO;
import cn.iocoder.yudao.module.shopee.convert.shophistory.ShopHistoryConvert;
import cn.iocoder.yudao.module.shopee.service.shophistory.ShopHistoryService;

@Tag(name = "管理后台 - 虾皮店铺历史")
@RestController
@RequestMapping("/shopee/shop-history")
@Validated
public class ShopHistoryController {

    @Resource
    private ShopHistoryService shopHistoryService;

    @PostMapping("/create")
    @Operation(summary = "创建虾皮店铺历史")
    @PreAuthorize("@ss.hasPermission('shopee:shop-history:create')")
    public CommonResult<Long> createShopHistory(@Valid @RequestBody ShopHistoryCreateReqVO createReqVO) {
        return success(shopHistoryService.createShopHistory(createReqVO));
    }

    @PutMapping("/update")
    @Operation(summary = "更新虾皮店铺历史")
    @PreAuthorize("@ss.hasPermission('shopee:shop-history:update')")
    public CommonResult<Boolean> updateShopHistory(@Valid @RequestBody ShopHistoryUpdateReqVO updateReqVO) {
        shopHistoryService.updateShopHistory(updateReqVO);
        return success(true);
    }

    @DeleteMapping("/delete")
    @Operation(summary = "删除虾皮店铺历史")
    @Parameter(name = "id", description = "编号", required = true)
    @PreAuthorize("@ss.hasPermission('shopee:shop-history:delete')")
    public CommonResult<Boolean> deleteShopHistory(@RequestParam("id") Long id) {
        shopHistoryService.deleteShopHistory(id);
        return success(true);
    }

    @GetMapping("/get")
    @Operation(summary = "获得虾皮店铺历史")
    @Parameter(name = "id", description = "编号", required = true, example = "1024")
    @PreAuthorize("@ss.hasPermission('shopee:shop-history:query')")
    public CommonResult<ShopHistoryRespVO> getShopHistory(@RequestParam("id") Long id) {
        ShopHistoryDO shopHistory = shopHistoryService.getShopHistory(id);
        return success(ShopHistoryConvert.INSTANCE.convert(shopHistory));
    }

    @GetMapping("/list")
    @Operation(summary = "获得虾皮店铺历史列表")
    @Parameter(name = "ids", description = "编号列表", required = true, example = "1024,2048")
    @PreAuthorize("@ss.hasPermission('shopee:shop-history:query')")
    public CommonResult<List<ShopHistoryRespVO>> getShopHistoryList(@RequestParam("ids") Collection<Long> ids) {
        List<ShopHistoryDO> list = shopHistoryService.getShopHistoryList(ids);
        return success(ShopHistoryConvert.INSTANCE.convertList(list));
    }

    @GetMapping("/page")
    @Operation(summary = "获得虾皮店铺历史分页")
    @PreAuthorize("@ss.hasPermission('shopee:shop-history:query')")
    public CommonResult<PageResult<ShopHistoryRespVO>> getShopHistoryPage(@Valid ShopHistoryPageReqVO pageVO) {
        PageResult<ShopHistoryDO> pageResult = shopHistoryService.getShopHistoryPage(pageVO);
        return success(ShopHistoryConvert.INSTANCE.convertPage(pageResult));
    }

    @GetMapping("/export-excel")
    @Operation(summary = "导出虾皮店铺历史 Excel")
    @PreAuthorize("@ss.hasPermission('shopee:shop-history:export')")
    @OperateLog(type = EXPORT)
    public void exportShopHistoryExcel(@Valid ShopHistoryExportReqVO exportReqVO,
              HttpServletResponse response) throws IOException {
        List<ShopHistoryDO> list = shopHistoryService.getShopHistoryList(exportReqVO);
        // 导出 Excel
        List<ShopHistoryExcelVO> datas = ShopHistoryConvert.INSTANCE.convertList02(list);
        ExcelUtils.write(response, "虾皮店铺历史.xls", "数据", ShopHistoryExcelVO.class, datas);
    }

}
