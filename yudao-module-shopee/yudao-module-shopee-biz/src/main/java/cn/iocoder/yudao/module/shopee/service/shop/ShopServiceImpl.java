package cn.iocoder.yudao.module.shopee.service.shop;

import cn.hutool.core.bean.BeanUtil;
import cn.iocoder.boot.yudao.module.spider.shopee.seller.SellerSpider;
import cn.iocoder.boot.yudao.module.spider.shopee.seller.request.SellerRequest;
import cn.iocoder.boot.yudao.module.spider.shopee.seller.response.shop.SellerShopResponse;
import cn.iocoder.yudao.framework.common.pojo.PageResult;
import cn.iocoder.yudao.module.shopee.controller.admin.shop.vo.ShopCreateReqVO;
import cn.iocoder.yudao.module.shopee.controller.admin.shop.vo.ShopExportReqVO;
import cn.iocoder.yudao.module.shopee.controller.admin.shop.vo.ShopPageReqVO;
import cn.iocoder.yudao.module.shopee.controller.admin.shop.vo.ShopUpdateReqVO;
import cn.iocoder.yudao.module.shopee.convert.shop.ShopConvert;
import cn.iocoder.yudao.module.shopee.dal.dataobject.shop.ShopDO;
import cn.iocoder.yudao.module.shopee.dal.dataobject.shophistory.ShopHistoryDO;
import cn.iocoder.yudao.module.shopee.dal.mysql.shop.ShopMapper;
import cn.iocoder.yudao.module.shopee.dal.mysql.shophistory.ShopHistoryMapper;
import cn.iocoder.yudao.module.shopee.utils.AccountRedisUtils;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import javax.annotation.Resource;
import java.util.Collection;
import java.util.List;

import static cn.iocoder.yudao.framework.common.exception.util.ServiceExceptionUtil.exception;
import static cn.iocoder.yudao.module.shopee.enums.ErrorCodeConstants.SHOP_NOT_EXISTS;

/**
 * 虾皮店铺 Service 实现类
 *
 * @author 芋道源码
 */
@Service
@Validated
public class ShopServiceImpl implements ShopService {

    @Resource
    private AccountRedisUtils accountRedisUtils;
    @Resource
    private ShopMapper shopMapper;
    @Resource
    private ShopHistoryMapper shopHistoryMapper;

    @Override
    public Long createShop(ShopCreateReqVO createReqVO) {
        // 插入
        ShopDO shop = ShopConvert.INSTANCE.convert(createReqVO);
        shopMapper.insert(shop);
        // 返回
        return shop.getId();
    }

    @Override
    public void updateShop(ShopUpdateReqVO updateReqVO) {
        // 校验存在
        validateShopExists(updateReqVO.getId());
        // 更新
        ShopDO updateObj = ShopConvert.INSTANCE.convert(updateReqVO);
        shopMapper.updateById(updateObj);
    }

    @Override
    public void deleteShop(Long id) {
        // 校验存在
        validateShopExists(id);
        // 删除
        shopMapper.deleteById(id);
    }

    private void validateShopExists(Long id) {
        if (shopMapper.selectById(id) == null) {
            throw exception(SHOP_NOT_EXISTS);
        }
    }

    @Override
    public ShopDO getShop(Long id) {
        return shopMapper.selectById(id);
    }

    @Override
    public List<ShopDO> getShopList(Collection<Long> ids) {
        return shopMapper.selectBatchIds(ids);
    }

    @Override
    public PageResult<ShopDO> getShopPage(ShopPageReqVO pageReqVO) {
        return shopMapper.selectPage(pageReqVO);
    }

    @Override
    public List<ShopDO> getShopList(ShopExportReqVO exportReqVO) {
        return shopMapper.selectList(exportReqVO);
    }

    @Override
    public void upsertFromRemote(List<Long> accountIdList) {
        for (Long accountId : accountIdList) {
            String cookie = accountRedisUtils.getCookie(accountId);
            SellerRequest request = SellerRequest.builder().cookie(cookie).accountId(accountId).build();
            List<SellerShopResponse> merchantShopFullList = SellerSpider.getMerchantShopFullList(request);
            List<ShopDO> shopDOS = BeanUtil.copyToList(merchantShopFullList, ShopDO.class);
            for (ShopDO shopDO : shopDOS) {
                shopMapper.upsert(shopDO);
            }
            List<ShopHistoryDO> shopHistoryDOS = BeanUtil.copyToList(merchantShopFullList, ShopHistoryDO.class);
            shopHistoryMapper.insertBatch(shopHistoryDOS);
        }
    }
}
