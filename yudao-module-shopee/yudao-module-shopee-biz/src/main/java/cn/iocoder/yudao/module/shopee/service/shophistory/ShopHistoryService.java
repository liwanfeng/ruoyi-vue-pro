package cn.iocoder.yudao.module.shopee.service.shophistory;

import java.util.*;
import javax.validation.*;
import cn.iocoder.yudao.module.shopee.controller.admin.shophistory.vo.*;
import cn.iocoder.yudao.module.shopee.dal.dataobject.shophistory.ShopHistoryDO;
import cn.iocoder.yudao.framework.common.pojo.PageResult;

/**
 * 虾皮店铺历史 Service 接口
 *
 * @author 芋道源码
 */
public interface ShopHistoryService {

    /**
     * 创建虾皮店铺历史
     *
     * @param createReqVO 创建信息
     * @return 编号
     */
    Long createShopHistory(@Valid ShopHistoryCreateReqVO createReqVO);

    /**
     * 更新虾皮店铺历史
     *
     * @param updateReqVO 更新信息
     */
    void updateShopHistory(@Valid ShopHistoryUpdateReqVO updateReqVO);

    /**
     * 删除虾皮店铺历史
     *
     * @param id 编号
     */
    void deleteShopHistory(Long id);

    /**
     * 获得虾皮店铺历史
     *
     * @param id 编号
     * @return 虾皮店铺历史
     */
    ShopHistoryDO getShopHistory(Long id);

    /**
     * 获得虾皮店铺历史列表
     *
     * @param ids 编号
     * @return 虾皮店铺历史列表
     */
    List<ShopHistoryDO> getShopHistoryList(Collection<Long> ids);

    /**
     * 获得虾皮店铺历史分页
     *
     * @param pageReqVO 分页查询
     * @return 虾皮店铺历史分页
     */
    PageResult<ShopHistoryDO> getShopHistoryPage(ShopHistoryPageReqVO pageReqVO);

    /**
     * 获得虾皮店铺历史列表, 用于 Excel 导出
     *
     * @param exportReqVO 查询条件
     * @return 虾皮店铺历史列表
     */
    List<ShopHistoryDO> getShopHistoryList(ShopHistoryExportReqVO exportReqVO);

}
