package cn.iocoder.yudao.module.shopee.service.product;

import cn.hutool.core.bean.BeanUtil;
import cn.iocoder.boot.yudao.module.spider.shopee.seller.SellerSpider;
import cn.iocoder.boot.yudao.module.spider.shopee.seller.request.SellerRequest;
import cn.iocoder.boot.yudao.module.spider.shopee.seller.response.product.SellerShopProductDetailResponse;
import cn.iocoder.yudao.module.shopee.dal.dataobject.producthistory.ProductHistoryDO;
import cn.iocoder.yudao.module.shopee.dal.dataobject.shop.ShopDO;
import cn.iocoder.yudao.module.shopee.dal.mysql.producthistory.ProductHistoryMapper;
import cn.iocoder.yudao.module.shopee.utils.AccountRedisUtils;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import org.springframework.validation.annotation.Validated;

import java.util.*;
import cn.iocoder.yudao.module.shopee.controller.admin.product.vo.*;
import cn.iocoder.yudao.module.shopee.dal.dataobject.product.ProductDO;
import cn.iocoder.yudao.framework.common.pojo.PageResult;

import cn.iocoder.yudao.module.shopee.convert.product.ProductConvert;
import cn.iocoder.yudao.module.shopee.dal.mysql.product.ProductMapper;

import static cn.iocoder.yudao.framework.common.exception.util.ServiceExceptionUtil.exception;
import static cn.iocoder.yudao.module.shopee.enums.ErrorCodeConstants.*;

/**
 * 虾皮商品 Service 实现类
 *
 * @author 芋道源码
 */
@Service
@Validated
public class ProductServiceImpl implements ProductService {

    @Resource
    private ProductMapper productMapper;
    @Resource
    private ProductHistoryMapper productHistoryMapper;
    @Resource
    private AccountRedisUtils accountRedisUtils;

    @Override
    public Long createProduct(ProductCreateReqVO createReqVO) {
        // 插入
        ProductDO product = ProductConvert.INSTANCE.convert(createReqVO);
        productMapper.insert(product);
        // 返回
        return product.getId();
    }

    @Override
    public void updateProduct(ProductUpdateReqVO updateReqVO) {
        // 校验存在
        validateProductExists(updateReqVO.getId());
        // 更新
        ProductDO updateObj = ProductConvert.INSTANCE.convert(updateReqVO);
        productMapper.updateById(updateObj);
    }

    @Override
    public void deleteProduct(Long id) {
        // 校验存在
        validateProductExists(id);
        // 删除
        productMapper.deleteById(id);
    }

    private void validateProductExists(Long id) {
        if (productMapper.selectById(id) == null) {
            throw exception(PRODUCT_NOT_EXISTS);
        }
    }

    @Override
    public ProductDO getProduct(Long id) {
        return productMapper.selectById(id);
    }

    @Override
    public List<ProductDO> getProductList(Collection<Long> ids) {
        return productMapper.selectBatchIds(ids);
    }

    @Override
    public PageResult<ProductDO> getProductPage(ProductPageReqVO pageReqVO) {
        return productMapper.selectPage(pageReqVO);
    }

    @Override
    public List<ProductDO> getProductList(ProductExportReqVO exportReqVO) {
        return productMapper.selectList(exportReqVO);
    }

    @Override
    public void updateFromRemote(List<ShopDO> shopDOList) {
        for (ShopDO shopDO : shopDOList) {
            String cookie = accountRedisUtils.getCookie(shopDO.getAccountId());
            List<SellerShopProductDetailResponse> mpskuList = SellerSpider.getMpskuList(SellerRequest.builder().accountId(shopDO.getAccountId()).cookie(cookie).build());
            List<ProductDO> productDOList = BeanUtil.copyToList(mpskuList, ProductDO.class);
            for (ProductDO productDO : productDOList) {
                productMapper.upsert(productDO);
            }
            List<ProductHistoryDO> productHistoryDOList = BeanUtil.copyToList(mpskuList, ProductHistoryDO.class);
            productHistoryMapper.insertBatch(productHistoryDOList);
        }
    }

}
