package cn.iocoder.yudao.module.shopee.controller.admin.account;

import cn.iocoder.yudao.framework.common.enums.CommonStatusEnum;
import cn.iocoder.yudao.framework.common.pojo.CommonResult;
import cn.iocoder.yudao.framework.common.pojo.PageResult;
import cn.iocoder.yudao.framework.excel.core.util.ExcelUtils;
import cn.iocoder.yudao.framework.operatelog.core.annotations.OperateLog;
import cn.iocoder.yudao.module.shopee.controller.admin.account.vo.*;
import cn.iocoder.yudao.module.shopee.convert.account.AccountConvert;
import cn.iocoder.yudao.module.shopee.dal.dataobject.account.AccountDO;
import cn.iocoder.yudao.module.shopee.service.account.AccountService;
import cn.iocoder.yudao.module.shopee.utils.AccountRedisUtils;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.util.Collection;
import java.util.List;

import static cn.iocoder.yudao.framework.common.pojo.CommonResult.error;
import static cn.iocoder.yudao.framework.common.pojo.CommonResult.success;
import static cn.iocoder.yudao.framework.operatelog.core.enums.OperateTypeEnum.EXPORT;
import static cn.iocoder.yudao.module.shopee.enums.ErrorCodeConstants.ACCOUNT_NOT_EXISTS;

@Tag(name = "管理后台 - 虾皮账户")
@RestController
@RequestMapping("/shopee/account")
@Validated
public class AccountController {

    @Resource
    private AccountService accountService;
    @Resource
    private AccountRedisUtils accountRedisUtils;

    @PostMapping("/create")
    @Operation(summary = "创建虾皮账户")
    @PreAuthorize("@ss.hasPermission('shopee:account:create')")
    public CommonResult<String> createAccount(@Valid @RequestBody AccountCreateReqVO createReqVO) {
        accountService.updateRemoteAccount(createReqVO.getCookie());

        return success("创建成功");
    }

    @PutMapping("/update")
    @Operation(summary = "更新虾皮账户")
    @PreAuthorize("@ss.hasPermission('shopee:account:update')")
    public CommonResult<String> updateAccount(@Valid @RequestBody AccountUpdateReqVO updateReqVO) {
        accountService.updateRemoteAccount(updateReqVO.getCookie());
        return success("修改成功");
    }

    @DeleteMapping("/delete")
    @Operation(summary = "删除虾皮账户")
    @Parameter(name = "id", description = "编号", required = true)
    @PreAuthorize("@ss.hasPermission('shopee:account:delete')")
    public CommonResult<Boolean> deleteAccount(@RequestParam("id") Long id) {
        accountService.deleteAccount(id);
        return success(true);
    }

    @GetMapping("/get")
    @Operation(summary = "获得虾皮账户")
    @Parameter(name = "id", description = "编号", required = true, example = "1024")
    @PreAuthorize("@ss.hasPermission('shopee:account:query')")
    public CommonResult<AccountRespVO> getAccount(@RequestParam("id") Long id) {
        AccountDO account = accountService.getAccount(id);
        return success(AccountConvert.INSTANCE.convert(account));
    }

    @GetMapping("/list")
    @Operation(summary = "获得虾皮账户列表")
    @Parameter(name = "ids", description = "编号列表", required = true, example = "1024,2048")
    @PreAuthorize("@ss.hasPermission('shopee:account:query')")
    public CommonResult<List<AccountRespVO>> getAccountList(@RequestParam("ids") Collection<Long> ids) {
        List<AccountDO> list = accountService.getAccountList(ids);
        List<AccountRespVO> accountRespVOS = AccountConvert.INSTANCE.convertList(list);
        accountRespVOS.forEach(e -> {
            if (accountRedisUtils.hasCookie(e.getAccountId())) {
                e.setCookieFlag(CommonStatusEnum.ENABLE.getStatus());
            } else {
                e.setCookieFlag(CommonStatusEnum.DISABLE.getStatus());
            }
        });
        return success(accountRespVOS);
    }

    @GetMapping("/page")
    @Operation(summary = "获得虾皮账户分页")
    @PreAuthorize("@ss.hasPermission('shopee:account:query')")
    public CommonResult<PageResult<AccountRespVO>> getAccountPage(@Valid AccountPageReqVO pageVO) {
        PageResult<AccountDO> pageResult = accountService.getAccountPage(pageVO);
        PageResult<AccountRespVO> accountRespVOPageResult = AccountConvert.INSTANCE.convertPage(pageResult);
        accountRespVOPageResult.getList().forEach(e -> {
            if (accountRedisUtils.hasCookie(e.getAccountId())) {
                e.setCookieFlag(CommonStatusEnum.ENABLE.getStatus());
            } else {
                e.setCookieFlag(CommonStatusEnum.DISABLE.getStatus());
            }
        });
        return success(accountRespVOPageResult);
    }

    @GetMapping("/refreshAccount")
    @Operation(summary = "获得虾皮账户分页")
    @PreAuthorize("@ss.hasPermission('shopee:account:query')")
    public CommonResult<String> refreshAccount(@RequestParam("id") Long id) {
        AccountDO accountDO = accountService.getAccount(id);
        if (accountDO == null) {
            return error(ACCOUNT_NOT_EXISTS);
        }
        String cookie = accountRedisUtils.getCookie(accountDO.getAccountId());
        accountService.updateRemoteAccount(cookie);
        return success("刷新成功");
    }

    @GetMapping("/export-excel")
    @Operation(summary = "导出虾皮账户 Excel")
    @PreAuthorize("@ss.hasPermission('shopee:account:export')")
    @OperateLog(type = EXPORT)
    public void exportAccountExcel(@Valid AccountExportReqVO exportReqVO,
                                   HttpServletResponse response) throws IOException {
        List<AccountDO> list = accountService.getAccountList(exportReqVO);
        // 导出 Excel
        List<AccountExcelVO> datas = AccountConvert.INSTANCE.convertList02(list);
        ExcelUtils.write(response, "虾皮账户.xls", "数据", AccountExcelVO.class, datas);
    }


}
