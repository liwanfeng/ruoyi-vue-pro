package cn.iocoder.yudao.module.shopee.convert.producthistory;

import java.util.*;

import cn.iocoder.yudao.framework.common.pojo.PageResult;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import cn.iocoder.yudao.module.shopee.controller.admin.producthistory.vo.*;
import cn.iocoder.yudao.module.shopee.dal.dataobject.producthistory.ProductHistoryDO;

/**
 * 虾皮商品历史 Convert
 *
 * @author 芋道源码
 */
@Mapper
public interface ProductHistoryConvert {

    ProductHistoryConvert INSTANCE = Mappers.getMapper(ProductHistoryConvert.class);

    ProductHistoryDO convert(ProductHistoryCreateReqVO bean);

    ProductHistoryDO convert(ProductHistoryUpdateReqVO bean);

    ProductHistoryRespVO convert(ProductHistoryDO bean);

    List<ProductHistoryRespVO> convertList(List<ProductHistoryDO> list);

    PageResult<ProductHistoryRespVO> convertPage(PageResult<ProductHistoryDO> page);

    List<ProductHistoryExcelVO> convertList02(List<ProductHistoryDO> list);

}
