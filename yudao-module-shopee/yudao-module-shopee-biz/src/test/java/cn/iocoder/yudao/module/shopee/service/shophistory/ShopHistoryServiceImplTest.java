package cn.iocoder.yudao.module.shopee.service.shophistory;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.mock.mockito.MockBean;

import javax.annotation.Resource;

import cn.iocoder.yudao.framework.test.core.ut.BaseDbUnitTest;

import cn.iocoder.yudao.module.shopee.controller.admin.shophistory.vo.*;
import cn.iocoder.yudao.module.shopee.dal.dataobject.shophistory.ShopHistoryDO;
import cn.iocoder.yudao.module.shopee.dal.mysql.shophistory.ShopHistoryMapper;
import cn.iocoder.yudao.framework.common.pojo.PageResult;

import javax.annotation.Resource;
import org.springframework.context.annotation.Import;
import java.util.*;
import java.time.LocalDateTime;

import static cn.hutool.core.util.RandomUtil.*;
import static cn.iocoder.yudao.module.shopee.enums.ErrorCodeConstants.*;
import static cn.iocoder.yudao.framework.test.core.util.AssertUtils.*;
import static cn.iocoder.yudao.framework.test.core.util.RandomUtils.*;
import static cn.iocoder.yudao.framework.common.util.date.LocalDateTimeUtils.*;
import static cn.iocoder.yudao.framework.common.util.object.ObjectUtils.*;
import static cn.iocoder.yudao.framework.common.util.date.DateUtils.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

/**
 * {@link ShopHistoryServiceImpl} 的单元测试类
 *
 * @author 芋道源码
 */
@Import(ShopHistoryServiceImpl.class)
public class ShopHistoryServiceImplTest extends BaseDbUnitTest {

    @Resource
    private ShopHistoryServiceImpl shopHistoryService;

    @Resource
    private ShopHistoryMapper shopHistoryMapper;

    @Test
    public void testCreateShopHistory_success() {
        // 准备参数
        ShopHistoryCreateReqVO reqVO = randomPojo(ShopHistoryCreateReqVO.class);

        // 调用
        Long shopHistoryId = shopHistoryService.createShopHistory(reqVO);
        // 断言
        assertNotNull(shopHistoryId);
        // 校验记录的属性是否正确
        ShopHistoryDO shopHistory = shopHistoryMapper.selectById(shopHistoryId);
        assertPojoEquals(reqVO, shopHistory);
    }

    @Test
    public void testUpdateShopHistory_success() {
        // mock 数据
        ShopHistoryDO dbShopHistory = randomPojo(ShopHistoryDO.class);
        shopHistoryMapper.insert(dbShopHistory);// @Sql: 先插入出一条存在的数据
        // 准备参数
        ShopHistoryUpdateReqVO reqVO = randomPojo(ShopHistoryUpdateReqVO.class, o -> {
            o.setId(dbShopHistory.getId()); // 设置更新的 ID
        });

        // 调用
        shopHistoryService.updateShopHistory(reqVO);
        // 校验是否更新正确
        ShopHistoryDO shopHistory = shopHistoryMapper.selectById(reqVO.getId()); // 获取最新的
        assertPojoEquals(reqVO, shopHistory);
    }

    @Test
    public void testUpdateShopHistory_notExists() {
        // 准备参数
        ShopHistoryUpdateReqVO reqVO = randomPojo(ShopHistoryUpdateReqVO.class);

        // 调用, 并断言异常
        assertServiceException(() -> shopHistoryService.updateShopHistory(reqVO), SHOP_HISTORY_NOT_EXISTS);
    }

    @Test
    public void testDeleteShopHistory_success() {
        // mock 数据
        ShopHistoryDO dbShopHistory = randomPojo(ShopHistoryDO.class);
        shopHistoryMapper.insert(dbShopHistory);// @Sql: 先插入出一条存在的数据
        // 准备参数
        Long id = dbShopHistory.getId();

        // 调用
        shopHistoryService.deleteShopHistory(id);
       // 校验数据不存在了
       assertNull(shopHistoryMapper.selectById(id));
    }

    @Test
    public void testDeleteShopHistory_notExists() {
        // 准备参数
        Long id = randomLongId();

        // 调用, 并断言异常
        assertServiceException(() -> shopHistoryService.deleteShopHistory(id), SHOP_HISTORY_NOT_EXISTS);
    }

    @Test
    @Disabled  // TODO 请修改 null 为需要的值，然后删除 @Disabled 注解
    public void testGetShopHistoryPage() {
       // mock 数据
       ShopHistoryDO dbShopHistory = randomPojo(ShopHistoryDO.class, o -> { // 等会查询到
           o.setSysUserId(null);
           o.setAccountId(null);
           o.setUsername(null);
           o.setUserId(null);
           o.setRegion(null);
           o.setEnabled(null);
           o.setIsSipAffiliated(null);
           o.setShopName(null);
           o.setShopId(null);
           o.setLastLogin(null);
           o.setIsSipPrimary(null);
           o.setPortrait(null);
           o.setCbOption(null);
           o.setIsInactive(null);
           o.setIs3PfShop(null);
           o.setCreateTime(null);
       });
       shopHistoryMapper.insert(dbShopHistory);
       // 测试 sysUserId 不匹配
       shopHistoryMapper.insert(cloneIgnoreId(dbShopHistory, o -> o.setSysUserId(null)));
       // 测试 accountId 不匹配
       shopHistoryMapper.insert(cloneIgnoreId(dbShopHistory, o -> o.setAccountId(null)));
       // 测试 username 不匹配
       shopHistoryMapper.insert(cloneIgnoreId(dbShopHistory, o -> o.setUsername(null)));
       // 测试 userId 不匹配
       shopHistoryMapper.insert(cloneIgnoreId(dbShopHistory, o -> o.setUserId(null)));
       // 测试 region 不匹配
       shopHistoryMapper.insert(cloneIgnoreId(dbShopHistory, o -> o.setRegion(null)));
       // 测试 enabled 不匹配
       shopHistoryMapper.insert(cloneIgnoreId(dbShopHistory, o -> o.setEnabled(null)));
       // 测试 isSipAffiliated 不匹配
       shopHistoryMapper.insert(cloneIgnoreId(dbShopHistory, o -> o.setIsSipAffiliated(null)));
       // 测试 shopName 不匹配
       shopHistoryMapper.insert(cloneIgnoreId(dbShopHistory, o -> o.setShopName(null)));
       // 测试 shopId 不匹配
       shopHistoryMapper.insert(cloneIgnoreId(dbShopHistory, o -> o.setShopId(null)));
       // 测试 lastLogin 不匹配
       shopHistoryMapper.insert(cloneIgnoreId(dbShopHistory, o -> o.setLastLogin(null)));
       // 测试 isSipPrimary 不匹配
       shopHistoryMapper.insert(cloneIgnoreId(dbShopHistory, o -> o.setIsSipPrimary(null)));
       // 测试 portrait 不匹配
       shopHistoryMapper.insert(cloneIgnoreId(dbShopHistory, o -> o.setPortrait(null)));
       // 测试 cbOption 不匹配
       shopHistoryMapper.insert(cloneIgnoreId(dbShopHistory, o -> o.setCbOption(null)));
       // 测试 isInactive 不匹配
       shopHistoryMapper.insert(cloneIgnoreId(dbShopHistory, o -> o.setIsInactive(null)));
       // 测试 is3PfShop 不匹配
       shopHistoryMapper.insert(cloneIgnoreId(dbShopHistory, o -> o.setIs3PfShop(null)));
       // 测试 createTime 不匹配
       shopHistoryMapper.insert(cloneIgnoreId(dbShopHistory, o -> o.setCreateTime(null)));
       // 准备参数
       ShopHistoryPageReqVO reqVO = new ShopHistoryPageReqVO();
       reqVO.setSysUserId(null);
       reqVO.setAccountId(null);
       reqVO.setUsername(null);
       reqVO.setUserId(null);
       reqVO.setRegion(null);
       reqVO.setEnabled(null);
       reqVO.setIsSipAffiliated(null);
       reqVO.setShopName(null);
       reqVO.setShopId(null);
       reqVO.setLastLogin(null);
       reqVO.setIsSipPrimary(null);
       reqVO.setPortrait(null);
       reqVO.setCbOption(null);
       reqVO.setIsInactive(null);
       reqVO.setIs3PfShop(null);
       reqVO.setCreateTime(buildBetweenTime(2023, 2, 1, 2023, 2, 28));

       // 调用
       PageResult<ShopHistoryDO> pageResult = shopHistoryService.getShopHistoryPage(reqVO);
       // 断言
       assertEquals(1, pageResult.getTotal());
       assertEquals(1, pageResult.getList().size());
       assertPojoEquals(dbShopHistory, pageResult.getList().get(0));
    }

    @Test
    @Disabled  // TODO 请修改 null 为需要的值，然后删除 @Disabled 注解
    public void testGetShopHistoryList() {
       // mock 数据
       ShopHistoryDO dbShopHistory = randomPojo(ShopHistoryDO.class, o -> { // 等会查询到
           o.setSysUserId(null);
           o.setAccountId(null);
           o.setUsername(null);
           o.setUserId(null);
           o.setRegion(null);
           o.setEnabled(null);
           o.setIsSipAffiliated(null);
           o.setShopName(null);
           o.setShopId(null);
           o.setLastLogin(null);
           o.setIsSipPrimary(null);
           o.setPortrait(null);
           o.setCbOption(null);
           o.setIsInactive(null);
           o.setIs3PfShop(null);
           o.setCreateTime(null);
       });
       shopHistoryMapper.insert(dbShopHistory);
       // 测试 sysUserId 不匹配
       shopHistoryMapper.insert(cloneIgnoreId(dbShopHistory, o -> o.setSysUserId(null)));
       // 测试 accountId 不匹配
       shopHistoryMapper.insert(cloneIgnoreId(dbShopHistory, o -> o.setAccountId(null)));
       // 测试 username 不匹配
       shopHistoryMapper.insert(cloneIgnoreId(dbShopHistory, o -> o.setUsername(null)));
       // 测试 userId 不匹配
       shopHistoryMapper.insert(cloneIgnoreId(dbShopHistory, o -> o.setUserId(null)));
       // 测试 region 不匹配
       shopHistoryMapper.insert(cloneIgnoreId(dbShopHistory, o -> o.setRegion(null)));
       // 测试 enabled 不匹配
       shopHistoryMapper.insert(cloneIgnoreId(dbShopHistory, o -> o.setEnabled(null)));
       // 测试 isSipAffiliated 不匹配
       shopHistoryMapper.insert(cloneIgnoreId(dbShopHistory, o -> o.setIsSipAffiliated(null)));
       // 测试 shopName 不匹配
       shopHistoryMapper.insert(cloneIgnoreId(dbShopHistory, o -> o.setShopName(null)));
       // 测试 shopId 不匹配
       shopHistoryMapper.insert(cloneIgnoreId(dbShopHistory, o -> o.setShopId(null)));
       // 测试 lastLogin 不匹配
       shopHistoryMapper.insert(cloneIgnoreId(dbShopHistory, o -> o.setLastLogin(null)));
       // 测试 isSipPrimary 不匹配
       shopHistoryMapper.insert(cloneIgnoreId(dbShopHistory, o -> o.setIsSipPrimary(null)));
       // 测试 portrait 不匹配
       shopHistoryMapper.insert(cloneIgnoreId(dbShopHistory, o -> o.setPortrait(null)));
       // 测试 cbOption 不匹配
       shopHistoryMapper.insert(cloneIgnoreId(dbShopHistory, o -> o.setCbOption(null)));
       // 测试 isInactive 不匹配
       shopHistoryMapper.insert(cloneIgnoreId(dbShopHistory, o -> o.setIsInactive(null)));
       // 测试 is3PfShop 不匹配
       shopHistoryMapper.insert(cloneIgnoreId(dbShopHistory, o -> o.setIs3PfShop(null)));
       // 测试 createTime 不匹配
       shopHistoryMapper.insert(cloneIgnoreId(dbShopHistory, o -> o.setCreateTime(null)));
       // 准备参数
       ShopHistoryExportReqVO reqVO = new ShopHistoryExportReqVO();
       reqVO.setSysUserId(null);
       reqVO.setAccountId(null);
       reqVO.setUsername(null);
       reqVO.setUserId(null);
       reqVO.setRegion(null);
       reqVO.setEnabled(null);
       reqVO.setIsSipAffiliated(null);
       reqVO.setShopName(null);
       reqVO.setShopId(null);
       reqVO.setLastLogin(null);
       reqVO.setIsSipPrimary(null);
       reqVO.setPortrait(null);
       reqVO.setCbOption(null);
       reqVO.setIsInactive(null);
       reqVO.setIs3PfShop(null);
       reqVO.setCreateTime(buildBetweenTime(2023, 2, 1, 2023, 2, 28));

       // 调用
       List<ShopHistoryDO> list = shopHistoryService.getShopHistoryList(reqVO);
       // 断言
       assertEquals(1, list.size());
       assertPojoEquals(dbShopHistory, list.get(0));
    }

}
