package cn.iocoder.yudao.module.shopee.service.shop;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.mock.mockito.MockBean;

import javax.annotation.Resource;

import cn.iocoder.yudao.framework.test.core.ut.BaseDbUnitTest;

import cn.iocoder.yudao.module.shopee.controller.admin.shop.vo.*;
import cn.iocoder.yudao.module.shopee.dal.dataobject.shop.ShopDO;
import cn.iocoder.yudao.module.shopee.dal.mysql.shop.ShopMapper;
import cn.iocoder.yudao.framework.common.pojo.PageResult;

import javax.annotation.Resource;
import org.springframework.context.annotation.Import;
import java.util.*;
import java.time.LocalDateTime;

import static cn.hutool.core.util.RandomUtil.*;
import static cn.iocoder.yudao.module.shopee.enums.ErrorCodeConstants.*;
import static cn.iocoder.yudao.framework.test.core.util.AssertUtils.*;
import static cn.iocoder.yudao.framework.test.core.util.RandomUtils.*;
import static cn.iocoder.yudao.framework.common.util.date.LocalDateTimeUtils.*;
import static cn.iocoder.yudao.framework.common.util.object.ObjectUtils.*;
import static cn.iocoder.yudao.framework.common.util.date.DateUtils.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

/**
 * {@link ShopServiceImpl} 的单元测试类
 *
 * @author 芋道源码
 */
@Import(ShopServiceImpl.class)
public class ShopServiceImplTest extends BaseDbUnitTest {

    @Resource
    private ShopServiceImpl shopService;

    @Resource
    private ShopMapper shopMapper;

    @Test
    public void testCreateShop_success() {
        // 准备参数
        ShopCreateReqVO reqVO = randomPojo(ShopCreateReqVO.class);

        // 调用
        Long shopId = shopService.createShop(reqVO);
        // 断言
        assertNotNull(shopId);
        // 校验记录的属性是否正确
        ShopDO shop = shopMapper.selectById(shopId);
        assertPojoEquals(reqVO, shop);
    }

    @Test
    public void testUpdateShop_success() {
        // mock 数据
        ShopDO dbShop = randomPojo(ShopDO.class);
        shopMapper.insert(dbShop);// @Sql: 先插入出一条存在的数据
        // 准备参数
        ShopUpdateReqVO reqVO = randomPojo(ShopUpdateReqVO.class, o -> {
            o.setId(dbShop.getId()); // 设置更新的 ID
        });

        // 调用
        shopService.updateShop(reqVO);
        // 校验是否更新正确
        ShopDO shop = shopMapper.selectById(reqVO.getId()); // 获取最新的
        assertPojoEquals(reqVO, shop);
    }

    @Test
    public void testUpdateShop_notExists() {
        // 准备参数
        ShopUpdateReqVO reqVO = randomPojo(ShopUpdateReqVO.class);

        // 调用, 并断言异常
        assertServiceException(() -> shopService.updateShop(reqVO), SHOP_NOT_EXISTS);
    }

    @Test
    public void testDeleteShop_success() {
        // mock 数据
        ShopDO dbShop = randomPojo(ShopDO.class);
        shopMapper.insert(dbShop);// @Sql: 先插入出一条存在的数据
        // 准备参数
        Long id = dbShop.getId();

        // 调用
        shopService.deleteShop(id);
       // 校验数据不存在了
       assertNull(shopMapper.selectById(id));
    }

    @Test
    public void testDeleteShop_notExists() {
        // 准备参数
        Long id = randomLongId();

        // 调用, 并断言异常
        assertServiceException(() -> shopService.deleteShop(id), SHOP_NOT_EXISTS);
    }

    @Test
    @Disabled  // TODO 请修改 null 为需要的值，然后删除 @Disabled 注解
    public void testGetShopPage() {
       // mock 数据
       ShopDO dbShop = randomPojo(ShopDO.class, o -> { // 等会查询到
           o.setSysUserId(null);
           o.setAccountId(null);
           o.setUsername(null);
           o.setUserId(null);
           o.setRegion(null);
           o.setEnabled(null);
           o.setIsSipAffiliated(null);
           o.setShopName(null);
           o.setShopId(null);
           o.setLastLogin(null);
           o.setIsSipPrimary(null);
           o.setPortrait(null);
           o.setCbOption(null);
           o.setIsInactive(null);
           o.setIs3PfShop(null);
           o.setCreateTime(null);
       });
       shopMapper.insert(dbShop);
       // 测试 sysUserId 不匹配
       shopMapper.insert(cloneIgnoreId(dbShop, o -> o.setSysUserId(null)));
       // 测试 accountId 不匹配
       shopMapper.insert(cloneIgnoreId(dbShop, o -> o.setAccountId(null)));
       // 测试 username 不匹配
       shopMapper.insert(cloneIgnoreId(dbShop, o -> o.setUsername(null)));
       // 测试 userId 不匹配
       shopMapper.insert(cloneIgnoreId(dbShop, o -> o.setUserId(null)));
       // 测试 region 不匹配
       shopMapper.insert(cloneIgnoreId(dbShop, o -> o.setRegion(null)));
       // 测试 enabled 不匹配
       shopMapper.insert(cloneIgnoreId(dbShop, o -> o.setEnabled(null)));
       // 测试 isSipAffiliated 不匹配
       shopMapper.insert(cloneIgnoreId(dbShop, o -> o.setIsSipAffiliated(null)));
       // 测试 shopName 不匹配
       shopMapper.insert(cloneIgnoreId(dbShop, o -> o.setShopName(null)));
       // 测试 shopId 不匹配
       shopMapper.insert(cloneIgnoreId(dbShop, o -> o.setShopId(null)));
       // 测试 lastLogin 不匹配
       shopMapper.insert(cloneIgnoreId(dbShop, o -> o.setLastLogin(null)));
       // 测试 isSipPrimary 不匹配
       shopMapper.insert(cloneIgnoreId(dbShop, o -> o.setIsSipPrimary(null)));
       // 测试 portrait 不匹配
       shopMapper.insert(cloneIgnoreId(dbShop, o -> o.setPortrait(null)));
       // 测试 cbOption 不匹配
       shopMapper.insert(cloneIgnoreId(dbShop, o -> o.setCbOption(null)));
       // 测试 isInactive 不匹配
       shopMapper.insert(cloneIgnoreId(dbShop, o -> o.setIsInactive(null)));
       // 测试 is3PfShop 不匹配
       shopMapper.insert(cloneIgnoreId(dbShop, o -> o.setIs3PfShop(null)));
       // 测试 createTime 不匹配
       shopMapper.insert(cloneIgnoreId(dbShop, o -> o.setCreateTime(null)));
       // 准备参数
       ShopPageReqVO reqVO = new ShopPageReqVO();
       reqVO.setSysUserId(null);
       reqVO.setAccountId(null);
       reqVO.setUsername(null);
       reqVO.setUserId(null);
       reqVO.setRegion(null);
       reqVO.setEnabled(null);
       reqVO.setIsSipAffiliated(null);
       reqVO.setShopName(null);
       reqVO.setShopId(null);
       reqVO.setLastLogin(null);
       reqVO.setIsSipPrimary(null);
       reqVO.setPortrait(null);
       reqVO.setCbOption(null);
       reqVO.setIsInactive(null);
       reqVO.setIs3PfShop(null);
       reqVO.setCreateTime(buildBetweenTime(2023, 2, 1, 2023, 2, 28));

       // 调用
       PageResult<ShopDO> pageResult = shopService.getShopPage(reqVO);
       // 断言
       assertEquals(1, pageResult.getTotal());
       assertEquals(1, pageResult.getList().size());
       assertPojoEquals(dbShop, pageResult.getList().get(0));
    }

    @Test
    @Disabled  // TODO 请修改 null 为需要的值，然后删除 @Disabled 注解
    public void testGetShopList() {
       // mock 数据
       ShopDO dbShop = randomPojo(ShopDO.class, o -> { // 等会查询到
           o.setSysUserId(null);
           o.setAccountId(null);
           o.setUsername(null);
           o.setUserId(null);
           o.setRegion(null);
           o.setEnabled(null);
           o.setIsSipAffiliated(null);
           o.setShopName(null);
           o.setShopId(null);
           o.setLastLogin(null);
           o.setIsSipPrimary(null);
           o.setPortrait(null);
           o.setCbOption(null);
           o.setIsInactive(null);
           o.setIs3PfShop(null);
           o.setCreateTime(null);
       });
       shopMapper.insert(dbShop);
       // 测试 sysUserId 不匹配
       shopMapper.insert(cloneIgnoreId(dbShop, o -> o.setSysUserId(null)));
       // 测试 accountId 不匹配
       shopMapper.insert(cloneIgnoreId(dbShop, o -> o.setAccountId(null)));
       // 测试 username 不匹配
       shopMapper.insert(cloneIgnoreId(dbShop, o -> o.setUsername(null)));
       // 测试 userId 不匹配
       shopMapper.insert(cloneIgnoreId(dbShop, o -> o.setUserId(null)));
       // 测试 region 不匹配
       shopMapper.insert(cloneIgnoreId(dbShop, o -> o.setRegion(null)));
       // 测试 enabled 不匹配
       shopMapper.insert(cloneIgnoreId(dbShop, o -> o.setEnabled(null)));
       // 测试 isSipAffiliated 不匹配
       shopMapper.insert(cloneIgnoreId(dbShop, o -> o.setIsSipAffiliated(null)));
       // 测试 shopName 不匹配
       shopMapper.insert(cloneIgnoreId(dbShop, o -> o.setShopName(null)));
       // 测试 shopId 不匹配
       shopMapper.insert(cloneIgnoreId(dbShop, o -> o.setShopId(null)));
       // 测试 lastLogin 不匹配
       shopMapper.insert(cloneIgnoreId(dbShop, o -> o.setLastLogin(null)));
       // 测试 isSipPrimary 不匹配
       shopMapper.insert(cloneIgnoreId(dbShop, o -> o.setIsSipPrimary(null)));
       // 测试 portrait 不匹配
       shopMapper.insert(cloneIgnoreId(dbShop, o -> o.setPortrait(null)));
       // 测试 cbOption 不匹配
       shopMapper.insert(cloneIgnoreId(dbShop, o -> o.setCbOption(null)));
       // 测试 isInactive 不匹配
       shopMapper.insert(cloneIgnoreId(dbShop, o -> o.setIsInactive(null)));
       // 测试 is3PfShop 不匹配
       shopMapper.insert(cloneIgnoreId(dbShop, o -> o.setIs3PfShop(null)));
       // 测试 createTime 不匹配
       shopMapper.insert(cloneIgnoreId(dbShop, o -> o.setCreateTime(null)));
       // 准备参数
       ShopExportReqVO reqVO = new ShopExportReqVO();
       reqVO.setSysUserId(null);
       reqVO.setAccountId(null);
       reqVO.setUsername(null);
       reqVO.setUserId(null);
       reqVO.setRegion(null);
       reqVO.setEnabled(null);
       reqVO.setIsSipAffiliated(null);
       reqVO.setShopName(null);
       reqVO.setShopId(null);
       reqVO.setLastLogin(null);
       reqVO.setIsSipPrimary(null);
       reqVO.setPortrait(null);
       reqVO.setCbOption(null);
       reqVO.setIsInactive(null);
       reqVO.setIs3PfShop(null);
       reqVO.setCreateTime(buildBetweenTime(2023, 2, 1, 2023, 2, 28));

       // 调用
       List<ShopDO> list = shopService.getShopList(reqVO);
       // 断言
       assertEquals(1, list.size());
       assertPojoEquals(dbShop, list.get(0));
    }

}
