package cn.iocoder.yudao.module.shopee.service.accounthistory;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.mock.mockito.MockBean;

import javax.annotation.Resource;

import cn.iocoder.yudao.framework.test.core.ut.BaseDbUnitTest;

import cn.iocoder.yudao.module.shopee.controller.admin.accounthistory.vo.*;
import cn.iocoder.yudao.module.shopee.dal.dataobject.accounthistory.AccountHistoryDO;
import cn.iocoder.yudao.module.shopee.dal.mysql.accounthistory.AccountHistoryMapper;
import cn.iocoder.yudao.framework.common.pojo.PageResult;

import javax.annotation.Resource;
import org.springframework.context.annotation.Import;
import java.util.*;
import java.time.LocalDateTime;

import static cn.hutool.core.util.RandomUtil.*;
import static cn.iocoder.yudao.module.shopee.enums.ErrorCodeConstants.*;
import static cn.iocoder.yudao.framework.test.core.util.AssertUtils.*;
import static cn.iocoder.yudao.framework.test.core.util.RandomUtils.*;
import static cn.iocoder.yudao.framework.common.util.date.LocalDateTimeUtils.*;
import static cn.iocoder.yudao.framework.common.util.object.ObjectUtils.*;
import static cn.iocoder.yudao.framework.common.util.date.DateUtils.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

/**
 * {@link AccountHistoryServiceImpl} 的单元测试类
 *
 * @author 芋道源码
 */
@Import(AccountHistoryServiceImpl.class)
public class AccountHistoryServiceImplTest extends BaseDbUnitTest {

    @Resource
    private AccountHistoryServiceImpl accountHistoryService;

    @Resource
    private AccountHistoryMapper accountHistoryMapper;

    @Test
    public void testCreateAccountHistory_success() {
        // 准备参数
        AccountHistoryCreateReqVO reqVO = randomPojo(AccountHistoryCreateReqVO.class);

        // 调用
        Long accountHistoryId = accountHistoryService.createAccountHistory(reqVO);
        // 断言
        assertNotNull(accountHistoryId);
        // 校验记录的属性是否正确
        AccountHistoryDO accountHistory = accountHistoryMapper.selectById(accountHistoryId);
        assertPojoEquals(reqVO, accountHistory);
    }

    @Test
    public void testUpdateAccountHistory_success() {
        // mock 数据
        AccountHistoryDO dbAccountHistory = randomPojo(AccountHistoryDO.class);
        accountHistoryMapper.insert(dbAccountHistory);// @Sql: 先插入出一条存在的数据
        // 准备参数
        AccountHistoryUpdateReqVO reqVO = randomPojo(AccountHistoryUpdateReqVO.class, o -> {
            o.setId(dbAccountHistory.getId()); // 设置更新的 ID
        });

        // 调用
        accountHistoryService.updateAccountHistory(reqVO);
        // 校验是否更新正确
        AccountHistoryDO accountHistory = accountHistoryMapper.selectById(reqVO.getId()); // 获取最新的
        assertPojoEquals(reqVO, accountHistory);
    }

    @Test
    public void testUpdateAccountHistory_notExists() {
        // 准备参数
        AccountHistoryUpdateReqVO reqVO = randomPojo(AccountHistoryUpdateReqVO.class);

        // 调用, 并断言异常
        assertServiceException(() -> accountHistoryService.updateAccountHistory(reqVO), ACCOUNT_HISTORY_NOT_EXISTS);
    }

    @Test
    public void testDeleteAccountHistory_success() {
        // mock 数据
        AccountHistoryDO dbAccountHistory = randomPojo(AccountHistoryDO.class);
        accountHistoryMapper.insert(dbAccountHistory);// @Sql: 先插入出一条存在的数据
        // 准备参数
        Long id = dbAccountHistory.getId();

        // 调用
        accountHistoryService.deleteAccountHistory(id);
       // 校验数据不存在了
       assertNull(accountHistoryMapper.selectById(id));
    }

    @Test
    public void testDeleteAccountHistory_notExists() {
        // 准备参数
        Long id = randomLongId();

        // 调用, 并断言异常
        assertServiceException(() -> accountHistoryService.deleteAccountHistory(id), ACCOUNT_HISTORY_NOT_EXISTS);
    }

    @Test
    @Disabled  // TODO 请修改 null 为需要的值，然后删除 @Disabled 注解
    public void testGetAccountHistoryPage() {
       // mock 数据
       AccountHistoryDO dbAccountHistory = randomPojo(AccountHistoryDO.class, o -> { // 等会查询到
           o.setSysUserId(null);
           o.setAccountId(null);
           o.setAccountType(null);
           o.setTobAccountId(null);
           o.setPhone(null);
           o.setPortrait(null);
           o.setAccountName(null);
           o.setIsOverdue(null);
           o.setLanguage(null);
           o.setNickName(null);
           o.setMainAccountId(null);
           o.setSubAccountId(null);
           o.setEmail(null);
           o.setMerchantName(null);
           o.setCreateTime(null);
       });
       accountHistoryMapper.insert(dbAccountHistory);
       // 测试 sysUserId 不匹配
       accountHistoryMapper.insert(cloneIgnoreId(dbAccountHistory, o -> o.setSysUserId(null)));
       // 测试 accountId 不匹配
       accountHistoryMapper.insert(cloneIgnoreId(dbAccountHistory, o -> o.setAccountId(null)));
       // 测试 accountType 不匹配
       accountHistoryMapper.insert(cloneIgnoreId(dbAccountHistory, o -> o.setAccountType(null)));
       // 测试 tobAccountId 不匹配
       accountHistoryMapper.insert(cloneIgnoreId(dbAccountHistory, o -> o.setTobAccountId(null)));
       // 测试 phone 不匹配
       accountHistoryMapper.insert(cloneIgnoreId(dbAccountHistory, o -> o.setPhone(null)));
       // 测试 portrait 不匹配
       accountHistoryMapper.insert(cloneIgnoreId(dbAccountHistory, o -> o.setPortrait(null)));
       // 测试 accountName 不匹配
       accountHistoryMapper.insert(cloneIgnoreId(dbAccountHistory, o -> o.setAccountName(null)));
       // 测试 isOverdue 不匹配
       accountHistoryMapper.insert(cloneIgnoreId(dbAccountHistory, o -> o.setIsOverdue(null)));
       // 测试 language 不匹配
       accountHistoryMapper.insert(cloneIgnoreId(dbAccountHistory, o -> o.setLanguage(null)));
       // 测试 nickName 不匹配
       accountHistoryMapper.insert(cloneIgnoreId(dbAccountHistory, o -> o.setNickName(null)));
       // 测试 mainAccountId 不匹配
       accountHistoryMapper.insert(cloneIgnoreId(dbAccountHistory, o -> o.setMainAccountId(null)));
       // 测试 subAccountId 不匹配
       accountHistoryMapper.insert(cloneIgnoreId(dbAccountHistory, o -> o.setSubAccountId(null)));
       // 测试 email 不匹配
       accountHistoryMapper.insert(cloneIgnoreId(dbAccountHistory, o -> o.setEmail(null)));
       // 测试 merchantName 不匹配
       accountHistoryMapper.insert(cloneIgnoreId(dbAccountHistory, o -> o.setMerchantName(null)));
       // 测试 createTime 不匹配
       accountHistoryMapper.insert(cloneIgnoreId(dbAccountHistory, o -> o.setCreateTime(null)));
       // 准备参数
       AccountHistoryPageReqVO reqVO = new AccountHistoryPageReqVO();
       reqVO.setSysUserId(null);
       reqVO.setAccountId(null);
       reqVO.setAccountType(null);
       reqVO.setTobAccountId(null);
       reqVO.setPhone(null);
       reqVO.setPortrait(null);
       reqVO.setAccountName(null);
       reqVO.setIsOverdue(null);
       reqVO.setLanguage(null);
       reqVO.setNickName(null);
       reqVO.setMainAccountId(null);
       reqVO.setSubAccountId(null);
       reqVO.setEmail(null);
       reqVO.setMerchantName(null);
       reqVO.setCreateTime(buildBetweenTime(2023, 2, 1, 2023, 2, 28));

       // 调用
       PageResult<AccountHistoryDO> pageResult = accountHistoryService.getAccountHistoryPage(reqVO);
       // 断言
       assertEquals(1, pageResult.getTotal());
       assertEquals(1, pageResult.getList().size());
       assertPojoEquals(dbAccountHistory, pageResult.getList().get(0));
    }

    @Test
    @Disabled  // TODO 请修改 null 为需要的值，然后删除 @Disabled 注解
    public void testGetAccountHistoryList() {
       // mock 数据
       AccountHistoryDO dbAccountHistory = randomPojo(AccountHistoryDO.class, o -> { // 等会查询到
           o.setSysUserId(null);
           o.setAccountId(null);
           o.setAccountType(null);
           o.setTobAccountId(null);
           o.setPhone(null);
           o.setPortrait(null);
           o.setAccountName(null);
           o.setIsOverdue(null);
           o.setLanguage(null);
           o.setNickName(null);
           o.setMainAccountId(null);
           o.setSubAccountId(null);
           o.setEmail(null);
           o.setMerchantName(null);
           o.setCreateTime(null);
       });
       accountHistoryMapper.insert(dbAccountHistory);
       // 测试 sysUserId 不匹配
       accountHistoryMapper.insert(cloneIgnoreId(dbAccountHistory, o -> o.setSysUserId(null)));
       // 测试 accountId 不匹配
       accountHistoryMapper.insert(cloneIgnoreId(dbAccountHistory, o -> o.setAccountId(null)));
       // 测试 accountType 不匹配
       accountHistoryMapper.insert(cloneIgnoreId(dbAccountHistory, o -> o.setAccountType(null)));
       // 测试 tobAccountId 不匹配
       accountHistoryMapper.insert(cloneIgnoreId(dbAccountHistory, o -> o.setTobAccountId(null)));
       // 测试 phone 不匹配
       accountHistoryMapper.insert(cloneIgnoreId(dbAccountHistory, o -> o.setPhone(null)));
       // 测试 portrait 不匹配
       accountHistoryMapper.insert(cloneIgnoreId(dbAccountHistory, o -> o.setPortrait(null)));
       // 测试 accountName 不匹配
       accountHistoryMapper.insert(cloneIgnoreId(dbAccountHistory, o -> o.setAccountName(null)));
       // 测试 isOverdue 不匹配
       accountHistoryMapper.insert(cloneIgnoreId(dbAccountHistory, o -> o.setIsOverdue(null)));
       // 测试 language 不匹配
       accountHistoryMapper.insert(cloneIgnoreId(dbAccountHistory, o -> o.setLanguage(null)));
       // 测试 nickName 不匹配
       accountHistoryMapper.insert(cloneIgnoreId(dbAccountHistory, o -> o.setNickName(null)));
       // 测试 mainAccountId 不匹配
       accountHistoryMapper.insert(cloneIgnoreId(dbAccountHistory, o -> o.setMainAccountId(null)));
       // 测试 subAccountId 不匹配
       accountHistoryMapper.insert(cloneIgnoreId(dbAccountHistory, o -> o.setSubAccountId(null)));
       // 测试 email 不匹配
       accountHistoryMapper.insert(cloneIgnoreId(dbAccountHistory, o -> o.setEmail(null)));
       // 测试 merchantName 不匹配
       accountHistoryMapper.insert(cloneIgnoreId(dbAccountHistory, o -> o.setMerchantName(null)));
       // 测试 createTime 不匹配
       accountHistoryMapper.insert(cloneIgnoreId(dbAccountHistory, o -> o.setCreateTime(null)));
       // 准备参数
       AccountHistoryExportReqVO reqVO = new AccountHistoryExportReqVO();
       reqVO.setSysUserId(null);
       reqVO.setAccountId(null);
       reqVO.setAccountType(null);
       reqVO.setTobAccountId(null);
       reqVO.setPhone(null);
       reqVO.setPortrait(null);
       reqVO.setAccountName(null);
       reqVO.setIsOverdue(null);
       reqVO.setLanguage(null);
       reqVO.setNickName(null);
       reqVO.setMainAccountId(null);
       reqVO.setSubAccountId(null);
       reqVO.setEmail(null);
       reqVO.setMerchantName(null);
       reqVO.setCreateTime(buildBetweenTime(2023, 2, 1, 2023, 2, 28));

       // 调用
       List<AccountHistoryDO> list = accountHistoryService.getAccountHistoryList(reqVO);
       // 断言
       assertEquals(1, list.size());
       assertPojoEquals(dbAccountHistory, list.get(0));
    }

}
