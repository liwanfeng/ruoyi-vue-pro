package cn.iocoder.yudao.module.shopee.service.producthistory;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.mock.mockito.MockBean;

import javax.annotation.Resource;

import cn.iocoder.yudao.framework.test.core.ut.BaseDbUnitTest;

import cn.iocoder.yudao.module.shopee.controller.admin.producthistory.vo.*;
import cn.iocoder.yudao.module.shopee.dal.dataobject.producthistory.ProductHistoryDO;
import cn.iocoder.yudao.module.shopee.dal.mysql.producthistory.ProductHistoryMapper;
import cn.iocoder.yudao.framework.common.pojo.PageResult;

import javax.annotation.Resource;
import org.springframework.context.annotation.Import;
import java.util.*;
import java.time.LocalDateTime;

import static cn.hutool.core.util.RandomUtil.*;
import static cn.iocoder.yudao.module.shopee.enums.ErrorCodeConstants.*;
import static cn.iocoder.yudao.framework.test.core.util.AssertUtils.*;
import static cn.iocoder.yudao.framework.test.core.util.RandomUtils.*;
import static cn.iocoder.yudao.framework.common.util.date.LocalDateTimeUtils.*;
import static cn.iocoder.yudao.framework.common.util.object.ObjectUtils.*;
import static cn.iocoder.yudao.framework.common.util.date.DateUtils.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

/**
 * {@link ProductHistoryServiceImpl} 的单元测试类
 *
 * @author 芋道源码
 */
@Import(ProductHistoryServiceImpl.class)
public class ProductHistoryServiceImplTest extends BaseDbUnitTest {

    @Resource
    private ProductHistoryServiceImpl productHistoryService;

    @Resource
    private ProductHistoryMapper productHistoryMapper;

    @Test
    public void testCreateProductHistory_success() {
        // 准备参数
        ProductHistoryCreateReqVO reqVO = randomPojo(ProductHistoryCreateReqVO.class);

        // 调用
        Long productHistoryId = productHistoryService.createProductHistory(reqVO);
        // 断言
        assertNotNull(productHistoryId);
        // 校验记录的属性是否正确
        ProductHistoryDO productHistory = productHistoryMapper.selectById(productHistoryId);
        assertPojoEquals(reqVO, productHistory);
    }

    @Test
    public void testUpdateProductHistory_success() {
        // mock 数据
        ProductHistoryDO dbProductHistory = randomPojo(ProductHistoryDO.class);
        productHistoryMapper.insert(dbProductHistory);// @Sql: 先插入出一条存在的数据
        // 准备参数
        ProductHistoryUpdateReqVO reqVO = randomPojo(ProductHistoryUpdateReqVO.class, o -> {
            o.setId(dbProductHistory.getId()); // 设置更新的 ID
        });

        // 调用
        productHistoryService.updateProductHistory(reqVO);
        // 校验是否更新正确
        ProductHistoryDO productHistory = productHistoryMapper.selectById(reqVO.getId()); // 获取最新的
        assertPojoEquals(reqVO, productHistory);
    }

    @Test
    public void testUpdateProductHistory_notExists() {
        // 准备参数
        ProductHistoryUpdateReqVO reqVO = randomPojo(ProductHistoryUpdateReqVO.class);

        // 调用, 并断言异常
        assertServiceException(() -> productHistoryService.updateProductHistory(reqVO), PRODUCT_HISTORY_NOT_EXISTS);
    }

    @Test
    public void testDeleteProductHistory_success() {
        // mock 数据
        ProductHistoryDO dbProductHistory = randomPojo(ProductHistoryDO.class);
        productHistoryMapper.insert(dbProductHistory);// @Sql: 先插入出一条存在的数据
        // 准备参数
        Long id = dbProductHistory.getId();

        // 调用
        productHistoryService.deleteProductHistory(id);
       // 校验数据不存在了
       assertNull(productHistoryMapper.selectById(id));
    }

    @Test
    public void testDeleteProductHistory_notExists() {
        // 准备参数
        Long id = randomLongId();

        // 调用, 并断言异常
        assertServiceException(() -> productHistoryService.deleteProductHistory(id), PRODUCT_HISTORY_NOT_EXISTS);
    }

    @Test
    @Disabled  // TODO 请修改 null 为需要的值，然后删除 @Disabled 注解
    public void testGetProductHistoryPage() {
       // mock 数据
       ProductHistoryDO dbProductHistory = randomPojo(ProductHistoryDO.class, o -> { // 等会查询到
           o.setSysUserId(null);
           o.setAccountId(null);
           o.setShopId(null);
           o.setSkuId(null);
           o.setAddOnDeal(null);
           o.setBoostCoolDownSeconds(null);
           o.setDeboosted(null);
           o.setFlag(null);
           o.setInPromotion(null);
           o.setLikeCount(null);
           o.setName(null);
           o.setParentSku(null);
           o.setSold(null);
           o.setStatus(null);
           o.setSellerCurrentStock(null);
           o.setViewCount(null);
           o.setWholesale(null);
           o.setCategoryStatus(null);
           o.setDaysToShip(null);
           o.setPreOrder(null);
           o.setBrandId(null);
           o.setBrand(null);
           o.setModifyTime(null);
           o.setIsB2CSkuInFbsShop(null);
           o.setMtskuItemId(null);
           o.setCategory1(null);
           o.setCategory2(null);
           o.setCategory3(null);
           o.setOngoingUpcomingCampaigns(null);
           o.setCreateTime(null);
       });
       productHistoryMapper.insert(dbProductHistory);
       // 测试 sysUserId 不匹配
       productHistoryMapper.insert(cloneIgnoreId(dbProductHistory, o -> o.setSysUserId(null)));
       // 测试 accountId 不匹配
       productHistoryMapper.insert(cloneIgnoreId(dbProductHistory, o -> o.setAccountId(null)));
       // 测试 shopId 不匹配
       productHistoryMapper.insert(cloneIgnoreId(dbProductHistory, o -> o.setShopId(null)));
       // 测试 skuId 不匹配
       productHistoryMapper.insert(cloneIgnoreId(dbProductHistory, o -> o.setSkuId(null)));
       // 测试 addOnDeal 不匹配
       productHistoryMapper.insert(cloneIgnoreId(dbProductHistory, o -> o.setAddOnDeal(null)));
       // 测试 boostCoolDownSeconds 不匹配
       productHistoryMapper.insert(cloneIgnoreId(dbProductHistory, o -> o.setBoostCoolDownSeconds(null)));
       // 测试 deboosted 不匹配
       productHistoryMapper.insert(cloneIgnoreId(dbProductHistory, o -> o.setDeboosted(null)));
       // 测试 flag 不匹配
       productHistoryMapper.insert(cloneIgnoreId(dbProductHistory, o -> o.setFlag(null)));
       // 测试 inPromotion 不匹配
       productHistoryMapper.insert(cloneIgnoreId(dbProductHistory, o -> o.setInPromotion(null)));
       // 测试 likeCount 不匹配
       productHistoryMapper.insert(cloneIgnoreId(dbProductHistory, o -> o.setLikeCount(null)));
       // 测试 name 不匹配
       productHistoryMapper.insert(cloneIgnoreId(dbProductHistory, o -> o.setName(null)));
       // 测试 parentSku 不匹配
       productHistoryMapper.insert(cloneIgnoreId(dbProductHistory, o -> o.setParentSku(null)));
       // 测试 sold 不匹配
       productHistoryMapper.insert(cloneIgnoreId(dbProductHistory, o -> o.setSold(null)));
       // 测试 status 不匹配
       productHistoryMapper.insert(cloneIgnoreId(dbProductHistory, o -> o.setStatus(null)));
       // 测试 sellerCurrentStock 不匹配
       productHistoryMapper.insert(cloneIgnoreId(dbProductHistory, o -> o.setSellerCurrentStock(null)));
       // 测试 viewCount 不匹配
       productHistoryMapper.insert(cloneIgnoreId(dbProductHistory, o -> o.setViewCount(null)));
       // 测试 wholesale 不匹配
       productHistoryMapper.insert(cloneIgnoreId(dbProductHistory, o -> o.setWholesale(null)));
       // 测试 categoryStatus 不匹配
       productHistoryMapper.insert(cloneIgnoreId(dbProductHistory, o -> o.setCategoryStatus(null)));
       // 测试 daysToShip 不匹配
       productHistoryMapper.insert(cloneIgnoreId(dbProductHistory, o -> o.setDaysToShip(null)));
       // 测试 preOrder 不匹配
       productHistoryMapper.insert(cloneIgnoreId(dbProductHistory, o -> o.setPreOrder(null)));
       // 测试 brandId 不匹配
       productHistoryMapper.insert(cloneIgnoreId(dbProductHistory, o -> o.setBrandId(null)));
       // 测试 brand 不匹配
       productHistoryMapper.insert(cloneIgnoreId(dbProductHistory, o -> o.setBrand(null)));
       // 测试 modifyTime 不匹配
       productHistoryMapper.insert(cloneIgnoreId(dbProductHistory, o -> o.setModifyTime(null)));
       // 测试 isB2CSkuInFbsShop 不匹配
       productHistoryMapper.insert(cloneIgnoreId(dbProductHistory, o -> o.setIsB2CSkuInFbsShop(null)));
       // 测试 mtskuItemId 不匹配
       productHistoryMapper.insert(cloneIgnoreId(dbProductHistory, o -> o.setMtskuItemId(null)));
       // 测试 category1 不匹配
       productHistoryMapper.insert(cloneIgnoreId(dbProductHistory, o -> o.setCategory1(null)));
       // 测试 category2 不匹配
       productHistoryMapper.insert(cloneIgnoreId(dbProductHistory, o -> o.setCategory2(null)));
       // 测试 category3 不匹配
       productHistoryMapper.insert(cloneIgnoreId(dbProductHistory, o -> o.setCategory3(null)));
       // 测试 ongoingUpcomingCampaigns 不匹配
       productHistoryMapper.insert(cloneIgnoreId(dbProductHistory, o -> o.setOngoingUpcomingCampaigns(null)));
       // 测试 createTime 不匹配
       productHistoryMapper.insert(cloneIgnoreId(dbProductHistory, o -> o.setCreateTime(null)));
       // 准备参数
       ProductHistoryPageReqVO reqVO = new ProductHistoryPageReqVO();
       reqVO.setSysUserId(null);
       reqVO.setAccountId(null);
       reqVO.setShopId(null);
       reqVO.setSkuId(null);
       reqVO.setAddOnDeal(null);
       reqVO.setBoostCoolDownSeconds(null);
       reqVO.setDeboosted(null);
       reqVO.setFlag(null);
       reqVO.setInPromotion(null);
       reqVO.setLikeCount(null);
       reqVO.setName(null);
       reqVO.setParentSku(null);
       reqVO.setSold(null);
       reqVO.setStatus(null);
       reqVO.setSellerCurrentStock(null);
       reqVO.setViewCount(null);
       reqVO.setWholesale(null);
       reqVO.setCategoryStatus(null);
       reqVO.setDaysToShip(null);
       reqVO.setPreOrder(null);
       reqVO.setBrandId(null);
       reqVO.setBrand(null);
       reqVO.setIsB2CSkuInFbsShop(null);
       reqVO.setMtskuItemId(null);
       reqVO.setCategory1(null);
       reqVO.setCategory2(null);
       reqVO.setCategory3(null);
       reqVO.setOngoingUpcomingCampaigns(null);
       reqVO.setCreateTime(buildBetweenTime(2023, 2, 1, 2023, 2, 28));

       // 调用
       PageResult<ProductHistoryDO> pageResult = productHistoryService.getProductHistoryPage(reqVO);
       // 断言
       assertEquals(1, pageResult.getTotal());
       assertEquals(1, pageResult.getList().size());
       assertPojoEquals(dbProductHistory, pageResult.getList().get(0));
    }

    @Test
    @Disabled  // TODO 请修改 null 为需要的值，然后删除 @Disabled 注解
    public void testGetProductHistoryList() {
       // mock 数据
       ProductHistoryDO dbProductHistory = randomPojo(ProductHistoryDO.class, o -> { // 等会查询到
           o.setSysUserId(null);
           o.setAccountId(null);
           o.setShopId(null);
           o.setSkuId(null);
           o.setAddOnDeal(null);
           o.setBoostCoolDownSeconds(null);
           o.setDeboosted(null);
           o.setFlag(null);
           o.setInPromotion(null);
           o.setLikeCount(null);
           o.setName(null);
           o.setParentSku(null);
           o.setSold(null);
           o.setStatus(null);
           o.setSellerCurrentStock(null);
           o.setViewCount(null);
           o.setWholesale(null);
           o.setCategoryStatus(null);
           o.setDaysToShip(null);
           o.setPreOrder(null);
           o.setBrandId(null);
           o.setBrand(null);
           o.setModifyTime(null);
           o.setIsB2CSkuInFbsShop(null);
           o.setMtskuItemId(null);
           o.setCategory1(null);
           o.setCategory2(null);
           o.setCategory3(null);
           o.setOngoingUpcomingCampaigns(null);
           o.setCreateTime(null);
       });
       productHistoryMapper.insert(dbProductHistory);
       // 测试 sysUserId 不匹配
       productHistoryMapper.insert(cloneIgnoreId(dbProductHistory, o -> o.setSysUserId(null)));
       // 测试 accountId 不匹配
       productHistoryMapper.insert(cloneIgnoreId(dbProductHistory, o -> o.setAccountId(null)));
       // 测试 shopId 不匹配
       productHistoryMapper.insert(cloneIgnoreId(dbProductHistory, o -> o.setShopId(null)));
       // 测试 skuId 不匹配
       productHistoryMapper.insert(cloneIgnoreId(dbProductHistory, o -> o.setSkuId(null)));
       // 测试 addOnDeal 不匹配
       productHistoryMapper.insert(cloneIgnoreId(dbProductHistory, o -> o.setAddOnDeal(null)));
       // 测试 boostCoolDownSeconds 不匹配
       productHistoryMapper.insert(cloneIgnoreId(dbProductHistory, o -> o.setBoostCoolDownSeconds(null)));
       // 测试 deboosted 不匹配
       productHistoryMapper.insert(cloneIgnoreId(dbProductHistory, o -> o.setDeboosted(null)));
       // 测试 flag 不匹配
       productHistoryMapper.insert(cloneIgnoreId(dbProductHistory, o -> o.setFlag(null)));
       // 测试 inPromotion 不匹配
       productHistoryMapper.insert(cloneIgnoreId(dbProductHistory, o -> o.setInPromotion(null)));
       // 测试 likeCount 不匹配
       productHistoryMapper.insert(cloneIgnoreId(dbProductHistory, o -> o.setLikeCount(null)));
       // 测试 name 不匹配
       productHistoryMapper.insert(cloneIgnoreId(dbProductHistory, o -> o.setName(null)));
       // 测试 parentSku 不匹配
       productHistoryMapper.insert(cloneIgnoreId(dbProductHistory, o -> o.setParentSku(null)));
       // 测试 sold 不匹配
       productHistoryMapper.insert(cloneIgnoreId(dbProductHistory, o -> o.setSold(null)));
       // 测试 status 不匹配
       productHistoryMapper.insert(cloneIgnoreId(dbProductHistory, o -> o.setStatus(null)));
       // 测试 sellerCurrentStock 不匹配
       productHistoryMapper.insert(cloneIgnoreId(dbProductHistory, o -> o.setSellerCurrentStock(null)));
       // 测试 viewCount 不匹配
       productHistoryMapper.insert(cloneIgnoreId(dbProductHistory, o -> o.setViewCount(null)));
       // 测试 wholesale 不匹配
       productHistoryMapper.insert(cloneIgnoreId(dbProductHistory, o -> o.setWholesale(null)));
       // 测试 categoryStatus 不匹配
       productHistoryMapper.insert(cloneIgnoreId(dbProductHistory, o -> o.setCategoryStatus(null)));
       // 测试 daysToShip 不匹配
       productHistoryMapper.insert(cloneIgnoreId(dbProductHistory, o -> o.setDaysToShip(null)));
       // 测试 preOrder 不匹配
       productHistoryMapper.insert(cloneIgnoreId(dbProductHistory, o -> o.setPreOrder(null)));
       // 测试 brandId 不匹配
       productHistoryMapper.insert(cloneIgnoreId(dbProductHistory, o -> o.setBrandId(null)));
       // 测试 brand 不匹配
       productHistoryMapper.insert(cloneIgnoreId(dbProductHistory, o -> o.setBrand(null)));
       // 测试 modifyTime 不匹配
       productHistoryMapper.insert(cloneIgnoreId(dbProductHistory, o -> o.setModifyTime(null)));
       // 测试 isB2CSkuInFbsShop 不匹配
       productHistoryMapper.insert(cloneIgnoreId(dbProductHistory, o -> o.setIsB2CSkuInFbsShop(null)));
       // 测试 mtskuItemId 不匹配
       productHistoryMapper.insert(cloneIgnoreId(dbProductHistory, o -> o.setMtskuItemId(null)));
       // 测试 category1 不匹配
       productHistoryMapper.insert(cloneIgnoreId(dbProductHistory, o -> o.setCategory1(null)));
       // 测试 category2 不匹配
       productHistoryMapper.insert(cloneIgnoreId(dbProductHistory, o -> o.setCategory2(null)));
       // 测试 category3 不匹配
       productHistoryMapper.insert(cloneIgnoreId(dbProductHistory, o -> o.setCategory3(null)));
       // 测试 ongoingUpcomingCampaigns 不匹配
       productHistoryMapper.insert(cloneIgnoreId(dbProductHistory, o -> o.setOngoingUpcomingCampaigns(null)));
       // 测试 createTime 不匹配
       productHistoryMapper.insert(cloneIgnoreId(dbProductHistory, o -> o.setCreateTime(null)));
       // 准备参数
       ProductHistoryExportReqVO reqVO = new ProductHistoryExportReqVO();
       reqVO.setSysUserId(null);
       reqVO.setAccountId(null);
       reqVO.setShopId(null);
       reqVO.setSkuId(null);
       reqVO.setAddOnDeal(null);
       reqVO.setBoostCoolDownSeconds(null);
       reqVO.setDeboosted(null);
       reqVO.setFlag(null);
       reqVO.setInPromotion(null);
       reqVO.setLikeCount(null);
       reqVO.setName(null);
       reqVO.setParentSku(null);
       reqVO.setSold(null);
       reqVO.setStatus(null);
       reqVO.setSellerCurrentStock(null);
       reqVO.setViewCount(null);
       reqVO.setWholesale(null);
       reqVO.setCategoryStatus(null);
       reqVO.setDaysToShip(null);
       reqVO.setPreOrder(null);
       reqVO.setBrandId(null);
       reqVO.setBrand(null);
       reqVO.setIsB2CSkuInFbsShop(null);
       reqVO.setMtskuItemId(null);
       reqVO.setCategory1(null);
       reqVO.setCategory2(null);
       reqVO.setCategory3(null);
       reqVO.setOngoingUpcomingCampaigns(null);
       reqVO.setCreateTime(buildBetweenTime(2023, 2, 1, 2023, 2, 28));

       // 调用
       List<ProductHistoryDO> list = productHistoryService.getProductHistoryList(reqVO);
       // 断言
       assertEquals(1, list.size());
       assertPojoEquals(dbProductHistory, list.get(0));
    }

}
