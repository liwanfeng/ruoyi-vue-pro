package cn.iocoder.yudao.module.shopee.service.account;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.mock.mockito.MockBean;

import javax.annotation.Resource;

import cn.iocoder.yudao.framework.test.core.ut.BaseDbUnitTest;

import cn.iocoder.yudao.module.shopee.controller.admin.account.vo.*;
import cn.iocoder.yudao.module.shopee.dal.dataobject.account.AccountDO;
import cn.iocoder.yudao.module.shopee.dal.mysql.account.AccountMapper;
import cn.iocoder.yudao.framework.common.pojo.PageResult;

import javax.annotation.Resource;
import org.springframework.context.annotation.Import;
import java.util.*;
import java.time.LocalDateTime;

import static cn.hutool.core.util.RandomUtil.*;
import static cn.iocoder.yudao.module.shopee.enums.ErrorCodeConstants.*;
import static cn.iocoder.yudao.framework.test.core.util.AssertUtils.*;
import static cn.iocoder.yudao.framework.test.core.util.RandomUtils.*;
import static cn.iocoder.yudao.framework.common.util.date.LocalDateTimeUtils.*;
import static cn.iocoder.yudao.framework.common.util.object.ObjectUtils.*;
import static cn.iocoder.yudao.framework.common.util.date.DateUtils.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

/**
 * {@link AccountServiceImpl} 的单元测试类
 *
 * @author 芋道源码
 */
@Import(AccountServiceImpl.class)
public class AccountServiceImplTest extends BaseDbUnitTest {

    @Resource
    private AccountServiceImpl accountService;

    @Resource
    private AccountMapper accountMapper;

    @Test
    public void testCreateAccount_success() {
        // 准备参数
        AccountCreateReqVO reqVO = randomPojo(AccountCreateReqVO.class);

        // 调用
        Long accountId = accountService.createAccount(reqVO);
        // 断言
        assertNotNull(accountId);
        // 校验记录的属性是否正确
        AccountDO account = accountMapper.selectById(accountId);
        assertPojoEquals(reqVO, account);
    }

    @Test
    public void testUpdateAccount_success() {
        // mock 数据
        AccountDO dbAccount = randomPojo(AccountDO.class);
        accountMapper.insert(dbAccount);// @Sql: 先插入出一条存在的数据
        // 准备参数
        AccountUpdateReqVO reqVO = randomPojo(AccountUpdateReqVO.class, o -> {
            o.setId(dbAccount.getId()); // 设置更新的 ID
        });

        // 调用
        accountService.updateAccount(reqVO);
        // 校验是否更新正确
        AccountDO account = accountMapper.selectById(reqVO.getId()); // 获取最新的
        assertPojoEquals(reqVO, account);
    }

    @Test
    public void testUpdateAccount_notExists() {
        // 准备参数
        AccountUpdateReqVO reqVO = randomPojo(AccountUpdateReqVO.class);

        // 调用, 并断言异常
        assertServiceException(() -> accountService.updateAccount(reqVO), ACCOUNT_NOT_EXISTS);
    }

    @Test
    public void testDeleteAccount_success() {
        // mock 数据
        AccountDO dbAccount = randomPojo(AccountDO.class);
        accountMapper.insert(dbAccount);// @Sql: 先插入出一条存在的数据
        // 准备参数
        Long id = dbAccount.getId();

        // 调用
        accountService.deleteAccount(id);
       // 校验数据不存在了
       assertNull(accountMapper.selectById(id));
    }

    @Test
    public void testDeleteAccount_notExists() {
        // 准备参数
        Long id = randomLongId();

        // 调用, 并断言异常
        assertServiceException(() -> accountService.deleteAccount(id), ACCOUNT_NOT_EXISTS);
    }

    @Test
    @Disabled  // TODO 请修改 null 为需要的值，然后删除 @Disabled 注解
    public void testGetAccountPage() {
       // mock 数据
       AccountDO dbAccount = randomPojo(AccountDO.class, o -> { // 等会查询到
           o.setSysUserId(null);
           o.setAccountId(null);
           o.setAccountType(null);
           o.setTobAccountId(null);
           o.setPhone(null);
           o.setPortrait(null);
           o.setAccountName(null);
           o.setIsOverdue(null);
           o.setLanguage(null);
           o.setNickName(null);
           o.setMainAccountId(null);
           o.setSubAccountId(null);
           o.setEmail(null);
           o.setMerchantName(null);
           o.setCreateTime(null);
       });
       accountMapper.insert(dbAccount);
       // 测试 sysUserId 不匹配
       accountMapper.insert(cloneIgnoreId(dbAccount, o -> o.setSysUserId(null)));
       // 测试 accountId 不匹配
       accountMapper.insert(cloneIgnoreId(dbAccount, o -> o.setAccountId(null)));
       // 测试 accountType 不匹配
       accountMapper.insert(cloneIgnoreId(dbAccount, o -> o.setAccountType(null)));
       // 测试 tobAccountId 不匹配
       accountMapper.insert(cloneIgnoreId(dbAccount, o -> o.setTobAccountId(null)));
       // 测试 phone 不匹配
       accountMapper.insert(cloneIgnoreId(dbAccount, o -> o.setPhone(null)));
       // 测试 portrait 不匹配
       accountMapper.insert(cloneIgnoreId(dbAccount, o -> o.setPortrait(null)));
       // 测试 accountName 不匹配
       accountMapper.insert(cloneIgnoreId(dbAccount, o -> o.setAccountName(null)));
       // 测试 isOverdue 不匹配
       accountMapper.insert(cloneIgnoreId(dbAccount, o -> o.setIsOverdue(null)));
       // 测试 language 不匹配
       accountMapper.insert(cloneIgnoreId(dbAccount, o -> o.setLanguage(null)));
       // 测试 nickName 不匹配
       accountMapper.insert(cloneIgnoreId(dbAccount, o -> o.setNickName(null)));
       // 测试 mainAccountId 不匹配
       accountMapper.insert(cloneIgnoreId(dbAccount, o -> o.setMainAccountId(null)));
       // 测试 subAccountId 不匹配
       accountMapper.insert(cloneIgnoreId(dbAccount, o -> o.setSubAccountId(null)));
       // 测试 email 不匹配
       accountMapper.insert(cloneIgnoreId(dbAccount, o -> o.setEmail(null)));
       // 测试 merchantName 不匹配
       accountMapper.insert(cloneIgnoreId(dbAccount, o -> o.setMerchantName(null)));
       // 测试 createTime 不匹配
       accountMapper.insert(cloneIgnoreId(dbAccount, o -> o.setCreateTime(null)));
       // 准备参数
       AccountPageReqVO reqVO = new AccountPageReqVO();
       reqVO.setSysUserId(null);
       reqVO.setAccountId(null);
       reqVO.setAccountType(null);
       reqVO.setTobAccountId(null);
       reqVO.setPhone(null);
       reqVO.setPortrait(null);
       reqVO.setAccountName(null);
       reqVO.setIsOverdue(null);
       reqVO.setLanguage(null);
       reqVO.setNickName(null);
       reqVO.setMainAccountId(null);
       reqVO.setSubAccountId(null);
       reqVO.setEmail(null);
       reqVO.setMerchantName(null);
       reqVO.setCreateTime(buildBetweenTime(2023, 2, 1, 2023, 2, 28));

       // 调用
       PageResult<AccountDO> pageResult = accountService.getAccountPage(reqVO);
       // 断言
       assertEquals(1, pageResult.getTotal());
       assertEquals(1, pageResult.getList().size());
       assertPojoEquals(dbAccount, pageResult.getList().get(0));
    }

    @Test
    @Disabled  // TODO 请修改 null 为需要的值，然后删除 @Disabled 注解
    public void testGetAccountList() {
       // mock 数据
       AccountDO dbAccount = randomPojo(AccountDO.class, o -> { // 等会查询到
           o.setSysUserId(null);
           o.setAccountId(null);
           o.setAccountType(null);
           o.setTobAccountId(null);
           o.setPhone(null);
           o.setPortrait(null);
           o.setAccountName(null);
           o.setIsOverdue(null);
           o.setLanguage(null);
           o.setNickName(null);
           o.setMainAccountId(null);
           o.setSubAccountId(null);
           o.setEmail(null);
           o.setMerchantName(null);
           o.setCreateTime(null);
       });
       accountMapper.insert(dbAccount);
       // 测试 sysUserId 不匹配
       accountMapper.insert(cloneIgnoreId(dbAccount, o -> o.setSysUserId(null)));
       // 测试 accountId 不匹配
       accountMapper.insert(cloneIgnoreId(dbAccount, o -> o.setAccountId(null)));
       // 测试 accountType 不匹配
       accountMapper.insert(cloneIgnoreId(dbAccount, o -> o.setAccountType(null)));
       // 测试 tobAccountId 不匹配
       accountMapper.insert(cloneIgnoreId(dbAccount, o -> o.setTobAccountId(null)));
       // 测试 phone 不匹配
       accountMapper.insert(cloneIgnoreId(dbAccount, o -> o.setPhone(null)));
       // 测试 portrait 不匹配
       accountMapper.insert(cloneIgnoreId(dbAccount, o -> o.setPortrait(null)));
       // 测试 accountName 不匹配
       accountMapper.insert(cloneIgnoreId(dbAccount, o -> o.setAccountName(null)));
       // 测试 isOverdue 不匹配
       accountMapper.insert(cloneIgnoreId(dbAccount, o -> o.setIsOverdue(null)));
       // 测试 language 不匹配
       accountMapper.insert(cloneIgnoreId(dbAccount, o -> o.setLanguage(null)));
       // 测试 nickName 不匹配
       accountMapper.insert(cloneIgnoreId(dbAccount, o -> o.setNickName(null)));
       // 测试 mainAccountId 不匹配
       accountMapper.insert(cloneIgnoreId(dbAccount, o -> o.setMainAccountId(null)));
       // 测试 subAccountId 不匹配
       accountMapper.insert(cloneIgnoreId(dbAccount, o -> o.setSubAccountId(null)));
       // 测试 email 不匹配
       accountMapper.insert(cloneIgnoreId(dbAccount, o -> o.setEmail(null)));
       // 测试 merchantName 不匹配
       accountMapper.insert(cloneIgnoreId(dbAccount, o -> o.setMerchantName(null)));
       // 测试 createTime 不匹配
       accountMapper.insert(cloneIgnoreId(dbAccount, o -> o.setCreateTime(null)));
       // 准备参数
       AccountExportReqVO reqVO = new AccountExportReqVO();
       reqVO.setSysUserId(null);
       reqVO.setAccountId(null);
       reqVO.setAccountType(null);
       reqVO.setTobAccountId(null);
       reqVO.setPhone(null);
       reqVO.setPortrait(null);
       reqVO.setAccountName(null);
       reqVO.setIsOverdue(null);
       reqVO.setLanguage(null);
       reqVO.setNickName(null);
       reqVO.setMainAccountId(null);
       reqVO.setSubAccountId(null);
       reqVO.setEmail(null);
       reqVO.setMerchantName(null);
       reqVO.setCreateTime(buildBetweenTime(2023, 2, 1, 2023, 2, 28));

       // 调用
       List<AccountDO> list = accountService.getAccountList(reqVO);
       // 断言
       assertEquals(1, list.size());
       assertPojoEquals(dbAccount, list.get(0));
    }

}
