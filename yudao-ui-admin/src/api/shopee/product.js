import request from '@/utils/request'

// 创建虾皮商品
export function createProduct(data) {
  return request({
    url: '/shopee/product/create',
    method: 'post',
    data: data
  })
}

// 更新虾皮商品
export function updateProduct(data) {
  return request({
    url: '/shopee/product/update',
    method: 'put',
    data: data
  })
}

// 删除虾皮商品
export function deleteProduct(id) {
  return request({
    url: '/shopee/product/delete?id=' + id,
    method: 'delete'
  })
}

// 获得虾皮商品
export function getProduct(id) {
  return request({
    url: '/shopee/product/get?id=' + id,
    method: 'get'
  })
}

// 获得虾皮商品分页
export function getProductPage(query) {
  return request({
    url: '/shopee/product/page',
    method: 'get',
    params: query
  })
}

// 导出虾皮商品 Excel
export function exportProductExcel(query) {
  return request({
    url: '/shopee/product/export-excel',
    method: 'get',
    params: query,
    responseType: 'blob'
  })
}

// 获得虾皮商品
export function refreshAll() {
  return request({
    url: '/shopee/product/refreshAll',
    method: 'get'
  })
}
