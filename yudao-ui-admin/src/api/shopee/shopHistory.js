import request from '@/utils/request'

// 创建虾皮店铺历史
export function createShopHistory(data) {
  return request({
    url: '/shopee/shop-history/create',
    method: 'post',
    data: data
  })
}

// 更新虾皮店铺历史
export function updateShopHistory(data) {
  return request({
    url: '/shopee/shop-history/update',
    method: 'put',
    data: data
  })
}

// 删除虾皮店铺历史
export function deleteShopHistory(id) {
  return request({
    url: '/shopee/shop-history/delete?id=' + id,
    method: 'delete'
  })
}

// 获得虾皮店铺历史
export function getShopHistory(id) {
  return request({
    url: '/shopee/shop-history/get?id=' + id,
    method: 'get'
  })
}

// 获得虾皮店铺历史分页
export function getShopHistoryPage(query) {
  return request({
    url: '/shopee/shop-history/page',
    method: 'get',
    params: query
  })
}

// 导出虾皮店铺历史 Excel
export function exportShopHistoryExcel(query) {
  return request({
    url: '/shopee/shop-history/export-excel',
    method: 'get',
    params: query,
    responseType: 'blob'
  })
}
