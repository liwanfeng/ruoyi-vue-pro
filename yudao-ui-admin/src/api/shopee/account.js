import request from '@/utils/request'

// 创建虾皮账户
export function createAccount(data) {
  return request({
    url: '/shopee/account/create',
    method: 'post',
    data: data
  })
}

// 更新虾皮账户
export function updateAccount(data) {
  return request({
    url: '/shopee/account/update',
    method: 'put',
    data: data
  })
}

// 删除虾皮账户
export function deleteAccount(id) {
  return request({
    url: '/shopee/account/delete?id=' + id,
    method: 'delete'
  })
}

// 获得虾皮账户
export function getAccount(id) {
  return request({
    url: '/shopee/account/get?id=' + id,
    method: 'get'
  })
}

// 获得虾皮账户分页
export function getAccountPage(query) {
  return request({
    url: '/shopee/account/page',
    method: 'get',
    params: query
  })
}

// 导出虾皮账户 Excel
export function exportAccountExcel(query) {
  return request({
    url: '/shopee/account/export-excel',
    method: 'get',
    params: query,
    responseType: 'blob'
  })
}


// 获得虾皮账户
export function refreshAccount(id) {
  return request({
    url: '/shopee/account/refreshAccount?id=' + id,
    method: 'get'
  })
}
