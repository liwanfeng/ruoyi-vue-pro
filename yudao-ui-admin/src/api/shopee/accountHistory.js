import request from '@/utils/request'

// 创建虾皮账户历史
export function createAccountHistory(data) {
  return request({
    url: '/shopee/account-history/create',
    method: 'post',
    data: data
  })
}

// 更新虾皮账户历史
export function updateAccountHistory(data) {
  return request({
    url: '/shopee/account-history/update',
    method: 'put',
    data: data
  })
}

// 删除虾皮账户历史
export function deleteAccountHistory(id) {
  return request({
    url: '/shopee/account-history/delete?id=' + id,
    method: 'delete'
  })
}

// 获得虾皮账户历史
export function getAccountHistory(id) {
  return request({
    url: '/shopee/account-history/get?id=' + id,
    method: 'get'
  })
}

// 获得虾皮账户历史分页
export function getAccountHistoryPage(query) {
  return request({
    url: '/shopee/account-history/page',
    method: 'get',
    params: query
  })
}

// 导出虾皮账户历史 Excel
export function exportAccountHistoryExcel(query) {
  return request({
    url: '/shopee/account-history/export-excel',
    method: 'get',
    params: query,
    responseType: 'blob'
  })
}
