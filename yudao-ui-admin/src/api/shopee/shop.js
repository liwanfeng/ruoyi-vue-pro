import request from '@/utils/request'

// 创建虾皮店铺
export function createShop(data) {
  return request({
    url: '/shopee/shop/create',
    method: 'post',
    data: data
  })
}

// 更新虾皮店铺
export function updateShop(data) {
  return request({
    url: '/shopee/shop/update',
    method: 'put',
    data: data
  })
}

// 删除虾皮店铺
export function deleteShop(id) {
  return request({
    url: '/shopee/shop/delete?id=' + id,
    method: 'delete'
  })
}

// 获得虾皮店铺
export function getShop(id) {
  return request({
    url: '/shopee/shop/get?id=' + id,
    method: 'get'
  })
}

// 获得虾皮店铺分页
export function getShopPage(query) {
  return request({
    url: '/shopee/shop/page',
    method: 'get',
    params: query
  })
}

// 导出虾皮店铺 Excel
export function exportShopExcel(query) {
  return request({
    url: '/shopee/shop/export-excel',
    method: 'get',
    params: query,
    responseType: 'blob'
  })
}

// 获得虾皮店铺
export function refreshAll() {
  return request({
    url: '/shopee/shop/refreshAll',
    method: 'get'
  })
}