import request from '@/utils/request'

// 创建虾皮商品历史
export function createProductHistory(data) {
  return request({
    url: '/shopee/product-history/create',
    method: 'post',
    data: data
  })
}

// 更新虾皮商品历史
export function updateProductHistory(data) {
  return request({
    url: '/shopee/product-history/update',
    method: 'put',
    data: data
  })
}

// 删除虾皮商品历史
export function deleteProductHistory(id) {
  return request({
    url: '/shopee/product-history/delete?id=' + id,
    method: 'delete'
  })
}

// 获得虾皮商品历史
export function getProductHistory(id) {
  return request({
    url: '/shopee/product-history/get?id=' + id,
    method: 'get'
  })
}

// 获得虾皮商品历史分页
export function getProductHistoryPage(query) {
  return request({
    url: '/shopee/product-history/page',
    method: 'get',
    params: query
  })
}

// 导出虾皮商品历史 Excel
export function exportProductHistoryExcel(query) {
  return request({
    url: '/shopee/product-history/export-excel',
    method: 'get',
    params: query,
    responseType: 'blob'
  })
}
